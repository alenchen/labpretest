#!/bin/bash
#
# Test Plan : Basic test plan functions
#
#

# Set delay time between two test case
maillist=( )
testcase_title=( )
testcase_category=( )
testcase_file=( )
testcase_parameters=( )
testcase_delay=( )
testcase_result=( )

################################################
# Prints output to console with time stamp
# Arguments:
#   None
# Returns:
#   None
################################################
log_print()
{
  if [ -z "$1" ]; then
    echo "# $(date +'%D %T')"
  else
    echo "# $(date +'%D %T'): $1"
  fi
}

################################################
# Add account into mail list
# Arguments:
#   $1 is the account name you want to add into mail list
# Returns:
#   None
################################################

add_maillist()
{
    maillist[${#maillist[*]}]=$1
}

################################################
# Add test case into plan
# Arguments:
#   $1 is the test case title (will be shown on report)
#   $2 is the category of test case
#   $3 is the name of test case python file
#   $4 is the delay before to run next case
# Returns:
#   None
################################################

insert_testcase()
{
    testcase_title[${#testcase_title[*]}]=$1
    testcase_category[${#testcase_category[*]}]=$2
    testcase_file[${#testcase_file[*]}]=$3
    testcase_parameters[${#testcase_parameters[*]}]=$4
    testcase_delay[${#testcase_delay[*]}]=$5
    testcase_result[${#testcase_result[*]}]="Fail"
}

update_result()
{
    testcase_result[$1]=$2
}

update_testreport_head()
{
    echo "    var test_data = {" >> $1
    echo "        \"test_info\" : {" >> $1
    echo "            \"device\" : \"$2\"," >> $1
    echo "            \"sw_rev\" : \"$3\"," >> $1
    echo "            \"hw_rev\" : \"$4\"," >> $1
    echo "            \"start_datetime\" : \"$5\"," >> $1
    echo "            \"end_datetime\" : \"$6\"," >> $1
    echo "            \"station\" : \"$7\"," >> $1
    echo "            \"log_path\" : \"$8\"" >> $1
    echo "        }," >> $1
    echo "        \"test_result\" : [" >> $1
}

update_testreport_body()
{
    echo "            { \"result\" : \"$2\", \"category\" : \"$3\", \"description\" : \"$4\", " >> $1
    echo -n "              \"snapshot\" : ">> $1
    ls public/${5}/${6}/${7}-*.png public/${5}/${6}/${7}.png | awk ' BEGIN { ORS = ""; print "["; } { print "/@"$0"/@"; } END { print "]"; }' | sed "s^\"^\\\\\"^g;s^/@/@^\", \"^g;s^/@^\"^g" >> $1
    echo -n "              , \"logfile\" : ">> $1
    ls public/${5}/${6}/${7}/* | awk ' BEGIN { ORS = ""; print "["; } { print "/@"$0"/@"; } END { print "]"; }' | sed "s^\"^\\\\\"^g;s^/@/@^\", \"^g;s^/@^\"^g" >> $1
    echo "            }," >> $1
}

update_testreport_body_no_comma()
{
    echo "            { \"result\" : \"$2\", \"category\" : \"$3\", \"description\" : \"$4\", " >> $1
    echo -n "              \"snapshot\" : ">> $1
    ls public/${5}/${6}/${7}-*.png public/${5}/${6}/${7}.png | awk ' BEGIN { ORS = ""; print "["; } { print "/@"$0"/@"; } END { print "]"; }' | sed "s^\"^\\\\\"^g;s^/@/@^\", \"^g;s^/@^\"^g" >> $1
    echo -n "              , \"logfile\" : ">> $1
    ls public/${5}/${6}/${7}/* | awk ' BEGIN { ORS = ""; print "["; } { print "/@"$0"/@"; } END { print "]"; }' | sed "s^\"^\\\\\"^g;s^/@/@^\", \"^g;s^/@^\"^g" >> $1
    echo "            }" >> $1
}

update_testreport_foot()
{
    echo "        ]" >> $1
    echo "    };" >> $1
}

run_test_plan()
{
    # Part 3: Execute test case
    log_print "Start to run test cases..."
    sleep 60

    START_DATETIME=`date +%Y/%m/%d-%T`

    for ((i=0; i<${#testcase_title[*]}; i++)); do
        echo case_result_is=Manual > case_result_is
        log_print "Execute test case - [${testcase_category[$i]}] ${testcase_title[$i]}"
        log_print "./tools/monkeyrunner ${PROJECT}/${testcase_file[$i]} $1 $2 $i ${testcase_parameters[$i]}"
        tools/monkeyrunner ${PROJECT}/${testcase_file[$i]} "$1" "$2" "$i" "${testcase_parameters[$i]}"
        curl 'http://localhost:4567/update_device?device_id=$device&device_name=$product&state=testing&data=${testcase_file[$i]}'
        . case_result_is
        log_print "==> \$case_result_is $case_result_is"
        update_result $i $case_result_is
        log_print "Wait ${testcase_delay[$i]} seconds"
        sleep ${testcase_delay[$i]}
    done

    # Part 4: Generate report
    log_print "Generating the report"
    IP=`ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}'`
    END_DATETIME=`date +%Y/%m/%d-%T`
    mkdir -p "public/${PROJECT}"
    cat report_template/report_part1.html > "public/${PROJECT}/${REPORT_NAME}"
    update_testreport_head "public/${PROJECT}/${REPORT_NAME}" "${PROJECT}" "${REPORT_SW_VERSION}" "${REPORT_HW_VERSION}" "${START_DATETIME}" "${END_DATETIME}" "${IP}" "${REPORT_LOG}"
    pass_count=0
    fail_count=0
    draw_count=0
    for ((i=0; i<${#testcase_title[*]}; i++)); do
        log_print "Test case - [${testcase_category[$i]}] ${testcase_title[$i]} is ${testcase_result[$i]}"
        j=$((${#testcase_title[*]}-1))
        if [ "$i" == "$j" ]; then
            update_testreport_body_no_comma "public/${PROJECT}/${REPORT_NAME}" "${testcase_result[$i]}" "${testcase_category[$i]}" "${testcase_title[$i]}" "${PROJECT}" "${TEST_SERIAL}" "${i}"
        else
            update_testreport_body "public/${PROJECT}/${REPORT_NAME}" "${testcase_result[$i]}" "${testcase_category[$i]}" "${testcase_title[$i]}" "${PROJECT}" "${TEST_SERIAL}" "${i}"
        fi
        if [ "${testcase_result[$i]}" == "Pass" ]; then
            pass_count=$(($pass_count+1))
        elif [ "${testcase_result[$i]}" == "Fail" ]; then
            fail_count=$(($fail_count+1))
        else
            draw_count=$(($draw_count+1))
        fi
    done
    update_testreport_foot "public/${PROJECT}/${REPORT_NAME}"
    cat report_template/report_part3.html >> "public/${PROJECT}/${REPORT_NAME}"
    sleep $DELAY

    # Part 5: Send report by mail
    for ((i=0; i<${#maillist[*]}; i++)); do
        log_print "Send report to ${maillist[$i]}@compalcomm.com"
        curl "http://${IP}:4567/send_notify_mail?to=${maillist[$i]}&test_station=${IP}&project=${PROJECT}&plan_description=${PLAN_DESCRIPTION}&pass_count=${pass_count}&fail_count=${fail_count}&draw_count=${draw_count}&report_name=${REPORT_NAME}&sw_version=${REPORT_SW_VERSION}"
        sleep $MAILDELAY
    done   
}
