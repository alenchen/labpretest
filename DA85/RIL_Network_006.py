from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys


#Test6 : Search Network


device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 
sleep(1)

print "Click Network operators"
cci_device.clickText('Network operators')
sleep(1)

#Network 006 Result
print "Snap shot"
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])


print "wait search"
sleep(150)


print 'ensureUnlock'
Telephony.ensureUnlock(device) 
sleep(1)


print "Click Search networks"
cci_device.clickText('Search networks')
print "wait search"
sleep(150)


print 'ensureUnlock'
Telephony.ensureUnlock(device) 
sleep(1)


#Network 006 Result
print "Snap shot xxx_006_SearchNetworks.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'

