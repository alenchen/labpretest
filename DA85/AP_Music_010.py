from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep
import filecmp

def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

Screen_unlock()

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(5)

image = device.takeSnapshot()
image.writeToFile('.tmp1','png')
sleep(3) 

print 'press menu key'
device.touch(355, 1100, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press WIDGETS'
device.touch(244, 107, MonkeyDevice.DOWN_AND_UP)
sleep(2)

cci_device.setIsFullTextMatch(True)

print 'search and click music widget'
i = 0 

while True :  
  if cci_device.getTextLocation('Music') :
     print 'found music widget'
     X = int(cci_device.getTextLocation('Music').split('(')[1].split(',')[0]); 
     Y = int(cci_device.getTextLocation('Music').split(',')[1].split(')')[0]); 

     device.touch(X, Y, MonkeyDevice.DOWN)
     sleep(3)
     device.touch(X, Y, MonkeyDevice.UP)     
     break
  print 'not found music widget'
  if i > 5 :
     print 'Can not find music widget, test fail'
     Common.updateResult('Fail')
     sys.exit(1)
     break  
  i += 1
  Common.scrollRight(device)

sleep(3)

image = device.takeSnapshot()
image.writeToFile('.tmp2','png')
sleep(3)

res = filecmp.cmp('.tmp1','.tmp2')

print "=========================================="
if  not res:
	print 'Pass'
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Pass');	
else :
	print 'Fail'
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Fail');	

os.remove(".tmp1")
os.remove(".tmp2")
