from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device=EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device=CCIEasyMonkeyDevice("ss",device)
print "Create CCIEasyMonkeyDevice"

print 'wake up device'
device.wake()
sleep(2)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(3)

print 'press Contacts icon'
cci_device.clickText('People')
sleep(2)

print 'Choose a Contact'
cci_device.clickText('Shinjyo')
sleep(2)

print 'press add favorite'
device.touch(575, 100, MonkeyDevice.DOWN_AND_UP);
sleep(2)
print 'back to previous'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'go to Favorite page'
device.touch(620, 100, MonkeyDevice.DOWN_AND_UP);
sleep(2)
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)
if cci_device.isTextVisible('Shinjyo'):
     print 'Add favorites pass'
     Common.updateResult('Pass');
else:
     print 'Add favorites failure'
     Common.updateResult('Fail');
sleep(2)


