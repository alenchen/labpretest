from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands, filecmp
from PScript import *

print 'Wait connect to device'
device = MonkeyRunner.waitForConnection()

print 'Wake up device'
device.wake()
sleep(1)

print 'Create easymonkeydevice'
easy_device = EasyMonkeyDevice(device)
sleep(1)

print 'Create CCI easymonkeydevice'
cci_device = CCIEasyMonkeyDevice(device)
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

Email.enterEmailAP(device, easy_device, cci_device)
res = Email.waitText(cci_device, 'Inbox')
if res == False:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'
  sys.exit(1)

####################
# test case start
####################

print "touch wrtie mail icon"
easy_device.touch(By.id('id/compose'),'downAndUp')
sleep(2)

#wait
Email.waitText(cci_device, 'Compose')

print 'input To'
easy_device.touch(By.id('id/to'),'downAndUp')
input_to = 'test.compalcomm@gmail.com'
device.type(input_to)
sleep(2)

print 'input Subject'
easy_device.touch(By.id('id/subject'),'downAndUp')
input_subject = 'test'
device.type(input_subject)
sleep(2)

print 'input Body text'
easy_device.touch(By.id('id/body_text'),'downAndUp')
input_mail = 'test'
device.type(input_mail)
sleep(2)

print "touch Send icon"
easy_device.touch(By.id('id/send'),'downAndUp')
sleep(2)

if cci_device.isTextVisible('Inbox'):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass')
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'