# Part 1: Define mail list
add_maillist ben_meng
add_maillist brianh_huang
add_maillist luke_huang
add_maillist snaka_liao
add_maillist alen_tseng
add_maillist michael_yang
add_maillist danny_tsai
add_maillist molin_kuo
add_maillist vandusa_hsu
add_maillist blake_lin
add_maillist nina_lin
add_maillist willie_chen

# Part 2: Define test case
# ====== Group - AP_3rdParty ======
#insert_testcase "AP_3rdParty_001" "SYS" "AP_3rdParty_001.py" "" 1

# ====== Group - AP_AboutPhone ======
insert_testcase "AP_AboutPhone_001" "SYS" "AP_AboutPhone_001.py" "" 1
insert_testcase "AP_AboutPhone_002" "SYS" "AP_AboutPhone_002.py" "" 1
#insert_testcase "AP_AboutPhone_003" "SYS" "AP_AboutPhone_003.py" "" 1

# ====== Group - AP_AlarmClock ======
#insert_testcase "AP_AlarmClock_001" "SYS" "AP_AlarmClock_001.py" "" 1
insert_testcase "AP_AlarmClock_002" "SYS" "AP_AlarmClock_002.py" "" 1
#insert_testcase "AP_AlarmClock_003" "SYS" "AP_AlarmClock_003.py" "" 1
insert_testcase "AP_AlarmClock_004" "SYS" "AP_AlarmClock_004.py" "" 1

# ====== Group - AP_Calculator ======
insert_testcase "AP_Calculator_005" "SYS" "AP_Calculator_005.py" "" 1
insert_testcase "AP_Calculator_006" "SYS" "AP_Calculator_006.py" "" 1
insert_testcase "AP_Calculator_007" "SYS" "AP_Calculator_007.py" "" 1
insert_testcase "AP_Calculator_008" "SYS" "AP_Calculator_008.py" "" 1
insert_testcase "AP_Calculator_009" "SYS" "AP_Calculator_009.py" "" 1

# ====== Group - AP_Calendar ======
insert_testcase "AP_Calendar_001" "SYS" "AP_Calendar_001.py" "" 1
#insert_testcase "AP_Calendar_002" "SYS" "AP_Calendar_002.py" "" 1
#insert_testcase "AP_Calendar_003" "SYS" "AP_Calendar_003.py" "" 1
#insert_testcase "AP_Calendar_004" "SYS" "AP_Calendar_004.py" "" 1

# ====== Group - AP_LatinIME ======
insert_testcase "AP_LatinIME_001" "SYS" "AP_LatinIME_001.py" "" 1
#insert_testcase "AP_LatinIME_002" "SYS" "AP_LatinIME_002.py" "" 1
insert_testcase "AP_LatinIME_003" "SYS" "AP_LatinIME_003.py" "" 1
#insert_testcase "AP_LatinIME_004" "SYS" "AP_LatinIME_004.py" "" 1

# ====== Group - AP_Launcher ======
insert_testcase "AP_Launcher_001" "SYS" "AP_Launcher_001.py" "" 1
insert_testcase "AP_Launcher_002" "SYS" "AP_Launcher_002.py" "" 1
insert_testcase "AP_Launcher_003" "SYS" "AP_Launcher_003.py" "" 1
insert_testcase "AP_Launcher_004" "SYS" "AP_Launcher_004.py" "" 1

# ====== Group - AP_Search ======
insert_testcase "AP_Search_001" "SYS" "AP_Search_001.py" "" 1
insert_testcase "AP_Search_002" "SYS" "AP_Search_002.py" "" 1
insert_testcase "AP_Search_003" "SYS" "AP_Search_003.py" "" 1

# ====== Group - AP_Settings ======
insert_testcase "AP_Settings_001" "SYS" "AP_Settings_001.py" "" 1
insert_testcase "AP_Settings_002" "SYS" "AP_Settings_002.py" "" 1
insert_testcase "AP_Settings_003" "SYS" "AP_Settings_003.py" "" 1
insert_testcase "AP_Settings_004" "SYS" "AP_Settings_004.py" "" 1

# ====== Group - GOOGLE_GMS ======
insert_testcase "GOOGLE_GMS_001" "SYS" "GOOGLE_GMS_001.py" "" 1
#insert_testcase "GOOGLE_GMS_002" "SYS" "GOOGLE_GMS_002.py" "" 1
#insert_testcase "GOOGLE_GMS_003" "SYS" "GOOGLE_GMS_003.py" "" 1
#insert_testcase "GOOGLE_GMS_004" "SYS" "GOOGLE_GMS_004.py" "" 1
#insert_testcase "GOOGLE_GMS_005" "SYS" "GOOGLE_GMS_005.py" "" 1
#insert_testcase "GOOGLE_GMS_006" "SYS" "GOOGLE_GMS_006.py" "" 1
#insert_testcase "GOOGLE_GMS_007" "SYS" "GOOGLE_GMS_007.py" "" 1
insert_testcase "GOOGLE_GMS_008" "SYS" "GOOGLE_GMS_008.py" "" 1
#insert_testcase "GOOGLE_GMS_009" "SYS" "GOOGLE_GMS_009.py" "" 1
insert_testcase "GOOGLE_GMS_010" "SYS" "GOOGLE_GMS_010.py" "" 1
#insert_testcase "GOOGLE_GMS_011" "SYS" "GOOGLE_GMS_011.py" "" 1
#insert_testcase "GOOGLE_GMS_012" "SYS" "GOOGLE_GMS_012.py" "" 1
#insert_testcase "GOOGLE_GMS_013" "SYS" "GOOGLE_GMS_013.py" "" 1
insert_testcase "GOOGLE_GMS_014" "SYS" "GOOGLE_GMS_014.py" "" 1

# ====== Group - ISV_Youtube ======
insert_testcase "ISV_Youtube_001" "SYS" "ISV_Youtube_001.py" "" 1
insert_testcase "ISV_Youtube_002" "SYS" "ISV_Youtube_002.py" "" 1
insert_testcase "ISV_Youtube_003" "SYS" "ISV_Youtube_003.py" "" 1
#insert_testcase "ISV_Youtube_004" "SYS" "ISV_Youtube_004.py" "" 1
#insert_testcase "ISV_Youtube_005" "SYS" "ISV_Youtube_005.py" "" 1
insert_testcase "ISV_Youtube_006" "SYS" "ISV_Youtube_006.py" "" 1
#insert_testcase "ISV_Youtube_007" "SYS" "ISV_Youtube_007.py" "" 1

# ====== Group - SYS_Key ======
#insert_testcase "SYS_Key_001" "SYS" "SYS_Key_001.py" "" 1
#insert_testcase "SYS_Key_002" "SYS" "SYS_Key_002.py" "" 1
#insert_testcase "SYS_Key_003" "SYS" "SYS_Key_003.py" "" 1
#insert_testcase "SYS_Key_004" "SYS" "SYS_Key_004.py" "" 1
#insert_testcase "SYS_Key_005" "SYS" "SYS_Key_005.py" "" 1

# ====== Group - SYS_Loader ======
#insert_testcase "SYS_Loader_001" "SYS" "SYS_Loader_001.py" "" 1
#insert_testcase "SYS_Loader_002" "SYS" "SYS_Loader_002.py" "" 1
#insert_testcase "SYS_Loader_003" "SYS" "SYS_Loader_003.py" "" 1
#insert_testcase "SYS_Loader_004" "SYS" "SYS_Loader_004.py" "" 1

# ====== Group - SYS_Battery ======
#insert_testcase "SYS_Battery_001" "SYS" "SYS_Battery_001.py" "" 1
insert_testcase "SYS_Battery_002" "SYS" "SYS_Battery_002.py" "" 1
insert_testcase "SYS_Battery_003" "SYS" "SYS_Battery_003.py" "" 1
insert_testcase "SYS_Battery_004" "SYS" "SYS_Battery_004.py" "" 1
insert_testcase "SYS_Battery_005" "SYS" "SYS_Battery_005.py" "" 1
#insert_testcase "SYS_Battery_006" "SYS" "SYS_Battery_006.py" "" 1
#insert_testcase "SYS_Battery_007" "SYS" "SYS_Battery_007.py" "" 1
#insert_testcase "SYS_Battery_008" "SYS" "SYS_Battery_008.py" "" 1

# ====== Group - SYS_Power_Mgt ======
#insert_testcase "SYS_Power_Mgt_001" "SYS" "SYS_Power_Mgt_001.py" "" 1
#insert_testcase "SYS_Power_Mgt_002" "SYS" "SYS_Power_Mgt_002.py" "" 1
#insert_testcase "SYS_Power_Mgt_003" "SYS" "SYS_Power_Mgt_003.py" "" 1

# ====== Group - SYS_Recovery ======
#insert_testcase "SYS_Recovery_001" "SYS" "SYS_Recovery_001.py" "" 1
