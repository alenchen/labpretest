#!/bin/bash
#
# Test Plan : Auto Regular CM Process Demo
#
#

# Set delay time between two test case
DELAY=5
MAILDELAY=1
PROJECT="DA85"
REPORT_NAME="${1}_${2}.html"
REPORT_SW_VERSION="${3}"
REPORT_HW_VERSION="DVT1-2"
TEST_SERIAL="${2}"
TEL_NUMBER="${4}"
PLAN_DESCRIPTION="Demo-Automation_CM_Process"

. common/plan_basic.sh

# Part 1: Define mail list
add_maillist alen2_chen
add_maillist carrie_wang
add_maillist danny_tsai

# Part 2: Define test case

# Start to run test plan
run_test_plan "${PROJECT}" "${2}"
