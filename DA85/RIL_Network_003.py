from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys

#do network_0 first
#Test 3 set Airplan mode on and reboot 

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"


print 'ensureUnlock'
Telephony.ensureUnlock(device) 
sleep(1)

print 'Click Airplan mode (on)'
cci_device.clickText('Airplane mode')
sleep(5)


print 'reboot'
device.shell("reboot")
sleep(50)


#Network 003 Result
print "Snap shot xxx_003_Airplan.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'





