
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands
from PScript import *

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device=EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device=CCIEasyMonkeyDevice("ss",device)
print "Create CCIEasyMonkeyDevice"

print 'wake up device'
device.wake()
sleep(2)
 
print 'ensureUnlock'
Telephony.ensureUnlock(device)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press Contacts Icon'
cci_device.clickText('People')
sleep(2)

#<Preparing...>
#====================================================================================================

print 'press menu'
device.touch(666, 1168, MonkeyDevice.DOWN_AND_UP)
sleep(1)
print 'Import/export SIM'
cci_device.clickText('Import/export')
sleep(1)

cci_device.clickText('Manage SIM card contacts')
sleep(2)
count=0
for x in range(30):
    count+=1
    sleep(1)
    if not cci_device.isTextVisible('Reading'): 
       break                
sleep(3)
print count
if count>=30:
    print 'Reading Contacts from SIM fail'
    Common.updateResult('Fail')
    sys.exit(1)   
else:
    count=0
    sleep(1)
    device.touch(668, 112, MonkeyDevice.DOWN_AND_UP)
    sleep(1)
    if cci_device.isTextVisible('Delete All SIM Contacts'):  
           print 'press Delete All SIM Contacts'
           sleep(1)
           cci_device.clickText('Delete All SIM Contacts')
           for x in range(15):
               count+=1
               sleep(1)
               if cci_device.isTextVisible('Result...'): 
                  print 'press OK'
                  cci_device.clickText('OK')
                  break 
           if(count>=15):
              print 'Delete Contacts on your SIM card, it is too late'
    else:
           print 'press Back by No Contacts'
           device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
           sleep(1)
           device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
           sleep(1)
sleep(3)
if not cci_device.isTextVisible('Shinjyo'):
   print 'back error'
   Common.updateResult('Fail');
   sys.exit(1)
sleep(1)
print 'press menu'
device.touch(666, 1168, MonkeyDevice.DOWN_AND_UP)
sleep(1)
print 'press Import/export'
cci_device.clickText('Import/export')
sleep(1)
print 'press Export to SIM card'
cci_device.clickText('Export to SIM card')
sleep(1)
count=0
for x in range(30):
      count+=1
      sleep(1)
      if cci_device.isTextVisible('Result...'): 
         cci_device.clickText('OK')
         break 
print count
if count>=30: 
      print 'Export Contacts to SIM fail'
sleep(1)
if cci_device.isTextVisible('Shinjyo'):
   print 'Choose a Contact'
   cci_device.clickText('Shinjyo')
   sleep(1)
else:
   Common.updateResult('Fail')
   sys.exit(1)
print 'press menu'
device.touch(675, 120, MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'press Contacts icon'
cci_device.clickText('Delete')
sleep(1)

print 'press OK'
cci_device.clickText('OK')
sleep(1)

#<import SIM>
#=====================================================================================================

Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])


if cci_device.isTextVisible('Import contacts'):
     print 'Import Contacts'
     cci_device.clickText('Import contacts')
else:
     print 'press menu'
     device.touch(666, 1168, MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'choose Import contacts'
cci_device.clickText('Import/export')
sleep(1)

print 'choose Manage SIM card contacts'
cci_device.clickText('Manage SIM card contacts')
sleep(1)
print 'press menu'
device.touch(660, 108, MonkeyDevice.DOWN_AND_UP);
sleep(1)

Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)
print 'choose import all'
cci_device.clickText('Import all')
sleep(10)

if cci_device.isTextVisible('Shinjyo'):
     print 'import Contacts pass'
     Common.updateResult('Pass');
else:
     print 'import Contacts failure'
     Common.updateResult('Fail');
sleep(1)

sleep(2)

