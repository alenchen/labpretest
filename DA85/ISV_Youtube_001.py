import sys,os
from PScript import *


c= Common.getInstance()

c.init_log(os.path.basename(__file__).split(".")[0] )

c.waitForConnection()

c.wake()

c.unLockIfScreenLock()

c.goHome()

c.launchAppList()

c.info('search and click YouTube')
res=False
if c.search_and_click('YouTube'):
  c.info('launch YouTube')
  if c.visible(id="id/home"):
    c.info('make sure we are at youtube home')
    c.touch(id="id/home")
    c.sleep(0.5)
    c.touch(id="id/home")
    c.sleep(0.5)
  c.clickText('Trending')
  c.sleep(1)
  c.touch(300,350, wait=10)
  res=True

c.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
if res:
  c.info("TestResult: Manual")
  c.updateResult('Manual')
else:
  c.info("TestResult: false")
  c.updateResult('Fail')

if c.visible(id="id/home"):
  c.touch(id="id/home")
  c.touch(id="id/home")
