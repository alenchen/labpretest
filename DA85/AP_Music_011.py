from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep


def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

Screen_unlock()

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(5)

print 'click Music widget play'
device.touch(500, 299, MonkeyDevice.DOWN_AND_UP)
sleep(10)
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])

print 'click Music widget pause'
device.touch(500, 299, MonkeyDevice.DOWN_AND_UP)
sleep(5)
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])

print 'click Music widget next'
device.touch(625, 299, MonkeyDevice.DOWN_AND_UP)
sleep(10)
Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])

print 'click Music widget pause'
device.touch(500, 299, MonkeyDevice.DOWN_AND_UP)
sleep(5)

print 'delete music widget'
device.touch(560, 296, MonkeyDevice.DOWN)
music_widget_start = (320, 250)
music_widget_end = (320, 150)
device.drag(music_widget_start,music_widget_end)

print 'press menu key'
device.touch(355, 1100, MonkeyDevice.DOWN_AND_UP)
sleep(2)

for x in range(1, 4):
  print 'scroll left'
  Common.scrollLeft(device);
  sleep(2)

print 'press APPS'
device.touch(66, 96, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press back key'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print "=========================================="
Common.updateResult('Manual');
print 'Manual'

os.system("adb shell am force-stop com.android.music")
