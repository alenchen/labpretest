import sys,os
from PScript import *

g = GoogleGms.getInstance()

Log = Log.getInstance(TAG=os.path.basename(__file__).split(".")[0], levelfilter='iw')

g.waitForConnection()

g.wake()

g.unLockIfScreenLock()

g.goHome()

g.launchAppList()

Log.i('search and click Play Store')
res=False
if g.search_and_click('Play Store'):
  Log.i("launch Play Store")
  g.sleep(3)

  if g.visible(id="id/search_button"):
    g.touch(id="id/search_button", wait=2)
    Log.i("touch id/search_button")

  g.input("angry birdsE")  
  Log.i("input angry birds to search")

  g.touch(360, 320) # first item
  Log.i("touch first item")

  try:
    id="id/buy_button"
    g.touch(id) # install button
    Log.i("touch install button")

    id="id/acquire_button"
    g.touch(id) # Accept & download button
    Log.i("touch Accept & download button")

    Log.i("wait download to finish")
    for i in range (0, 50):
      if g.visible(id="id/launch_button"):
        g.touch(id="id/launch_button") # open button
        Log.i("touch open button")
        res=True
        break
      g.sleep(10)

    g.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    
    for i in range (0 , 6):
      g.press('KEYCODE_BACK')

  except ValueError:
    Log.w("not find id=%s", id)
    pass
  else:
    g.takeSnapshot(filename=sys.argv[3]+"_error", path=sys.argv[1]+'/'+sys.argv[2])
    pass

if res:
  Log.i("TestResult: Manual")
  g.updateResult('Manual')
else:
  Log.i("TestResult: Fail")
  g.updateResult('Fail')

