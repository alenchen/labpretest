from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands, filecmp
from PScript import *

print 'Wait connect to device'
device = MonkeyRunner.waitForConnection()

print 'Wake up device'
device.wake()
sleep(1)

print 'Create easymonkeydevice'
easy_device = EasyMonkeyDevice(device)
sleep(1)

print 'Create CCI easymonkeydevice'
cci_device = CCIEasyMonkeyDevice(device)
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

####################
# local function
####################

def donwloadFileFromRarlab():
  print 'press Home key'
  device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
  sleep(2)

  print 'launch http://www.rarlab.com/download.htm using startActivity'
  device.startActivity(component='com.android.browser/.BrowserActivity', uri='http://www.rarlab.com/download.htm')
  sleep(20)

  print('touch file')
  device.touch(150, 600, MonkeyDevice.DOWN)
  sleep(1)
  device.touch(150, 600, MonkeyDevice.UP)
  sleep(2)

  print 'click Open'
  cci_device.clickText('Open')
  sleep(5)

def changeUseOnly2GNetworks():
  print 'press Home key'
  device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
  sleep(2)

  print 'press Menu key'
  device.press('KEYCODE_MENU','downAndUp')
  sleep(2)

  print 'click System settings'
  cci_device.clickText('System settings')
  sleep(5)

  print 'click More'
  cci_device.clickText('More')
  sleep(5)

  print 'click Mobile networks'
  cci_device.clickText('Mobile networks')
  sleep(5)

  print 'click Use only 2G networks'
  cci_device.clickText('Use only 2G networks')
  sleep(20)

def showStatusBar():
  device.drag((360,10),(360,1010),0.5,10)
  
def waitOneMinute():
  showStatusBar()
  sleep(20)
  showStatusBar()
  sleep(20)
  showStatusBar()
  sleep(20)

####################
# test case start
####################

# step 0
res = Telephony.CheckSDcard(device)
if res == False:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'This testcase need insert SD card'
  print 'Failed'
  sys.exit(1)

# step 1
donwloadFileFromRarlab()
Common.takeSnapshot(device, filename=sys.argv[3]+'-11', path=sys.argv[1]+'/'+sys.argv[2])

print 'scroll-down Status Bar'
showStatusBar()
sleep(5)
Common.takeSnapshot(device, filename=sys.argv[3]+'-12', path=sys.argv[1]+'/'+sys.argv[2])

print 'wait 1 minute'
waitOneMinute()

showStatusBar()
sleep(5)
Common.takeSnapshot(device, filename=sys.argv[3]+'-13', path=sys.argv[1]+'/'+sys.argv[2])

# step 2
changeUseOnly2GNetworks()

# step 3
donwloadFileFromRarlab()
Common.takeSnapshot(device, filename=sys.argv[3]+'-21', path=sys.argv[1]+'/'+sys.argv[2])

print 'scroll-down Status Bar'
showStatusBar()
sleep(5)
Common.takeSnapshot(device, filename=sys.argv[3]+'-22', path=sys.argv[1]+'/'+sys.argv[2])

print 'wait 3 minutes'
waitOneMinute()
waitOneMinute()
waitOneMinute()

showStatusBar()
sleep(5)
Common.takeSnapshot(device, filename=sys.argv[3]+'-23', path=sys.argv[1]+'/'+sys.argv[2])

Common.updateResult('Manual')
print 'Manual'

print 'touch Clear button'
easy_device.touch(By.id('id/clear_all_button'),'downAndUp')
sleep(2)

# step 4
changeUseOnly2GNetworks()

print 'press Home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
