import sys,os
from PScript import *

c = Calculator.getInstance()

c.init_log(os.path.basename(__file__).split(".")[0])

c.waitForConnection()

c.wake()

c.unLockIfScreenLock()

c.goHome()

c.launchAppList()

c.info('search and click Calculator')
res=False
if c.search_and_click('Calculator'):
  c.info('launch Calculator')
  c.goBasic();
  c.longClickCLR()
  c.inputButton("2*(3-10.6)+5798/4=")
  if c.isTextVisible('1434.3'):
    res=True
  c.longClickCLR()

c.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
if res:
  c.info("TestResult: Pass")
  c.updateResult('Pass')
else:
  c.info("TestResult: false")
  c.updateResult('Fail')
