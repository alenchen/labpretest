from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
import sys,os,commands
from time import sleep
from PScript import *

def Screen_unlock():
	#print 'wake up device'
	device.wake()
	sleep(1)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1)
	print 'wake up device'
	device.wake()
	sleep(1)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

Screen_unlock()

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

model = device.getSystemProperty('ro.product.model')
print "The ro.product.model property on device = " + model

cci_device.setIsFullTextMatch(False)

device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(1)

device.shell("mkdir sdcard/music").split('\r\n')
sleep(2)

print 'push music into device'
os.system("adb push DA85/music/1.mp3 sdcard/music")
device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED")
sleep(6)

print 'play music'
device.shell("am start -n com.android.music/.MusicBrowserActivity")          
device.touch(360, 224, MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.touch(384, 344, MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.touch(360, 224, MonkeyDevice.DOWN_AND_UP)
sleep(1)
oldList= device.shell("ls sdcard/music").split('\r\n')

print 'push another music file into device'
os.system("adb push DA85/music/2.mp3 sdcard/music")
device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED")
sleep(6)

newList= device.shell("ls sdcard/music").split('\r\n')
print 'check music file'
new_list = []
i = 0
for pic in newList : 
  if pic not in oldList :
    new_list.append(pic)
    i += 1
    print 'new file : sdcard/music/' + pic

if 0 != i:
  Common.updateResult('Pass');
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  print 'Pass'
else :
  Common.updateResult('Fail');
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  print 'Failed'

device.touch(364, 1072, MonkeyDevice.DOWN_AND_UP)

MonkeyRunner.sleep(1)

device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(1)
