from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands
from PScript import *
import filecmp

def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

def SlideShow(resA, resB):
	sleep(3)
	
	image = device.takeSnapshot()
	rect =  (0, 280, 720, 720)
	subimage = image.getSubImage( rect )
	subimage.writeToFile('.tmp1','png')
	Common.takeSnapshot(device, filename=sys.argv[3]+resA, path=sys.argv[1]+'/'+sys.argv[2])
	sleep(2)

	sleep(5)

	image = device.takeSnapshot()
	rect =  (0, 280, 720, 720)
	subimage = image.getSubImage( rect )
	subimage.writeToFile('.tmp2','png')
	Common.takeSnapshot(device, filename=sys.argv[3]+resB, path=sys.argv[1]+'/'+sys.argv[2])

	res = filecmp.cmp('.tmp1','.tmp2')
	if  not res:
		print 'Pass'
		#Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
		Common.updateResult('Pass');
	else :
		print 'Fail'
		#Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
		Common.updateResult('Fail');

	os.remove(".tmp1")
	os.remove(".tmp2")

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

Screen_unlock()
 		
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'launch app list'
device.touch(355, 1100, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'Launch Gallery'
cci_device.clickText('Gallery')
sleep(2)

print 'click Album "Pictures"'
device.touch(345,700, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'Tap "Slideshow" tab'
device.touch(550,100, MonkeyDevice.DOWN_AND_UP)
sleep(5)

SlideShow('-1', '-2')

device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)
sleep(1)
#test case 1 end

print 'click picture 1'
device.touch(215,255, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'show menu'
device.touch(660,70, MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'press "Slideshow" tab'
device.touch(400,220, MonkeyDevice.DOWN_AND_UP)
sleep(1)

SlideShow('-3', '-4')

for x in range(0, 5):
    device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)
    sleep(0.5)

