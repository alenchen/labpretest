from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands, filecmp
from PScript import *

print 'Wait connect to device'
device = MonkeyRunner.waitForConnection()

print 'Wake up device'
device.wake()
sleep(1)

print 'Create easymonkeydevice'
easy_device = EasyMonkeyDevice(device)
sleep(1)

print 'Create CCI easymonkeydevice'
cci_device = CCIEasyMonkeyDevice(device)
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

Email.enterEmailAP(device, easy_device, cci_device)
res = Email.waitText(cci_device, 'Inbox')
if res == False:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'
  sys.exit(1)

####################
# test case start
####################

print 'touch Folder icon'
easy_device.touch(By.id('id/show_all_mailboxes'),'downAndUp')
sleep(2)

Email.waitText(cci_device, 'Folders')

print 'click Sent'
cci_device.clickText('Sent')
sleep(5)

Email.waitText(cci_device, 'Sent')

print 'touch Folder icon'
easy_device.touch(By.id('id/show_all_mailboxes'),'downAndUp')
sleep(2)

Email.waitText(cci_device, 'Folders')

print 'touch Inbox'
cci_device.clickText('Inbox')
sleep(5)

Email.waitText(cci_device, 'Inbox')

if cci_device.isTextVisible('Inbox'):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass')
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'  