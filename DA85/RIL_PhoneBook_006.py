
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands
from PScript import *
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device=EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device=CCIEasyMonkeyDevice("ss",device)
print "Create CCIEasyMonkeyDevice"

print 'wake up device'
device.wake()
sleep(2)

print 'ensureUnlock'
Telephony.ensureUnlock(device)
sleep(2)

print 'click main page'
device.touch(368, 110, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'choose a Contact'
cci_device.clickText('Shinjyo')
sleep(2)

print 'press menu'
device.touch(675, 120, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press Edit'
cci_device.clickText('Edit')
sleep(2)

print 'Shinjyo'
cci_device.clickText('Shinjyo')
sleep(2)

print 'press Back'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press and add e-mail field'
cci_device.clickText('Add another field')
sleep(2)

cci_device.clickText('Email')
sleep(2)

device.type('shinjyo_chen@compalcomm.com')
sleep(2)

print 'press and Edit Phone number'
device.touch(350, 464, MonkeyDevice.DOWN_AND_UP);
sleep(2)

for x in range(10): 
    device.press('KEYCODE_DEL', MonkeyDevice.DOWN_AND_UP)
sleep(2) 
device.type('0287516228')
sleep(2)

print 'press add icon'
device.touch(585, 190, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'press and Edit the first name'
device.touch(200, 290, MonkeyDevice.DOWN_AND_UP);
sleep(2)
for x in range(7): 
    device.press('KEYCODE_DEL', MonkeyDevice.DOWN_AND_UP)
sleep(2) 
    
device.type('Andy')
sleep(2)

print 'press Back'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press and Edit the last name'
device.touch(200, 480, MonkeyDevice.DOWN_AND_UP)
sleep(2)
device.type('Hung')
sleep(2)

print 'press Back'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press Done'
device.touch(95, 90, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press menu'
device.touch(675, 120, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press Ringtones'
cci_device.clickText('Set ringtone')
sleep(2)

print 'press choose Ringtones'
cci_device.clickText('Beat Plucker')
sleep(2)

Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
print 'press OK'
cci_device.clickText('OK')
sleep(2)

print 'press menu'
device.touch(675, 120, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press All calls to voicemail'
cci_device.clickText('All calls to voicemail')
sleep(3)

print 'press menu'
device.touch(675, 120, MonkeyDevice.DOWN_AND_UP)
sleep(2)

Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

print 'press All calls to voicemail'
cci_device.clickText('All calls to voicemail')
sleep(2)
Common.updateResult('Manual')
