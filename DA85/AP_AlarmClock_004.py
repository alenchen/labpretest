import sys,os
from PScript import *

a = AlarmClock.getInstance()

a.init_log(os.path.basename(__file__).split(".")[0])

a.waitForConnection()

a.wake()

a.unLockIfScreenLock()

a.goHome()

a.scrollLeft(5)

a.launchAppList()

a.info('search and drag Analog Clock widget to launcher')
res=False
loc = a.search_and_getXY('Analog clock', isWidget=True)
if loc :
  a.touch(loc[0]+20, loc[1]-50, touchType='down', wait=2);
  a.touch(loc[0]+20, loc[1]-50, touchType='up');
  a.sleep(3)
  res = a.compare()


a.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
if res:
  a.info("TestResult: Pass")
  a.updateResult('Pass')
else:
  a.info("TestResult: Manual")
  a.updateResult('Manual')
