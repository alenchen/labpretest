from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
 
from PScript import *
from time import sleep
import sys
 
 
device = MonkeyRunner.waitForConnection()
print "Connect to device "
easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"
cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"
print 'ensureUnlock'
Telephony.ensureUnlock(device) 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch browser ap'
device.touch(618, 1108, MonkeyDevice.DOWN_AND_UP);
sleep(2)


#in order to check if reset to default
#set disable download image in advance
print 'set disable download image in advance'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)
cci_device.clickText('Settings')
sleep(2)
cci_device.clickText('Bandwidth management')
sleep(2)
easy_device.touch(By.id('id/checkbox'),'downAndUp')
sleep(2)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

#set reset to default
print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'click settings'
cci_device.clickText('Settings')
sleep(2)
print 'click Advanced'
cci_device.clickText('Advanced')
sleep(2)
print 'scrollDown'
Common.scrollDown(device)
sleep(2)
print 'click Reset to default'
cci_device.clickText('Reset to default')
sleep(2)
print 'click OK'
cci_device.clickText('OK')
sleep(3)
#check if load image back to enable
print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(4)
#1115: forbid exception
print 'click settings'
cci_device.clickText('Settings')
sleep(2)
print 'click Bandwidth management'
cci_device.clickText('Bandwidth management')
sleep(2)


print "==============\nAP_Browser_007:\n=============="
if Telephony.isResetDefaultOK(device,594,626):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Failed'



