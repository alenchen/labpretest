from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice

from PScript import *
from time import sleep
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device =EasyMonkeyDevice(device)
print 'Create EasyMonkeyDevice'

cci_device = CCIEasyMonkeyDevice(device)
print 'Create CCIEasyMonkeyDevice'

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

device.startActivity(component='com.android.mms/.ui.ComposeMessageActivity')
sleep(1)

print 'click item 6: recieve a mms message over GPRS/3G'
device.press("KEYCODE_MENU", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click Add subject'
cci_device.clickText('Add subject')
sleep(2)

device.type('Item6')
sleep(2)

print 'click 6,'
#cci_device.clickText('6,')
device.touch(270,220, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'enter phone number'
device.type(sys.argv[4])
sleep(2)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press send, change to 1m'
cci_device.clickText("MMS")

PASS=False
for i in range(5):
  sleep(20)

  print 'wake up device'
  device.wake()
  sleep(2)

  print 'check lockscreen'
  Telephony.ensureUnlock(device) 

  #print 'check database '   Telephony.SentOK(device, "Item6") and
  if Telephony.RecvSubOK(device, "Item6"):
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
    print 'Pass'
    PASS=True
    break

if not PASS :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Manual');
  print 'Manual'

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

