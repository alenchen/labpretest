import sys,os
from PScript import *

c = Calendar.getInstance()

c.init_log(os.path.basename(__file__).split(".")[0])

c.waitForConnection()

c.wake()

c.unLockIfScreenLock()

c.goHome()

c.launchAppList()

c.info('search and click Calendar')
res=False
if c.search_and_click('Calendar'):
  c.info("launch Calendar")
  ori_count =  c.Events_count()
  
  c.info( "add a event")
  if c.menu("New event"):
    c.type("Aaa", id="id/title")
    c.done()

  if ori_count + 1 == c.Events_count():
    res = True

c.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
if res:
  c.info("TestResult: Pass")
  c.updateResult('Pass')
else:
  c.info("TestResult: false")
  c.updateResult('Fail')
