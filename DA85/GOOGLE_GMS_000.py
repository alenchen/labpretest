import sys,os
from PScript import *

g = GoogleGms.getInstance()

Log = Log.getInstance(TAG=os.path.basename(__file__).split(".")[0], levelfilter='iw')

g.waitForConnection()

g.wake()

try:
  # Welcome
  id="id/welcome_title"
  Log.i("wait welcome page")
  for i in range (0, 50):
    if g.visible(id):
      Log.i("find welcome page")
      break
    g.sleep(5)

  if g.visible(id):
    id="id/start"
    Log.i("Welcome page, touch start button")
    g.touch(id, wait=100)
    g.wake()

  # Insert SIM Card
  id="id/title"
  if "Insert SIM card" == g.getText(id):
    Log.i("Insert SIM card page, touch skip button")
    id="id/skip_button"
    g.touch(id, wait=3)
  else:
    Log.i("no Insert SIM card page")

  # wifi setup
  id="id/title"
  if "Select Wi-Fi" == g.getText(id):
    Log.i("Select Wi-Fi page, touch skip button")
    g.clickText('Skip')
    g.sleep(3)
  else:
    Log.i("no Select Wi-Fi page")

  # Got Google?
  id="id/title"
  if g.getText(id).startswith("Got Google"):
    Log.i("Got Google? page, touch No button")
    id="id/yes_button"
    g.touch(id, wait=3)

    Log.i('setup gmail info')
    id="id/username_edit"
    g.type('cci.sys.account@gmail.com', id)

    id="id/password_edit"
    g.type('ilovecci', id)

    Log.i("touch next button")
    id="id/next_button"
    g.touch(id)

    Log.i("touch OK")
    g.clickText("OK")
    g.sleep(30)

    # Entertainment
    id="id/addinstrument_title"
    Log.i("wait entertainment page")
    for i in range (0, 50):
      if "Entertainment" == g.getText(id):
        Log.i("find entertainment page")
        break
      g.sleep(5)

    if "Entertainment" == g.getText(id):
      id="id/skip_button"
      Log.i("Entertainment page, touch Not now button")
      g.touch(id, wait=5)
    else:
      Log.i("no Entertainment page")
    
    # Backup and restore
    id="id/title"
    if "Backup and restore" == g.getText(id):
      id="id/done_button"
      Log.i("Backup and restore page, touch done button")
      g.touch(id, wait=10)
    else:
      Log.i("no Backup and restore page")

  else:
    Log.i("no Got Google? page")

  # Make it Google
  id="id/title"
  if "Make it Google" == g.getText(id):
    Log.i("Make it Google page, touch Not now button")
    id="id/skip_button"
    g.touch(id, wait=3)
  else:
    Log.i("no Make it Google page")

  # Google & location
  id="id/title"
  if "Google & location" == g.getText(id):
    Log.i("Google & location, touch next button")
    id="id/next_button"
    g.touch(id, wait=3)
  else :
    Log.i("no Google & location page")

  # Use Google Location
  id="id/title"
  if "Use Google Location" == g.getText(id):
    Log.i("Use Google Location page, touch next button")
    id="id/next_button"
    g.touch(id, wait=3)
  else:
    Log.i("no Use Google Location page")

  # Date & time
  id="id/title"
  if "Date & time" == g.getText(id):
    Log.i("Date & time page, touch next button")
    id="id/next_button"
    g.touch(id, wait=3)
  else:
    Log.i("no Date & time page")

  # This phone belongs to...
  id="id/title"
  if g.getText(id).startswith("This phone belongs to"):
    Log.i("This phone belongs to... page, fill and touch next botton")
    id="id/first_name_edit"
    g.type('cci', id)

    id="id/last_name_edit"
    g.type('auto_cm_test', id)

    id="id/next_button"
    g.touch(id, wait=10)
  else:
    Log.i("no This phone belongs to... page")

  # Google service
  id="id/title"
  if "Google services" == g.getText(id):
    Log.i("Google services page, touch next botton")
    id="id/next_button"
    g.touch(id, wait=3)
  else:
    Log.i("no Google services page")

  # setup complete
  if "Setup complete" == g.getText("id/title"):
    Log.i("Setup complete page, touch finish button")
    id="id/next_button"
    g.touch(id, wait=30)
  else:
    Log.i("no Setup complete page")

  # Make yourself at home
  Log.i("wait make yourself at home page")
  for i in range (0, 50):
    if g.visible('id/workspace_cling'):
      Log.i("find make yourself at home page")
      break
    g.sleep(2)

  if g.visible('id/workspace_cling'):
    Log.i('Make yourself at home page, click OK button')
    g.clickText('OK')

  g.sleep(2)

  # app list
  Log.i('launch app list')
  g.launchAppList()
  g.sleep(5)

  # Choose some apps
  id='id/cling_dismiss'
  if g.visible(id):
    Log.i('Choose some apps, click OK button')
    g.clickText('OK')
    g.sleep(2)

  g.goHome()

except ValueError:
  Log.w("ValueError: not find id=%s", id)
  pass
