from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
import filecmp
import sys,os,commands
from PScript import *

device = MonkeyRunner.waitForConnection()

easy_device = EasyMonkeyDevice(device)

cci_device = CCIEasyMonkeyDevice(device)

cci_device.setIsFullTextMatch(True)

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def dragToLeft():
    #drag to left
    device.drag((181,756),(543,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((181,756),(543,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((181,756),(543,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((181,756),(543,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((181,756),(543,756),0.1,10)
    MonkeyRunner.sleep(1)

def dragToRight():
    #drag to right
    device.drag((543,756),(181,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((543,756),(181,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((543,756),(181,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((543,756),(181,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((543,756),(181,756),0.1,10)
    MonkeyRunner.sleep(1)

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  MonkeyRunner.sleep(2)
  device.wake()
  MonkeyRunner.sleep(2)
  device.drag(lock_start,lock_end)

print 'wake up device'
device.wake()
MonkeyRunner.sleep(2)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
MonkeyRunner.sleep(2)

#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

res = 0
dragToRight()

#save right workspace picture
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])

dragToLeft()

#save left workspace picture
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])

#click all app 
device.touch(359,1106,'downAndUp')
MonkeyRunner.sleep(2)

#click APPS 
device.touch(65,106,'downAndUp')
MonkeyRunner.sleep(2)

dragToLeft()

#search Calculator
for i in range(5):
    if (cci_device.isTextVisible('Calculator')):
        #get Calculator location
        location = cci_device.getTextLocation('Calculator')
        location_len= len(location)
        location = location[1:location_len-1]
        x = location.split(',')[0]
        y = location.split(',')[1]
        #put Calculator to laucher
        device.touch(int(x,10),int(y,10),'down')
        MonkeyRunner.sleep(1)
        device.touch(int(x,10),int(y,10),'up')
        break
    else:
        device.drag((543,756),(181,756),0.1,10)
        MonkeyRunner.sleep(1)

dragToLeft()

for i in range(5):
   if (cci_device.isTextVisible('Calculator')):
      #get Calculator location
      location = cci_device.getTextLocation('Calculator')
      location_len= len(location)
      location = location[1:location_len-1]
      x = location.split(',')[0]
      y = location.split(',')[1]
      x_int = int(x,10)
      y_int = int(y,10)
      #saving before move icon
      Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
      #move icon to x,y => 100 point
      device.drag((x_int,y_int),(x_int+100,y_int+100),10,10)
      MonkeyRunner.sleep(1)
      #saving after move icon
      Common.takeSnapshot(device, filename=sys.argv[3]+'-4', path=sys.argv[1]+'/'+sys.argv[2])
      #get Calculator location
      location = cci_device.getTextLocation('Calculator')
      location_len= len(location)
      location = location[1:location_len-1]
      x = location.split(',')[0]
      y = location.split(',')[1]
      #put Calculator to laucher
      device.touch(int(x,10),int(y,10),'down')
      MonkeyRunner.sleep(1)
      #get Remove location
      location = cci_device.getTextLocation('Remove')
      location_len= len(location)
      location = location[1:location_len-1]
      Rev_x = location.split(',')[0]
      Rev_y = location.split(',')[1]
      device.touch(int(x,10),int(y,10),'up')
      MonkeyRunner.sleep(1)
      #drag Calculator to Remove
      print "drag to remove....from x="+x+",y="+y+"to x="+Rev_x+",y="+Rev_y
      device.drag((int(x,10),int(y,10)),(int(Rev_x,10),int(Rev_y,10)),10,10)
      MonkeyRunner.sleep(1)
      Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
      Common.updateResult('Manual');
      res = 1
      break
   
   else:
      device.drag((543,756),(181,756),0.1,10)
      MonkeyRunner.sleep(1)

if(res == 0):
   print "Fail"
   Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
   Common.updateResult('Fail');
