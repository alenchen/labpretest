from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
import sys

device = MonkeyRunner.waitForConnection()
print "Connect to device "

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

model = device.getSystemProperty('ro.product.model')
print "The ro.product.model property on device = " + model


cci_device.setIsFullTextMatch(False)

#Battery test , 5% Warning check
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)

device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

device.shell("echo 1 > /sys/module/pm8921_charger/parameters/cci_fake_flag")
MonkeyRunner.sleep(1)
device.shell("echo 2 > /sys/module/pm8921_charger/parameters/cci_fake_charger_status")
MonkeyRunner.sleep(1)
device.shell("echo 0 > /sys/module/pm8921_charger/parameters/cci_fake_usb_present")
MonkeyRunner.sleep(1)
device.shell("echo 4 > /sys/module/pm8921_charger/parameters/cci_fake_capacity")

MonkeyRunner.sleep(2)

if cci_device.isTextVisible('The battery is getting low.'):
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Pass');
	print 'Pass'
else:
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Fail');
	print 'Failed'

MonkeyRunner.sleep(2)
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)
#Original setting recovery
device.shell("echo 0 > /sys/module/pm8921_charger/parameters/cci_fake_flag")
MonkeyRunner.sleep(1)
device.shell("echo -1 > /sys/module/pm8921_charger/parameters/cci_fake_capacity")
MonkeyRunner.sleep(1)
device.shell("echo -1 > /sys/module/pm8921_charger/parameters/cci_fake_charger_status")
MonkeyRunner.sleep(1)
device.shell("echo -1 > /sys/module/pm8921_charger/parameters/cci_fake_usb_present")
MonkeyRunner.sleep(1)
