from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys

#PIN = sys.argv[5]
PIN = '0000' 
#Test8: After reboot, Unit should unlock with entering correct PIN

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 


#input correct SIM PIN
print "input correct SIM PIN"
device.type(PIN)
sleep(1)


#click ok
print "Click OK"
cci_device.clickText('OK')
sleep(1)


#Sim 008 Result
print "Snap shot xxx_008_PINAccept.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'















