from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys
import re
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper

num=sys.argv[4]

print 'RIL_SMSCB_016 test'
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'
print 'create EasyMonkey device'
easy_device = EasyMonkeyDevice(device)
print 'create CCIEasyMonkey device'
cci_device = CCIEasyMonkeyDevice(device)

print 'ensureUnlock'
Telephony.ensureUnlock(device) 
 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#send a msg to DUT itself
#click new sms button
device.touch(60,1130, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#focus on phone number editor  
device.touch(150,200, MonkeyDevice.DOWN_AND_UP);
sleep(1)
    
#input phone number
#device.type(sys.argv[4])
device.type(num)
sleep(1)

#focus on text body editor 
device.touch(150,690, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#input text body
device.type('Detail')
device.press('KEYCODE_SPACE', MonkeyDevice.DOWN_AND_UP)
device.type('test')
sleep(1)

#send buttom
print 'send message, wait 15s'
device.touch(660,610, MonkeyDevice.DOWN_AND_UP);
sleep(15)

#back to hide keyboard
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
#long press received msg 
print 'long press received msg '
device.drag((370,1000), (370,1000), 10, 1) 
sleep(1)

#press Detail button
#S:carrie, no work
#cci_device.clickText('View details')
#sleep(2)
#E:
print 'tick view details'
#cci_device.clickText('View details')
device.touch(345, 745, MonkeyDevice.DOWN_AND_UP)
sleep(2)
#device.touch(360, 780, MonkeyDevice.DOWN_AND_UP)
#sleep(1)

#print 'check text string if existed '
#E: 
if cci_device.isTextVisible('Type:') and cci_device.isTextVisible('Sent:'):
  print 'Type and Sent'
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass')
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed' 
#E:  
# twice back key, return to messaging screen, for more than twice test
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
#click delete all button
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(1)

device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click yes to delete all msgs
device.touch(510, 735, MonkeyDevice.DOWN_AND_UP)
sleep(1)

# back to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

