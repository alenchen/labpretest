from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys

#do network_0 first
#Test 2 Airplan mode work


device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 
sleep(1)

print 'Click Airplan mode (on)'
cci_device.clickText('Airplane mode')
sleep(5)


#Network 002 Result
print "Snap shot xxx_002_AirplanOn.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device) 



print 'Click Airplan mode (off)'
cci_device.clickText('Airplane mode')
sleep(5)



#Network 002 Result
print "Snap shot xxx_002_AirplanOff.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device) 


#press back for test 9
print "back"
device.press('KEYCODE_BACK','downAndUp')




