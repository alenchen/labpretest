from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys


PIN = '0000' 
#sys.argv[5]
PUK = '13805245'
#sys.argv[7]


#Test7: Unit can unlock and change PIN with correct PUK

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

#click Edit for input PUK
#easy_device.touch(By.id('id/pukDisplay'), 'downAndUp')
device.touch(267,290,'downAndUp')
sleep(1)


#input correct PUK
print "input correct SIM PUK"
device.type(PUK)
sleep(1)


#click Edit for input PIN
#easy_device.touch(By.id('id/pinDisplay'), 'downAndUp')
#device.touch(267,420,'downAndUp')
device.touch(267,384,'downAndUp')
sleep(1)



#input New SIM PIN
print "input New SIM PIN"
device.type(PIN)
sleep(1)



#click ok
print "Click OK"
cci_device.clickText('OK')
sleep(5)


#Sim 007 Result
print "Snap shot xxx_007_OKPUK.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'



#reboot for test 8
device.shell("reboot")
sleep(50)



