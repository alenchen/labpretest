from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice 
from PScript import *
from time import sleep
import sys
 
device = MonkeyRunner.waitForConnection()
print "Connect to device "
 
easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"
 
cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"
print 'ensureUnlock'
Telephony.ensureUnlock(device) 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch browser ap'
device.touch(618, 1108, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click bookmark'
cci_device.clickText('Bookmarks')
sleep(2)

print 'click Picasa'
cci_device.clickText('Picasa')
sleep(2)

#load webpage need time
print 'takeSnapshot(1/3):original with images'
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'click settings'
cci_device.clickText('Settings')
sleep(2)
print 'click Bandwidth management'
cci_device.clickText('Bandwidth management')
sleep(2)
print 'click Load images to disable'
easy_device.touch(By.id('id/checkbox'),'downAndUp')
sleep(2)


print 'double back key go back to browser for refreshing'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)
#print 'click refresh'
cci_device.clickText('Refresh')
sleep(3)

print 'takeSnapshot(2/3): disabled images'
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

#
print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'click settings'
cci_device.clickText('Settings')
sleep(2)
print 'click Bandwidth management'
cci_device.clickText('Bandwidth management')
sleep(2)
print 'click Load images to enable'
easy_device.touch(By.id('id/checkbox'),'downAndUp')
sleep(2)
print 'takeSnapshot(3/3): enabled images'
Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
#sleep(2)

print "==============\nAP_Browser_005:\n=============="
Common.updateResult('Manual');
print 'Manual'

