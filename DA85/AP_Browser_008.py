from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys
 
device = MonkeyRunner.waitForConnection()
print "Connect to device "
easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"
cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"
print 'ensureUnlock'
Telephony.ensureUnlock(device) 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'touch browser ap'
device.touch(618, 1108, MonkeyDevice.DOWN_AND_UP);
sleep(2)
print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)


print 'click Bookmarks'
cci_device.clickText('Bookmarks')
sleep(3)

print 'long press one page'
device.drag((151,364),(151,364), 10,1)
sleep(2)
print "==============\nAP_Browser_008:\n=============="
if Telephony.isBookmarkMenuOK(device,50,295):
   cci_device.clickText('Share link')
   sleep(2)
   cci_device.clickText('Messaging')
   sleep(2)
   cci_device.clickText('OK')
   sleep(4)
   if Telephony.isMA(device,95,90):
     Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
     Common.updateResult('Pass');
     print 'Pass'
   else:
     Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
     Common.updateResult('Fail');
     print 'Failed'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Failed'


#clean SMS draf
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)
cci_device.clickText('OK')
sleep(2)
