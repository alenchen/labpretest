
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from PScript import *
from time import sleep
import sys
import os
import commands
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device=EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device=CCIEasyMonkeyDevice("ss",device)
print "Create CCIEasyMonkeyDevice"

print 'wake up device'
device.wake()
sleep(2)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

device.startActivity(component='com.android.settings/.Settings')
sleep(3)

Common.scrollDown(device)
sleep(1)
print 'press Add account'
cci_device.clickText('Add account')
sleep(2)

print 'press Google'
cci_device.clickText('Google')
sleep(3)

print 'press Existing'
device.touch(360, 995, MonkeyDevice.DOWN_AND_UP)
sleep(2)

if not cci_device.isTextVisible('Sign in'):
   device.touch(364, 976, MonkeyDevice.DOWN_AND_UP)
sleep(2)

device.type('cci.riltester@gmail.com')
sleep(2)

device.touch(280, 385, MonkeyDevice.DOWN_AND_UP)
sleep(1)

device.type('cci.riltester')
sleep(2)

print 'press next'
device.touch(636, 660, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press OK'
cci_device.clickText('OK')
sleep(2)
print 'press next'
device.touch(390, 1042, MonkeyDevice.DOWN_AND_UP)
sleep(2)
i=0
for x in range(30):
  i+=1
  if cci_device.isTextVisible('Signing'):
       sleep(1)
  else:
       break
sleep(1)

if i>=30:
   print "Not Signing to Google"
   Common.updateResult('Manual')
   sleep(2)
   Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
   sleep(2)
   sys.exit(1)
else:
    print 'back to previous'
    device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP);
    sleep(2)

    print 'press next'
    device.touch(635, 1124, MonkeyDevice.DOWN_AND_UP)
    sleep(3)

    device.startActivity(component='com.android.contacts/.activities.PeopleActivity')
    sleep(3)

    print 'press first contacts'
    cci_device.clickText('Andy')
    sleep(2)

    print 'press menu'
    device.touch(675, 98, MonkeyDevice.DOWN_AND_UP)
    sleep(2)

    print 'press Share'
    cci_device.clickText('Share')
    sleep(2)

    print 'press choose Gmail'
    cci_device.clickText('Gmail')
    sleep(10)
    Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
    sleep(2)

    print 'press Subject'
    device.touch(200, 415, MonkeyDevice.DOWN_AND_UP)
    sleep(2)
    print 'back to previous'
    device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP);
    sleep(2)
    print 'back to previous'
    device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP);
    sleep(2)
Common.updateResult('Manual')
