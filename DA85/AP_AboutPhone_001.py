import sys,os
from PScript import *


c= Common.getInstance()

c.init_log(os.path.basename(__file__).split(".")[0])

c.waitForConnection()

c.wake()

c.unLockIfScreenLock()

c.goHome()

c.launchAppList()

c.info('search and click Settings')
res=False
if c.search_and_click('Settings'):
  c.info('launch About phone')
  if not c.search_and_click('About phone', before="scrollUp", beforeTimes=3, after="scrollDown"):
    c.info("not find About phone")
  else:
    c.info("take screenshot for Manual")
    c.scroll_and_Snapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2], before="scrollUp", after="scrollDown")    
    res=True

c.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
if res:
  c.info("TestResult: Manual")
  c.updateResult('Manual')
else:
  c.info("TestResult: false")
  c.updateResult('Fail')
