import sys,os
from PScript import *


c= Common.getInstance()

c.init_log(os.path.basename(__file__).split(".")[0] )

c.waitForConnection()

c.wake()

c.unLockIfScreenLock()

c.goHome()

c.launchAppList()

c.info('search and click YouTube')
res=False
if c.search_and_click('YouTube'):
  c.info('launch YouTube')
  if c.visible(id="id/home"):
    c.info('make sure we are at youtube home')
    c.touch(id="id/home")
    c.sleep(0.5)
    c.touch(id="id/home")
    c.sleep(0.5)
  if c.visible(id="id/menu_search"):
    c.touch(id="id/menu_search")
    c.sleep(2)
    c.info('touch search button')
    c.type("g")
    c.type("a")
    c.type("n")
    c.type("g")
    c.type("n")
    c.type("a")
    c.type("m")

    c.touch(667,1123)
    c.sleep(3)
    res=True

c.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
if res:
  c.info("TestResult: Manual")
  c.updateResult('Manual')
else:
  c.info("TestResult: false")
  c.updateResult('Fail')

if c.visible(id="id/home"):
  c.touch(id="id/home")
  c.sleep(0.5)
  c.touch(id="id/home")

