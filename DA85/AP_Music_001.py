from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep
import filecmp

def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

Screen_unlock()

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click Music'
device.startActivity(component='com.android.music/com.android.music.MusicBrowserActivity')
sleep(2)

print 'press Songs'
device.touch(468, 106, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'check music if they exist'
musicExist = device.shell("ls storage/sdcard0/Music").split('\r\n')
if musicExist:
	print 'remove all music in device'
	device.shell("rm /storage/sdcard0/Music/*")
	sleep(2)
	print 'Send Intent'
	device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")
	sleep(2)
else:	
	print 'No music found.'            

print 'No music files'
image = device.takeSnapshot()
image.writeToFile('.tmp1','png')
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

print 'push music into device'
os.system("adb push DA85/music/1.mp3 /storage/sdcard0/Music/")
os.system("adb push DA85/music/2.mp3 /storage/sdcard0/Music/")
os.system("adb push DA85/music/3.mp3 /storage/sdcard0/Music/")
sleep(5)
musicExist = device.shell("ls storage/sdcard0/Music").split('\r\n')
if musicExist:
	print 'Send Intent'
	device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")
	sleep(5)		
else :
	print 'push music fail'

print 'After push music'
image = device.takeSnapshot()
image.writeToFile('.tmp2','png')
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

res = filecmp.cmp('.tmp1','.tmp2')
if  not res:
	print 'Pass'
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Pass');
else :
	print 'Fail'
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Fail');

os.remove(".tmp1")
os.remove(".tmp2")

print 'press Artists'
device.touch(100, 105, MonkeyDevice.DOWN_AND_UP)
sleep(2)

for x in range(1, 3):
  print 'press back key'
  device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
  sleep(1)
  
os.system("adb shell am force-stop com.android.music")  
