from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys


#Go to Mobile networks

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 
sleep(1)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'Go to system settings'
device.startActivity( component='com.android.settings/.Settings' )
sleep(2)

print 'scroll up x3, go to menu top'
Common.scrollUp(device)
sleep(1)
Common.scrollUp(device)
sleep(1)
Common.scrollUp(device)
sleep(1)


print "Click More"
cci_device.clickText('More')

