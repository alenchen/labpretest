from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]


#protect procedure
print 'wake up device'   
device.wake()
sleep(2)

print 'press power key to lock'
device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'wake up device'
device.wake()
sleep(2)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
device.drag(lock_start,lock_end)
sleep(2)
#-------------------------------


if LockScreen.isLock(device):
  print 'unlock screen'
  LockScreen.unLock(device)
  LockScreen.waitUnLock(device) 
 

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)


print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)
sleep(2)


print '~~~Start to test proximity Sensor~~~'
name=device.shell("cat sys/class/input/input1/name")
print name
if 'proximity' in name :
   enable=device.shell("echo 4 > sys/class/input/input1/enable")
   sleep(1)
   prox = device.shell("cat sys/class/input/input1/pdata")
   prox_int=int(prox,10)
   print prox_int
   if prox_int >= 0 :
      if prox_int <= 1023 :
         Common.updateResult('Pass');
         print 'Pass: prox= ' + prox
      else :
         Common.updateResult('Fail');
         print 'Fail: LUX = ' + prox
   else :
      Common.updateResult('Fail');
      print 'Fail: LUX = ' + prox
else :
   Common.updateResult('Fail');
   print 'Fail: input0 name = ' + name



