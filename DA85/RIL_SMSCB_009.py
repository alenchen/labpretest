from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys
import re
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper

num=sys.argv[4]

print 'mo_sms_013 test'
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

print 'create EasyMonkey device'
easy_device = EasyMonkeyDevice(device)

print 'create CCIEasyMonkey device'
cci_device = CCIEasyMonkeyDevice(device)

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#Telephony.clearSms(device)

print 'press settings'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(5)
#device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
cci_device.clickText("Settings")
sleep(5)


print 'press manage SIM, wait longer for refreshing'
device.touch(360,890, MonkeyDevice.DOWN_AND_UP);
sleep(4)

#we can compare a fixed text in a msg
#after deleting the msg, we found if it is still visible
#by using isvisible(), see mo_sms_013_v2.py
bool1 = cci_device.isTextVisible('Sim test')
_bool1 = str(bool1) 
print 'Sim test visiable?  ' + _bool1

print 'capture one snapshot'
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])


#long press msg
device.drag((370,1110), (370,1110), 10, 1) 
sleep(1)

#press delete, wait longer for SIM card processing
device.touch(360,660, MonkeyDevice.DOWN_AND_UP);
sleep(1)
device.touch(510,740, MonkeyDevice.DOWN_AND_UP);


for x in range(5):
    sleep(2)
    #check text here
    print 'check if SIM is deleted'
    bool2 = cci_device.isTextVisible('Sim test')
    _bool2 = str(bool2)
    print 'Sim test visiable?  ' + _bool2
    if bool2:
       break

if bool1 and not bool2:
    Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass')
    print 'Pass' 
else:
    Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Manual')
    print 'Manual'


# twice back key, return to messaging screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

# back to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)






