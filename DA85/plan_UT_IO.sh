# Part 1: Define mail list
add_maillist herman_lin
add_maillist ethan_hsiao
add_maillist brandon_chang
add_maillist louis_chiu
add_maillist changyan_lee
add_maillist jeff_chen


# Part 2: Define test case
# ====== Group - IO_FM ======
#insert_testcase "IO_FM_001" "IO" "IO_FM_001.py" "" 1
insert_testcase "IO_FM_002_1" "IO" "IO_FM_001.py" "" 1
insert_testcase "IO_FM_002_2" "IO" "IO_FM_002.py" "" 1
#insert_testcase "IO_FM_003" "IO" "IO_FM_003.py" "" 1
#insert_testcase "IO_FM_004" "IO" "IO_FM_004.py" "" 1

# ====== Group - IO_NFC ======
insert_testcase "IO_NFC_001" "IO" "IO_NFC_001.py" "" 1
#insert_testcase "IO_NFC_002" "IO" "IO_NFC_002.py" "" 1
#insert_testcase "IO_NFC_003" "IO" "IO_NFC_003.py" "" 1
#insert_testcase "IO_NFC_004" "IO" "IO_NFC_004.py" "" 1
#insert_testcase "IO_NFC_005" "IO" "IO_NFC_005.py" "" 1

# ====== Group - IO_SD ======
insert_testcase "IO_SD_001" "IO" "IO_SD_001.py" "" 1
#insert_testcase "IO_SD_002" "IO" "IO_SD_002.py" "" 1
#insert_testcase "IO_SD_003" "IO" "IO_SD_003.py" "" 1
insert_testcase "IO_SD_004" "IO" "IO_SD_004.py" "" 1
insert_testcase "IO_SD_005" "IO" "IO_SD_005.py" "" 1
insert_testcase "IO_SD_006" "IO" "IO_SD_006.py" "" 1
insert_testcase "IO_SD_007" "IO" "IO_SD_007.py" "" 1
#insert_testcase "IO_SD_008" "IO" "IO_SD_008.py" "" 1
insert_testcase "IO_SD_009" "IO" "IO_SD_009.py" "" 1
insert_testcase "IO_SD_010" "IO" "IO_SD_010.py" "" 1
#insert_testcase "IO_SD_011" "IO" "IO_SD_011.py" "" 1
#insert_testcase "IO_SD_012" "IO" "IO_SD_012.py" "" 1
#insert_testcase "IO_SD_013" "IO" "IO_SD_013.py" "" 1

# ====== Group - IO_Sensor ======
#insert_testcase "IO_Sensor_001" "IO" "IO_Sensor_001.py" "" 1
#insert_testcase "IO_Sensor_002" "IO" "IO_Sensor_002.py" "" 1
#insert_testcase "IO_Sensor_003" "IO" "IO_Sensor_003.py" "" 1
#insert_testcase "IO_Sensor_004" "IO" "IO_Sensor_004.py" "" 1
#insert_testcase "IO_Sensor_005" "IO" "IO_Sensor_005.py" "" 1
insert_testcase "IO_Sensor_006" "IO" "IO_Sensor_006.py" "" 1
#insert_testcase "IO_Sensor_007" "IO" "IO_Sensor_007.py" "" 1
insert_testcase "IO_Sensor_008" "IO" "IO_Sensor_008.py" "" 1
#insert_testcase "IO_Sensor_009" "IO" "IO_Sensor_009.py" "" 1

# ====== Group - IO_USB ======
#insert_testcase "IO_USB_001" "IO" "IO_USB_001.py" "" 1
#insert_testcase "IO_USB_002" "IO" "IO_USB_002.py" "" 1
insert_testcase "IO_USB_003" "IO" "IO_USB_003.py" "" 1
#insert_testcase "IO_USB_004" "IO" "IO_USB_004.py" "" 1
#insert_testcase "IO_USB_005" "IO" "IO_USB_005.py" "" 1
#insert_testcase "IO_USB_006" "IO" "IO_USB_006.py" "" 1
insert_testcase "IO_USB_007" "IO" "IO_USB_007.py" "" 1
#insert_testcase "IO_USB_008" "IO" "IO_USB_008.py" "" 1
#insert_testcase "IO_USB_009" "IO" "IO_USB_009.py" "" 1
#insert_testcase "IO_USB_010" "IO" "IO_USB_010.py" "" 1

# ====== Group - IO_BT ======
insert_testcase "IO_BT_001" "IO" "IO_BT_001.py" "" 1
insert_testcase "IO_BT_002" "IO" "IO_BT_002.py" "" 1
insert_testcase "IO_BT_003" "IO" "IO_BT_003.py" "" 1
#insert_testcase "IO_BT_004" "IO" "IO_BT_004.py" "" 1
#insert_testcase "IO_BT_005" "IO" "IO_BT_005.py" "" 1
#insert_testcase "IO_BT_006" "IO" "IO_BT_006.py" "" 1
#insert_testcase "IO_BT_007" "IO" "IO_BT_007.py" "" 1
#insert_testcase "IO_BT_008" "IO" "IO_BT_008.py" "" 1
#insert_testcase "IO_BT_009" "IO" "IO_BT_009.py" "" 1
#insert_testcase "IO_BT_010" "IO" "IO_BT_010.py" "" 1
#insert_testcase "IO_BT_011" "IO" "IO_BT_011.py" "" 1
#insert_testcase "IO_BT_012" "IO" "IO_BT_012.py" "" 1
#insert_testcase "IO_BT_013" "IO" "IO_BT_013.py" "" 1
insert_testcase "IO_BT_014" "IO" "IO_BT_014.py" "" 1

# ====== Group - IO_GPS ======
#insert_testcase "IO_GPS_001" "IO" "IO_GPS_001.py" "" 1
#insert_testcase "IO_GPS_002" "IO" "IO_GPS_002.py" "" 1

# ====== Group - IO_WiFi ======
insert_testcase "IO_WiFi_001" "IO" "IO_WiFi_001.py" "" 1
#insert_testcase "IO_WiFi_002_1" "IO" "IO_WiFi_002.py" "" 1
#insert_testcase "IO_WiFi_002_2" "IO" "IO_WiFi_003.py" "" 1
#insert_testcase "IO_WiFi_003" "IO" "IO_WiFi_003.py" "" 1
#insert_testcase "IO_WiFi_004" "IO" "IO_WiFi_004.py" "" 1
#insert_testcase "IO_WiFi_005" "IO" "IO_WiFi_005.py" "" 1
#insert_testcase "IO_WiFi_006" "IO" "IO_WiFi_006.py" "" 1
#insert_testcase "IO_WiFi_007" "IO" "IO_WiFi_007.py" "" 1
#insert_testcase "IO_WiFi_008" "IO" "IO_WiFi_008.py" "" 1
#insert_testcase "IO_WiFi_009" "IO" "IO_WiFi_009.py" "" 1
