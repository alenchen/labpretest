from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands,filecmp
from PScript import *
from time import sleep


print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

print 'wake up device'   
device.wake()
sleep(2)
print 'press power key to lock'
device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'wake up device'
device.wake()
sleep(2)
print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
device.drag(lock_start,lock_end)
sleep(2)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP) #press home key
#device.touch(350, 1220, MonkeyDevice.DOWN_AND_UP); #press home key
sleep(2)


print 'press back key'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)


print 'press back key'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press back key'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)


print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch system settings'
device.touch(352, 1155, MonkeyDevice.DOWN_AND_UP);
sleep(2)

device.drag((350, 180), (350, 1110), 0.1, 10) #prevent it cannot find Bluetooth string

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'click Bluetooth'
cci_device.clickText('Bluetooth')
sleep(1)

print 'ready to turn on Bluetooth'

if Bluetooth.isBtOn_DA85(device):
	print 'Bluetooth is on now'
	print 'press to turn off Bluetooth'
	device.touch(550, 95, MonkeyDevice.DOWN_AND_UP); #press ON to turn OFF
	sleep(3)
	print 'press to turn on Bluetooth'
	device.touch(550, 95, MonkeyDevice.DOWN_AND_UP); #press OFF to turn ON
else:
	print 'Bluetooth is off now'
	print 'turning on Bluetooth'
	device.touch(550, 95, MonkeyDevice.DOWN_AND_UP); #press OFF to turn ON

sleep(5)

print 'ready to change name'
if Bluetooth.isBtOn_DA85(device):
	device.touch(680, 1150, MonkeyDevice.DOWN_AND_UP); #press "..." to change name
	sleep(2)
	device.touch(500, 750, MonkeyDevice.DOWN_AND_UP); #press "Rename phone"
	sleep(2)
	device.touch(620, 370, MonkeyDevice.DOWN_AND_UP); #press the name space at last to erase original name
	sleep(2)
	device.touch(340, 525, MonkeyDevice.DOWN_AND_UP); #press delete
	sleep(2)
	device.touch(30, 800, MonkeyDevice.DOWN_AND_UP); #press q to enter q
	sleep(2)
	device.touch(30, 800, MonkeyDevice.DOWN_AND_UP); #press q to enter q
	sleep(2)
	device.touch(500, 460, MonkeyDevice.DOWN_AND_UP); #press "Rename" to really change name
	sleep(2)
	if cci_device.isTextVisible('qq'):
		print 'Pass'
		Common.updateResult('Pass');
	else:
		print 'Faied-1'
		Common.updateResult('Fail');
else:
	print 'Failed-2'
	Common.updateResult('Fail');
