from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands, filecmp
from PScript import *

print 'Wait connect to device'
device = MonkeyRunner.waitForConnection()

print 'Wake up device'
device.wake()
sleep(1)

print 'Create easymonkeydevice'
easy_device = EasyMonkeyDevice(device)
sleep(1)

print 'Create CCI easymonkeydevice'
cci_device = CCIEasyMonkeyDevice(device)
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

####################
# test case start
####################

Telephony.turnOnAirplaneMode(device, cci_device)

print 'press Home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'launch Dialtacts using startActivity'
device.startActivity(component='com.android.contacts/.activities.DialtactsActivity')
sleep(5)

print "touch Dial icon"
device.touch(120, 100, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'input Emergency number'
input_no = '112'
device.type(input_no)

print 'touch Dial button'
easy_device.touch(By.id('id/dialButton'),'downAndUp')

sleep(10)

if Telephony.isCallState(device):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass')
  print 'Pass'

  print 'touch End button'
  easy_device.touch(By.id('id/endButton'),'downAndUp')
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'
