from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice

from PScript import *
from time import sleep
import sys
import os

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device =EasyMonkeyDevice(device)
print 'Create EasyMonkeyDevice'

cci_device = CCIEasyMonkeyDevice(device)
print 'Create CCIEasyMonkeyDevice'

print 'check lockscreen'
Telephony.ensureUnlock(device)   

device.startActivity(component='com.android.mms/.ui.ConversationList')
sleep(2)

print 'Settings'
device.startActivity(component='com.android.settings/.ApnSettings')
sleep(2)

print 'set MMS APN to wrong one'
#cci_device.clickText("fetnet01")
device.touch(315,740, MonkeyDevice.DOWN_AND_UP);
sleep(2)
#forbid click none, change sleep 1 to 2
#20121029

print 'press APN'
cci_device.clickText("APN")
sleep(1)

print 'modify MMS APN'
for i in range(10):
    device.press("KEYCODE_FORWARD_DEL", MonkeyDevice.DOWN_AND_UP)

sleep(2)
print 'type fetnet00'
device.type("fetnet00")
sleep(2)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)
#forbid click none, change sleep 1 to 2
#20121029

print 'press OK'
cci_device.clickText("OK")
sleep(3)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click item 5: modify mms apn to wrong setting, try to send it.'
cci_device.clickText("6, Draft")
sleep(2)

print 'click 6,'
#cci_device.clickText('6,')
#device.touch(315,740, MonkeyDevice.DOWN_AND_UP);
device.touch(270,220, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'delete 6,'
for i in range(5):
    device.press("KEYCODE_DEL", MonkeyDevice.DOWN_AND_UP)

print 'enter phone number'
device.type(sys.argv[4])
sleep(2)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)

print 'press send'
cci_device.clickText("MMS")

PASS=False
for i in range(1):
  sleep(20)

  print 'wake up device'
  device.wake()
  sleep(2)

  print 'check lockscreen'
  Telephony.ensureUnlock(device) 

  #print 'check database '
  if Telephony.SentOK(device, "Item5"):
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Manual');
    print 'Manual'
    PASS=True
    break

if not PASS:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
  print 'Pass'

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)

print 'Settings'
device.startActivity(component='com.android.settings/.ApnSettings')
sleep(1)

print 'set MMS APN to wrong one'
#cci_device.clickText("fetnet01")
device.touch(315,740, MonkeyDevice.DOWN_AND_UP);
sleep(1)

print 'press APN'
cci_device.clickText("APN")
sleep(1)

print 'reset MMS APN'
for i in range(10):
    device.press("KEYCODE_FORWARD_DEL", MonkeyDevice.DOWN_AND_UP)

device.type("fetnet01")

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)

print 'press OK'
cci_device.clickText("OK")
sleep(3)

os.system("tools/adb pull /data/data/com.android.providers.telephony/databases")

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

