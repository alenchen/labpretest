
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device=EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device=CCIEasyMonkeyDevice("ss",device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press Contacts Icon'
cci_device.clickText('People')
sleep(2)

cci_device.clickText('Andy')
sleep(2)

print 'press MOBILE text'
cci_device.clickText('OTHER')
sleep(2)
cci_device.clickText('MOBILE')
sleep(2)

if cci_device.isTextVisible('DIALING'):
     print 'Edit Phone pass'
else:
     print 'Edit Phone failure'
sleep(2)

Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
print 'press Dial off'
device.touch(340, 976, MonkeyDevice.DOWN_AND_UP);
sleep(1)

device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'press HOME String'
cci_device.clickText('HOME')
sleep(5)

print 'press Gmail'
device.touch(498, 648, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'press Just once'
cci_device.clickText('Just once');
sleep(7)

if cci_device.isTextVisible('Compose'):
      print 'Edit mail pass'
      print 'press Subject'
      device.touch(200, 415, MonkeyDevice.DOWN_AND_UP)
      sleep(2)
      print 'press Back'
      device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
      sleep(2)
      device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
      sleep(2)
else:
      print 'Edit mail failure'
      device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
      sleep(2)
sleep(2)

Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

print 'press MMS picture'
device.touch(665, 645, MonkeyDevice.DOWN_AND_UP)
sleep(2)

Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

print 'press Back'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press Back'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)
Common.updateResult('Manual')
