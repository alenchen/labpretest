from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands
from PScript import *
import filecmp

def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

def Push_picture(file_name):
	print 'remove all pictures in device'
	device.shell("rm /storage/sdcard0/Pictures/*")
	sleep(2)
	device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")
	sleep(2)

	print 'push picture into device'
	os.system("adb push DA85/picture/"+file_name+" /storage/sdcard0/Pictures/")
	sleep(5)

	pictureExist = device.shell("ls storage/sdcard0/Pictures").split('\r\n')
	if pictureExist:
		print 'Send Intent'
		device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")
		sleep(5)		
	else :
		print 'push pictures fail'

def Select_picture():
	print 'click Album "Pictures"'
	device.touch(345,700, MonkeyDevice.DOWN_AND_UP)
	sleep(2)

	print 'click picture'
	device.touch(350,640, MonkeyDevice.DOWN_AND_UP)
	sleep(2)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
    MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
    sys.exit(1)
elif len(devices) == 1:
    choice = 0
else:
    choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

print 'wake up device'
device.wake()
sleep(2)

Screen_unlock()
  
print 'check sdcard in device'
pathExist = device.shell("ls storage/sdcard0").split('\r\n')
if pathExist is None :
	print 'No sdcard found.'
	Common.updateResult('Fail');
	sys.exit(1)
		
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'launch app list'
device.touch(355, 1100, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'Launch Gallery'
cci_device.clickText('Gallery')
sleep(5)
  
print 'remove all media files in device'
device.shell("rm -r /storage/sdcard0/Pictures/*")
device.shell("rm -r /storage/sdcard0/Music/*")
device.shell("rm -r /storage/sdcard0/DCIM/Camera/*")
device.shell("rm -r /storage/sdcard0/Movies/*")
sleep(1)
print 'Send Intent'
device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")
sleep(2)

Push_picture('2.jpg')

Select_picture()
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])

print 'click BACK'
for x in range(0, 2):
    device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)
    sleep(0.5)
        
print 'push BMP into device'
Push_picture('*.bmp')
        
Select_picture()
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])

print 'click BACK'
for x in range(0, 2):
    device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)
    sleep(0.5)
        
print 'push PNG into device'
Push_picture('*.png')
        
Select_picture()
Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])

print 'click BACK'
for x in range(0, 2):
    device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)
    sleep(0.5)
        
print 'push GIF into device'
Push_picture('*.gif')        

Select_picture()
Common.takeSnapshot(device, filename=sys.argv[3]+'-4', path=sys.argv[1]+'/'+sys.argv[2])

print 'click BACK'
for x in range(0, 5):
    device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)
    sleep(0.5)
