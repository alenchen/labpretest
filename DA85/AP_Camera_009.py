from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands
import filecmp
from PScript import *

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
easy_device = EasyMonkeyDevice(device)
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]

if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  sleep(2)
  device.wake()
  sleep(1)
  device.drag(lock_start,lock_end)

print 'wake up device'
device.wake()
sleep(2)

device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
#lock_start = (270, 755)
#lock_end = (470, 755)
screenUnlock(device,lock_start, lock_end)
sleep(2)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'launch app list'
device.touch(360, 1105, MonkeyDevice.DOWN_AND_UP)
#device.touch(285, 830, MonkeyDevice.DOWN_AND_UP)
sleep(1)

for x in range(1, 8):
  print 'scroll left'
  Common.scrollLeft(device);
  sleep(1)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)
cci_device.setIsFullTextMatch(True)

#print 'scroll right'
#Common.scrollRight(device);
#sleep(1)

print 'Launch Settings'

device.startActivity(component='com.android.settings/.Settings')
#i = 0 

#while True :
#  if cci_device.clickText('Settings') :
#     break
#  if i > 4 :
#     print 'Can not find settings, test fail'
#     Common.updateResult('Fail')
#     sys.exit(1)
#     break
#  i += 1
#  Common.scrollRight(device)
#device.touch(475, 765, MonkeyDevice.DOWN_AND_UP)
sleep(3)

print 'drag up'
start_point = (300, 1000)
end_point = (300, 150)
device.drag(end_point,start_point )
sleep(2)

print 'turn wifi'
rect = (555, 235, 90, 45) # x, y ,w ,h  wifi rect
image = device.takeSnapshot()
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res = filecmp.cmp('.tmp', 'DA85/Camera/wifi_on.png');
os.remove(".tmp")

if res :
  print 'wifi already enable'
else :
  device.touch(580, 220, MonkeyDevice.DOWN_AND_UP)
  print 'wifi enable'
sleep(3)

print 'drag down'
start_point = (300, 1000)
end_point = (300, 150)
device.drag(start_point,end_point)
sleep(2)
device.drag(start_point,end_point)
sleep(2)

print 'click Location Service'
device.touch(335, 225, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'check google location services'
rect = (598, 251, 30, 30)
image = device.takeSnapshot()
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res = filecmp.cmp('.tmp', 'DA85/Camera/google_location_service.png');
os.remove(".tmp")
if res :
  print 'check enable service'
else :
  device.touch(610, 265, MonkeyDevice.DOWN_AND_UP)
  sleep(1)
  #print 'click agree'
  #device.touch(385, 615, MonkeyDevice.DOWN_AND_UP)
  print 'enable location services'
sleep(2)

print 'check GPS'
rect = (598, 445, 30, 30)
image = device.takeSnapshot()
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res = filecmp.cmp('.tmp', 'DA85/Camera/GPS.png');
os.remove(".tmp")
if res :
  print 'check enable GPS'
else :
  device.touch(610, 460, MonkeyDevice.DOWN_AND_UP)
  sleep(1)
 # print 'click agree'
  #device.touch(390, 575, MonkeyDevice.DOWN_AND_UP)
  print 'enable GPS services'
sleep(2)



print 'check location and google search'
rect = (598, 719, 30, 30)
image = device.takeSnapshot()
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res = filecmp.cmp('.tmp', 'DA85/Camera/location_and_google_search.png');
os.remove(".tmp")
if res :
  print 'check enable location_and_google_search'
else :
  device.touch(610, 735, MonkeyDevice.DOWN_AND_UP)
  sleep(1)
 # print 'click agree'
  #device.touch(390, 575, MonkeyDevice.DOWN_AND_UP)
  print 'enable location_and_google_search services'
sleep(2)

print 'back to home'
for x in range(1, 5):
  device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
  sleep(1)
sleep(2)

print 'launch app list'
#device.touch(360, 1105, MonkeyDevice.DOWN_AND_UP)
device.touch(360, 1105, MonkeyDevice.DOWN_AND_UP)
sleep(1)

for x in range(1, 8):
  print 'scroll left'
  Common.scrollLeft(device);
  sleep(1)


print 'Launch camera'
cci_device.clickText('Camera')
sleep(3)

print ' restore defaults'
print ' click setting '
device.touch(55, 910, MonkeyDevice.DOWN_AND_UP)
sleep(2)
print ' click setting3 button '
device.touch(55, 910, MonkeyDevice.DOWN_AND_UP)
sleep(1)
print ' click restore defaults button '
device.touch(325, 800, MonkeyDevice.DOWN_AND_UP)
sleep(1)
print ' click  OK button '
device.touch(560, 650, MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'enable store location'
print ' click setting1 button '
device.touch(240,915, MonkeyDevice.DOWN_AND_UP)
sleep(1)
print ' click store location '
device.touch(575,150, MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'camera take a picture'
device.touch(350, 1075, MonkeyDevice.DOWN_AND_UP);
#device.touch(335, 1070, MonkeyDevice.DOWN_AND_UP);
#easy_device.touch(By.id('id/shutter_button'),'downAndUp')
sleep(3)

print 'click thumbnail button'
device.touch(640, 1080, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#print 'into gallery'
#device.touch(515, 670, MonkeyDevice.DOWN_AND_UP);
#sleep(1)

print 'click action button'
device.touch(670, 55, MonkeyDevice.DOWN_AND_UP);
sleep(1)

print 'click show on map'
device.touch(500, 815, MonkeyDevice.DOWN_AND_UP);
sleep(25)
device.touch(200, 860, MonkeyDevice.DOWN_AND_UP);

device.wake()

test_res = False
print 'check google map launched'
for x in xrange(1,5):
  if easy_device.visible(By.id('id/btn_my_location')):
    test_res = True
  if easy_device.visible(By.id('id/split_action_bar')):
    test_res = True
  if test_res :
    break
  sleep(5)

print "==============\n Show on map:\n=============="
if test_res:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Fail'


device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')
device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")

for x in range(1, 5):
  device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
  sleep(1)

