from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys

#Test1: Can't change PIN with entering wrong PIN

PIN='0000'
wrongPIN='5678'
newPIN='1234'

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

#Enable Lock SIM Card
easy_device.touch(By.id('id/checkbox'), 'downAndUp')
sleep(1)


#input correct SIM PIN
print "input correct simPIM"
device.type(PIN)
sleep(3)


#click ok
print "Click OK"
device.touch(528,590,'downAndUp')
#cci_device.clickText('OK')
#easy_device.touch(By.id('id/button1'), 'downAndUp')
sleep(3)


#Click Change SIM PIN
print "Click Change SIM PIN"
cci_device.clickText('Change SIM PIN')
sleep(1)


#input wrong SIM PIN
print "input wrong simPIM"
device.type(wrongPIN)
sleep(3)

#click ok
print "Click OK"
device.touch(528,590,'downAndUp')
sleep(3)


#input New SIM PIN
print "input New SIM PIN"
device.type(newPIN)
sleep(3)

#click ok
print "Click OK"
device.touch(528,590,'downAndUp')
sleep(3)


#Re=input New SIM PIN
print "Re-input New SIM PIN"
device.type(newPIN)
sleep(3)

#click ok
print "Click OK"
device.touch(528,590,'downAndUp')
sleep(1)

#Sim 001 Result
print "Snap shot xxx_001_PINnoChange.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'

















