import sys,os
from PScript import *

g = GoogleGms.getInstance()

Log = Log.getInstance(TAG=os.path.basename(__file__).split(".")[0], levelfilter='iw')

g.waitForConnection()

g.wake()

g.unLockIfScreenLock()

g.goHome()

g.launchAppList()

Log.i('search and click google YouTube')
res=False
if g.search_and_click('YouTube'):
  Log.i("launch YouTube")
  g.sleep(7)

  if g.visible(id="id/tutorial_view"):
    g.press('KEYCODE_BACK')
    Log.i("find YouTube tutorial, and close it")
    g.sleep(1)
    
  if g.visible(id="id/menu_search"): 
    g.touch(id="id/menu_search")
    Log.i("touch search button")
    g.sleep(1)

  Log.i("search psy gangnam style")
  g.input('psy gangnam style')
  
  Log.i('touch first item')
  g.touch(200, 360, wait=40)
  g.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  g.goHome(backtimes=5)
  res=True

if res:
  Log.i("TestResult: Manual")
  g.updateResult('Manual')
else:
  Log.i("TestResult: Fail")
  g.updateResult('Fail')

