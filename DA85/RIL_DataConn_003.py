from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands, filecmp
from PScript import *

print 'Wait connect to device'
device = MonkeyRunner.waitForConnection()

print 'Wake up device'
device.wake()
sleep(1)

print 'Create easymonkeydevice'
easy_device = EasyMonkeyDevice(device)
sleep(1)

print 'Create CCI easymonkeydevice'
cci_device = CCIEasyMonkeyDevice(device)
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

####################
# local function
####################

def donwloadFileFromHinet():
  print 'press Home key'
  device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
  sleep(2)

  print 'launch http://speed.hinet.net/index_test01.htm'
  device.startActivity(component='com.android.browser/.BrowserActivity', uri='http://speed.hinet.net/index_test01.htm')
  sleep(20)

  print('touch file')
  device.touch(550, 350, MonkeyDevice.DOWN)
  sleep(1)
  device.touch(550, 350, MonkeyDevice.UP)
  sleep(2)

  print 'click Open'
  cci_device.clickText('Open')
  sleep(2)

def sendMMS():
  print 'press Home key'
  device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
  sleep(2)

  print 'launch MMS using startActivity'
  device.startActivity(component='com.android.mms/.ui.ConversationList')
  sleep(5)

  Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])

  print 'touch Compose'
  easy_device.touch(By.id('id/action_compose_new'),'downAndUp')
  sleep(2)

  print 'input To'
  easy_device.touch(By.id('id/recipients_editor'),'downAndUp')
  sleep(2)
  input_to = '0916611562'
  device.type(input_to)
  sleep(2)

  print 'touch Settings icon'
  device.touch(665, 100, MonkeyDevice.DOWN_AND_UP)
  sleep(2)

  print 'click Add subject'
  cci_device.clickText('Add subject')
  sleep(5)

  print 'input Subject'
  input_subject = 'This_is_a_test'
  device.type(input_subject)
  sleep(2)

  print 'touch Send'
  easy_device.touch(By.id('id/send_button_mms'),'downAndUp')
  sleep(2)

  print "press Back key"
  device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
  sleep(2)
  device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
  sleep(2)

def showStatusBar():
  device.drag((360,10),(360,1010),0.5,10)

####################
# test case start
####################

res = Telephony.CheckSDcard(device)
if res == False:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'This testcase need insert SD card'
  print 'Failed'
  sys.exit(1)

donwloadFileFromHinet()

print 'scroll-down Status Bar'
showStatusBar()
sleep(2)
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])

sendMMS()

sleep(30)

Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])

showStatusBar()
sleep(5)
Common.takeSnapshot(device, filename=sys.argv[3]+'-4', path=sys.argv[1]+'/'+sys.argv[2])

Common.updateResult('Manual')
print 'Manual'

print 'touch Clear button'
easy_device.touch(By.id('id/clear_all_button'),'downAndUp')
sleep(2)

print 'press Home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
