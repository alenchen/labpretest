
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys
import os
  
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device=EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device=CCIEasyMonkeyDevice("ss",device)
print "Create CCIEasyMonkeyDevice"

print 'wake up device'
device.wake()
sleep(2)

print 'reboot...'
device.shell("reboot")
sleep(50)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device=EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device=CCIEasyMonkeyDevice("ss",device)
print "Create CCIEasyMonkeyDevice"

print 'wake up device'
device.wake()
sleep(2)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'long-press Power key'
device.press('KEYCODE_POWER', MonkeyDevice.DOWN)
sleep(2)

if cci_device.isTextVisible('Airplane mode is OFF'):
   print "press Back key"
   device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
   sleep(1)
else:
    print 'press turn off airplanMode'
    cci_device.clickText('Airplane mode')
    sleep(15)

print 'press Contacts Icon'
cci_device.clickText('People')
sleep(2)

Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)
if cci_device.isTextVisible('Make it Google'):
   print 'press Not now'
   device.touch(369, 1110, MonkeyDevice.DOWN_AND_UP);
   sleep(2)

device.touch(368, 110, MonkeyDevice.DOWN_AND_UP)
sleep(2)

if cci_device.isTextVisible('Create a new contact'):
   print 'press Create a new contact'
   cci_device.clickText('Create a new contact')
   sleep(2)
else:
    print 'press menu'
    device.touch(673, 1173, MonkeyDevice.DOWN_AND_UP)
    sleep(1)

if cci_device.isTextVisible('Accounts'):
   cci_device.clickText('Accounts')
   sleep(1)
if cci_device.isTextVisible('@gmail.com') or cci_device.clickText('cci.sys.account'):
   cci_device.clickText('@gmail.com')
   cci_device.clickText('cci.sys.account')
   sleep(1)
   device.touch(672, 115, MonkeyDevice.DOWN_AND_UP)
   sleep(1)
else:
   print "press Back key"
   device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
   sleep(1)


if cci_device.isTextVisible('Remove account'):
   print 'press Remove account'
   cci_device.clickText('Remove account')
   sleep(1)
   device.touch(490, 777, MonkeyDevice.DOWN_AND_UP)
   sleep(3)

if cci_device.isTextVisible('Make it Google'):
   print 'press Not now'
   device.touch(369, 1110, MonkeyDevice.DOWN_AND_UP);
   sleep(2)

if cci_device.isTextVisible('Create a new contact'):
   print 'press Create a new contact'
   cci_device.clickText('Create a new contact')
   sleep(1)

if cci_device.isTextVisible('Set up my profile'):
   print 'press add Contact'
   device.touch(365, 1157, MonkeyDevice.DOWN_AND_UP)
   sleep(2)


if cci_device.isTextVisible('Keep local'):
   print 'press Keep local'
   cci_device.clickText('Keep local')
sleep(1)

print 'Type String field'
device.type('Shinjyo')
sleep(1)

print 'press Phone'
device.touch(120, 555, MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'Type String field'
device.type('0928123456')
sleep(2)

print 'press Done'
cci_device.clickText('Done')
sleep(2)

Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
sleep(1)
if cci_device.isTextVisible('Shinjyo'):
      print 'Add Contacts pass'
      Common.updateResult('Pass')
else:
      print 'Add Contacts failure'
      Common.updateResult('Fail')
sleep(1)

print 'press Back'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
