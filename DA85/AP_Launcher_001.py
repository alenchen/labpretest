from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
import filecmp
import sys,os,commands
from PScript import *

device = MonkeyRunner.waitForConnection()

easy_device = EasyMonkeyDevice(device)

cci_device = CCIEasyMonkeyDevice(device)

cci_device.setIsFullTextMatch(True)

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  MonkeyRunner.sleep(2)
  device.wake()
  MonkeyRunner.sleep(2)
  device.drag(lock_start,lock_end)

print 'wake up device'
device.wake()
MonkeyRunner.sleep(2)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
MonkeyRunner.sleep(2)

#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

#Click Menu key
device.press('KEYCODE_MENU','downAndUp')
MonkeyRunner.sleep(2)

#come to setting
cci_device.clickText('System settings')
MonkeyRunner.sleep(2)

#drag to Date & time location
device.drag((359,479),(359,1019),0.1,10)
MonkeyRunner.sleep(1)

#come to Display
cci_device.clickText('Display')
MonkeyRunner.sleep(2)

#selected Sleep
cci_device.clickText('Sleep')
MonkeyRunner.sleep(2)

#selected 30 minutes
cci_device.clickText('30 minutes')
MonkeyRunner.sleep(2)

#Click Back key
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)

#drag to Date & time location
device.drag((359,1019),(359,497),0.1,10)
MonkeyRunner.sleep(1)

#enter Data & time
cci_device.clickText('Date & time')
MonkeyRunner.sleep(2)

#check Automatic date & time is click
image = device.takeSnapshot()
rect = (597, 193, 31, 31) # x, y ,w ,h
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res = filecmp.cmp('.tmp', 'DA85/sys/img/AP_Launcher_001/AP_Launcher_001_01.png')
os.remove(".tmp")
if(res):
  print "Automatic date & time had click"
  #click Automatic date & time
  device.touch(613,206,'downAndUp')
  MonkeyRunner.sleep(2) 

#check Use 24-hour format is click
image = device.takeSnapshot()
rect = (597, 838, 31, 31) # x, y ,w ,h
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res = filecmp.cmp('.tmp', 'DA85/sys/img/AP_Launcher_001/AP_Launcher_001_02.png')
os.remove(".tmp")
if(res):
  print "24-hour format had click"
  #click Automatic date & time
  device.touch(613,851,'downAndUp')
  MonkeyRunner.sleep(2) 

#enter Set time
cci_device.clickText('Set time')
MonkeyRunner.sleep(2)

#enter min time setting
device.touch(359,635,'downAndUp')
device.touch(359,635,'downAndUp')
MonkeyRunner.sleep(2)

#click '59'
device.type('5')
device.type('9')
MonkeyRunner.sleep(2)

#enter hour time setting
device.touch(199,413,'downAndUp')
device.touch(199,413,'downAndUp')
MonkeyRunner.sleep(2)

#click '11'
device.type('1')
device.type('1')
MonkeyRunner.sleep(2)

#enter Next key
device.touch(653,1124,'downAndUp')
MonkeyRunner.sleep(2)

#Check "PM" or "AM" case
image = device.takeSnapshot()
rect = (455, 586, 127, 95) # x, y ,w ,h
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res = filecmp.cmp('.tmp', 'DA85/sys/img/AP_Launcher_001/AP_Launcher_001_03.png')
os.remove(".tmp")
if(res):
   print "It is AM format"
else:
   print "It is PM format"

#click back key
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)

#click "Done"
cci_device.clickText('Done')
MonkeyRunner.sleep(2)

#wait 1 mins
MonkeyRunner.sleep(62)

#check "AM" or "PM" case
if(res):
  if(cci_device.isTextVisible('12:00 PM')):
    print "check PM format Pass"
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
  else:
    print "check PM format Fail"
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');
else:
  if(cci_device.isTextVisible('12:00 AM')):
    print "check AM format Pass"
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
  else:
    print "check AM format Fail"
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');

#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)
