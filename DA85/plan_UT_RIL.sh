# Part 1: Define mail list
add_maillist harrison_lee
add_maillist light_cheng
add_maillist murray_lu
add_maillist jeff_chang
add_maillist jeff_huang
add_maillist joe_liao
add_maillist andy_hung
add_maillist shinjyo_chen
add_maillist carrie_wang

# Part 2: Define test case
# ====== Group - RIL_AGPS ======
#insert_testcase "RIL_AGPS_001" "RIL" "RIL_AGPS_001.py" "" 1

# ====== Group - AP_Browser ======
insert_testcase "AP_Browser_001" "RIL" "AP_Browser_001.py" "" 1
insert_testcase "AP_Browser_002" "RIL" "AP_Browser_002.py" "" 1
insert_testcase "AP_Browser_003" "RIL" "AP_Browser_003.py" "" 1
insert_testcase "AP_Browser_004" "RIL" "AP_Browser_004.py" "" 1
insert_testcase "AP_Browser_005" "RIL" "AP_Browser_005.py" "" 1
insert_testcase "AP_Browser_006" "RIL" "AP_Browser_006.py" "" 1
insert_testcase "AP_Browser_007" "RIL" "AP_Browser_007.py" "" 1
insert_testcase "AP_Browser_008" "RIL" "AP_Browser_008.py" "" 1
insert_testcase "AP_Browser_009" "RIL" "AP_Browser_009.py" "" 1
insert_testcase "AP_Browser_010" "RIL" "AP_Browser_010.py" "" 1
insert_testcase "AP_Browser_013" "RIL" "AP_Browser_013.py" "" 1
insert_testcase "AP_Browser_012" "RIL" "AP_Browser_012.py" "" 1
insert_testcase "AP_Browser_014" "RIL" "AP_Browser_014.py" "" 1
insert_testcase "AP_Browser_011" "RIL" "AP_Browser_011.py" "" 1

# ====== Group - RIL_SMSCB ======
insert_testcase "RIL_SMSCB_001" "RIL" "RIL_SMSCB_001.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_002" "RIL" "RIL_SMSCB_002.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_003" "RIL" "RIL_SMSCB_003.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_004" "RIL" "RIL_SMSCB_004.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_005" "RIL" "RIL_SMSCB_005.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_006" "RIL" "RIL_SMSCB_006.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_007" "RIL" "RIL_SMSCB_007.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_008" "RIL" "RIL_SMSCB_008.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_009" "RIL" "RIL_SMSCB_009.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_010" "RIL" "RIL_SMSCB_010.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_011" "RIL" "RIL_SMSCB_011.py" "$TEL_NUMBER" 1
insert_testcase "RIL_SMSCB_012" "RIL" "RIL_SMSCB_012.py" "$TEL_NUMBER" 1

# ====== Group - RIL_MMS ======
insert_testcase "RIL_MMS_001" "RIL" "RIL_MMS_001.py" "$TEL_NUMBER" 1
insert_testcase "RIL_MMS_002" "RIL" "RIL_MMS_002.py" "$TEL_NUMBER" 1
insert_testcase "RIL_MMS_003" "RIL" "RIL_MMS_003.py" "$TEL_NUMBER" 1
insert_testcase "RIL_MMS_004" "RIL" "RIL_MMS_004.py" "$TEL_NUMBER" 1
insert_testcase "RIL_MMS_006" "RIL" "RIL_MMS_006.py" "$TEL_NUMBER" 1
insert_testcase "RIL_MMS_007" "RIL" "RIL_MMS_007.py" "$TEL_NUMBER" 1
insert_testcase "RIL_MMS_008" "RIL" "RIL_MMS_008.py" "$TEL_NUMBER" 1
insert_testcase "RIL_MMS_005" "RIL" "RIL_MMS_005.py" "$TEL_NUMBER" 1



# ====== Group - RIL_AP_Email ======
insert_testcase "AP_Email_001" "RIL" "AP_Email_001.py" "" 1
insert_testcase "AP_Email_002" "RIL" "AP_Email_002.py" "" 1
insert_testcase "AP_Email_003" "RIL" "AP_Email_003.py" "" 1
insert_testcase "AP_Email_004" "RIL" "AP_Email_004.py" "" 1
insert_testcase "AP_Email_005" "RIL" "AP_Email_005.py" "" 1
insert_testcase "AP_Email_006" "RIL" "AP_Email_006.py" "" 1
insert_testcase "AP_Email_007" "RIL" "AP_Email_007.py" "" 1
insert_testcase "AP_Email_008" "RIL" "AP_Email_008.py" "" 1
#insert_testcase "AP_Email_009" "RIL" "AP_Email_009.py" "" 1

# ====== Group - RIL_DataConn ======
insert_testcase "RIL_DataConn_001" "RIL" "RIL_DataConn_001.py" "" 1
#insert_testcase "RIL_DataConn_002" "RIL" "RIL_DataConn_002.py" "" 1
insert_testcase "RIL_DataConn_003" "RIL" "RIL_DataConn_003.py" "" 1
insert_testcase "RIL_DataConn_004" "RIL" "RIL_DataConn_004.py" "" 1
insert_testcase "RIL_DataConn_005" "RIL" "RIL_DataConn_005.py" "" 1
# RIL_DataConn_006 cant run on Vibo SIM
#insert_testcase "RIL_DataConn_006" "RIL" "RIL_DataConn_006.py" "" 1
insert_testcase "RIL_DataConn_007" "RIL" "RIL_DataConn_007.py" "" 1

# ====== Group - RIL_PhoneBook ======
insert_testcase "RIL_PhoneBook_001" "RIL" "RIL_PhoneBook_001.py" "$TEL_NUMBER" 1
insert_testcase "RIL_PhoneBook_002" "RIL" "RIL_PhoneBook_002.py" "$TEL_NUMBER" 1
insert_testcase "RIL_PhoneBook_003" "RIL" "RIL_PhoneBook_003.py" "$TEL_NUMBER" 1
insert_testcase "RIL_PhoneBook_004" "RIL" "RIL_PhoneBook_004.py" "$TEL_NUMBER" 1
insert_testcase "RIL_PhoneBook_005" "RIL" "RIL_PhoneBook_005.py" "$TEL_NUMBER" 1
insert_testcase "RIL_PhoneBook_006" "RIL" "RIL_PhoneBook_006.py" "$TEL_NUMBER" 1
insert_testcase "RIL_PhoneBook_007" "RIL" "RIL_PhoneBook_007.py" "$TEL_NUMBER" 1
insert_testcase "RIL_PhoneBook_008" "RIL" "RIL_PhoneBook_008.py" "$TEL_NUMBER" 1
insert_testcase "RIL_PhoneBook_009" "RIL" "RIL_PhoneBook_009.py" "$TEL_NUMBER" 1






# ====== Group - RIL_SS ======
#insert_testcase "RIL_SS_001" "RIL" "RIL_SS_001.py" "" 1
# RIL_SS_002 cant run on FET SIM
#insert_testcase "RIL_SS_002" "RIL" "RIL_SS_002.py" "" 1
#insert_testcase "RIL_SS_003" "RIL" "RIL_SS_003.py" "" 1
#insert_testcase "RIL_SS_004" "RIL" "RIL_SS_004.py" "" 1
insert_testcase "RIL_SS_005" "RIL" "RIL_SS_005.py" "" 1

# ====== Group - RIL_STK ======
#insert_testcase "RIL_STK_001" "RIL" "RIL_STK_001.py" "" 1
#insert_testcase "RIL_STK_002" "RIL" "RIL_STK_002.py" "" 1
#insert_testcase "RIL_STK_003" "RIL" "RIL_STK_003.py" "" 1
#insert_testcase "RIL_STK_004" "RIL" "RIL_STK_004.py" "" 1
#insert_testcase "RIL_STK_005" "RIL" "RIL_STK_005.py" "" 1
#insert_testcase "RIL_STK_006" "RIL" "RIL_STK_006.py" "" 1
#insert_testcase "RIL_STK_007" "RIL" "RIL_STK_007.py" "" 1
#insert_testcase "RIL_STK_008" "RIL" "RIL_STK_008.py" "" 1
#insert_testcase "RIL_STK_009" "RIL" "RIL_STK_009.py" "" 1
#insert_testcase "RIL_STK_010" "RIL" "RIL_STK_010.py" "" 1

# ====== Group - RIL_USSD ======
#insert_testcase "RIL_USSD_001" "RIL" "RIL_USSD_001.py" "" 1
#insert_testcase "RIL_USSD_002" "RIL" "RIL_USSD_002.py" "" 1
#insert_testcase "RIL_USSD_003" "RIL" "RIL_USSD_003.py" "" 1

# ====== Group - RIL_VoiceCall ======
#insert_testcase "RIL_VoiceCall_001" "RIL" "RIL_VoiceCall_001.py" "" 1
#insert_testcase "RIL_VoiceCall_002" "RIL" "RIL_VoiceCall_002.py" "" 1
#insert_testcase "RIL_VoiceCall_003" "RIL" "RIL_VoiceCall_003.py" "" 1
#insert_testcase "RIL_VoiceCall_004" "RIL" "RIL_VoiceCall_004.py" "" 1
#insert_testcase "RIL_VoiceCall_005" "RIL" "RIL_VoiceCall_005.py" "" 1
#insert_testcase "RIL_VoiceCall_006" "RIL" "RIL_VoiceCall_006.py" "" 1
#insert_testcase "RIL_VoiceCall_007" "RIL" "RIL_VoiceCall_007.py" "" 1
#insert_testcase "RIL_VoiceCall_008" "RIL" "RIL_VoiceCall_008.py" "" 1
#insert_testcase "RIL_VoiceCall_009" "RIL" "RIL_VoiceCall_009.py" "" 1
#insert_testcase "RIL_VoiceCall_010" "RIL" "RIL_VoiceCall_010.py" "" 1
insert_testcase "RIL_VoiceCall_011" "RIL" "RIL_VoiceCall_011.py" "" 1
#insert_testcase "RIL_VoiceCall_012" "RIL" "RIL_VoiceCall_012.py" "" 1
insert_testcase "RIL_VoiceCall_013" "RIL" "RIL_VoiceCall_013.py" "" 1
insert_testcase "RIL_VoiceCall_014" "RIL" "RIL_VoiceCall_014.py" "" 1
#insert_testcase "RIL_VoiceCall_015" "RIL" "RIL_VoiceCall_015.py" "" 1

# ====== Group - RIL_SIM ======
insert_testcase "RIL_SIM_000" "RIL" "RIL_SIM_000.py" "" 1
insert_testcase "RIL_SIM_001" "RIL" "RIL_SIM_001.py" "" 1
insert_testcase "RIL_SIM_002" "RIL" "RIL_SIM_002.py" "" 1
insert_testcase "RIL_SIM_003" "RIL" "RIL_SIM_003.py" "" 1
insert_testcase "RIL_SIM_004" "RIL" "RIL_SIM_004.py" "" 1
insert_testcase "RIL_SIM_005" "RIL" "RIL_SIM_005.py" "" 1
insert_testcase "RIL_SIM_006" "RIL" "RIL_SIM_006.py" "" 1
insert_testcase "RIL_SIM_007" "RIL" "RIL_SIM_007.py" "" 1
insert_testcase "RIL_SIM_008" "RIL" "RIL_SIM_008.py" "" 1
insert_testcase "RIL_SIM_009" "RIL" "RIL_SIM_009.py" "" 1
insert_testcase "RIL_SIM_010" "RIL" "RIL_SIM_010.py" "" 1
insert_testcase "RIL_SIM_011" "RIL" "RIL_SIM_011.py" "" 1
insert_testcase "RIL_SIM_012" "RIL" "RIL_SIM_012.py" "" 1
insert_testcase "RIL_SIM_013" "RIL" "RIL_SIM_013.py" "" 1
insert_testcase "RIL_SIM_000" "RIL" "RIL_SIM_000.py" "" 1
insert_testcase "RIL_SIM_end" "RIL" "RIL_SIM_end.py" "" 1

# ====== Group - RIL_Network ======
insert_testcase "RIL_Network_000" "RIL" "RIL_Network_000.py" "" 1
insert_testcase "RIL_Network_001" "RIL" "RIL_Network_001.py" "" 1
insert_testcase "RIL_Network_007" "RIL" "RIL_Network_007.py" "" 1
insert_testcase "RIL_Network_006" "RIL" "RIL_Network_006.py" "" 1
insert_testcase "RIL_Network_008" "RIL" "RIL_Network_008.py" "" 1

insert_testcase "RIL_Network_000" "RIL" "RIL_Network_000.py" "" 1
insert_testcase "RIL_Network_002" "RIL" "RIL_Network_002.py" "" 1
insert_testcase "RIL_Network_009" "RIL" "RIL_Network_009.py" "" 1

insert_testcase "RIL_Network_000" "RIL" "RIL_Network_000.py" "" 1
insert_testcase "RIL_Network_003" "RIL" "RIL_Network_003.py" "" 1

insert_testcase "RIL_Network_000" "RIL" "RIL_Network_000.py" "" 1
insert_testcase "RIL_Network_end" "RIL" "RIL_Network_end.py" "" 1

#(cannot auto test) insert_testcase "RIL_Network_004" "RIL" "RIL_Network_004.py" "" 1
#(cannot auto test) insert_testcase "RIL_Network_005" "RIL" "RIL_Network_005.py" "" 1
#(cannot auto test) insert_testcase "RIL_Network_010" "RIL" "RIL_Network_010.py" "" 1
