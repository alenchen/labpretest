from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep

def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

Screen_unlock()

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click Settings'
device.startActivity(component='com.android.settings/.Settings')
sleep(3)

print 'Click Display'
cci_device.clickText('Display')
sleep(1)

print 'Click Sleep'
cci_device.clickText('Sleep')
sleep(1)

print 'press 5 minutes'
device.touch(348, 747, MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'click Clock'
device.startActivity(component='com.android.deskclock/.SetAlarm')
sleep(3)

print 'click Turn alarm on'
device.touch(398, 203, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click Time'
device.touch(368, 340, MonkeyDevice.DOWN_AND_UP)
sleep(2)

for x in range(1, 3):
    device.touch(380, 757, MonkeyDevice.DOWN_AND_UP);
    sleep(1)

print 'click Done'
device.touch(393, 915, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'click Done'
device.touch(138, 110, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'click Music'
device.startActivity(component='com.android.music/com.android.music.MusicBrowserActivity')
sleep(2)

print 'press Songs'
device.touch(468, 106, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press a music file to play'
device.touch(378, 260, MonkeyDevice.DOWN_AND_UP);
sleep(100)

print 'press Dismiss'
cci_device.clickText('Dismiss')
sleep(5)

print 'press pause'
device.press('KEYCODE_MEDIA_PAUSE', MonkeyDevice.DOWN_AND_UP)
sleep(2)
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

print 'press back key'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press Artists'
device.touch(100, 105, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press back key'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click Clock'
device.startActivity(component='com.android.deskclock/.DeskClock')
sleep(3)

print 'click Set alarm'
device.touch(581, 718, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press del alarm'
device.touch(380, 227, MonkeyDevice.DOWN);
sleep(2)

print 'press Delete alarm'
device.touch(360, 781, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'press OK'
device.touch(564, 740, MonkeyDevice.DOWN_AND_UP);
sleep(2)

for x in range(1, 5):
    print 'press back key'
    device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
    sleep(2)

print "=========================================="
Common.updateResult('Manual');
print 'Manual'

os.system("adb shell am force-stop com.android.music")  	
