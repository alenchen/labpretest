from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys


#Test 13: Can change PIN code via **04*OLD*NEW*NEW#

PIN = '0000' 
#sys.argv[5]

oldPIN = PIN
newPIN = '1234'

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 


#home key
print "Click home key"
device.press('KEYCODE_HOME','downAndUp')
sleep(1)

#call icon
print 'press call icon'
device.touch(105, 1099, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#call tab
print 'press call tab'
device.touch(115, 84, MonkeyDevice.DOWN_AND_UP);
sleep(1)



#change PIN code via **04*OLD*NEW*NEW#
print 'change PIN code via **04*OLD*NEW*NEW#'
device.type('**04*')
device.type(oldPIN)
device.type('*')
device.type(newPIN)
device.type('*')
device.type(newPIN)
device.type('#')
sleep(1)


#press call
print "press call"
device.touch(376, 1140, MonkeyDevice.DOWN_AND_UP);



#Sim 013 Result
print "Snap shot xxx_013_DialChangePIN.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'



#reset to old pin, change PIN code via **04*OLD*NEW*NEW#
print 'change PIN code via **04*OLD*NEW*NEW#'
device.type('**04*')
device.type(newPIN)
device.type('*')
device.type(oldPIN)
device.type('*')
device.type(oldPIN)
device.type('#')
sleep(1)


#press call
print "press call"
device.touch(376, 1140, MonkeyDevice.DOWN_AND_UP);
sleep(1)







