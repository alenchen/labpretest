from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands, filecmp
from PScript import *

print 'Wait connect to device'
device = MonkeyRunner.waitForConnection()

print 'Wake up device'
device.wake()
sleep(1)

print 'Create easymonkeydevice'
easy_device = EasyMonkeyDevice(device)
sleep(1)

print 'Create CCI easymonkeydevice'
cci_device = CCIEasyMonkeyDevice(device)
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

####################
# local function
####################

def waitEnterDialtacts(maxWaitTime=30):
  i = 0
  while not easy_device.visible(By.id('id/dialButton')):
    i += 1
    if i > maxWaitTime:
      sys.stderr.write('warning: time is over when waitting id/dialButton\n')
      return False

    print 'id/dialButton not display, wait 1 second. (%i/%i)' % (i, maxWaitTime)
    sleep(1)
  return True

####################
# test case start
####################

print 'press Home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'launch Dialtacts using startActivity'
device.startActivity(component='com.android.contacts/.activities.DialtactsActivity')
waitEnterDialtacts()

print "touch Dial icon"
device.touch(120, 100, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print "long touch deleteButton"
easy_device.touch(By.id('id/deleteButton'),'down')
sleep(1)
easy_device.touch(By.id('id/deleteButton'),'up')
sleep(1)

print 'input Emergency number'
input_no = '112'
device.type(input_no)

print 'touch Dial button'
easy_device.touch(By.id('id/dialButton'),'downAndUp')
sleep(2)

if Telephony.isCallState(device):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass')
  print 'Pass'

  print 'touch End button'
  easy_device.touch(By.id('id/endButton'),'downAndUp')
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'
