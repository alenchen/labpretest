from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands, filecmp
from PScript import *

print 'Wait connect to device'
device = MonkeyRunner.waitForConnection()

print 'Wake up device'
device.wake()
sleep(1)

print 'Create easymonkeydevice'
easy_device = EasyMonkeyDevice(device)
sleep(1)

print 'Create CCI easymonkeydevice'
cci_device = CCIEasyMonkeyDevice(device)
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

####################
# test case start
####################

print 'press Home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'launch Dialtacts using startActivity'
device.startActivity(component='com.android.contacts/.activities.DialtactsActivity')
sleep(5)

print 'touch Menu'
easy_device.touch(By.id('id/overflow_menu'),'downAndUp')
sleep(2)

print 'click Settings'
cci_device.clickText('Settings')
sleep(5)

print 'scroll down'
Common.scrollDown(device)
sleep(1)

print 'scroll down'
Common.scrollDown(device)
sleep(1)

print 'click GSM call settings'
cci_device.clickText('GSM call settings')
sleep(5)

print 'click Call forwarding'
cci_device.clickText('Call forwarding')
sleep(5)

Email.waitText(cci_device, 'Call forwarding settings')

print 'check forwarding settings'
res1 = cci_device.isTextVisible('Always forward')
res2 = cci_device.isTextVisible('Forward when busy')
res3 = cci_device.isTextVisible('Forward when unanswered')
res4 = cci_device.isTextVisible('Forward when unreachable')

if res1 and res2 and res3 and res4:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass')
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'