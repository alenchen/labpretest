from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys
import re
import os

num=sys.argv[4]
#"0916611562"

print 'RIL_SMSCB_005 test'
#print 'wait connect to device'
#device = MonkeyRunner.waitForConnection()
#print 'connected to device'

#11132012 
#print 'reboot...'
#os.system("tools/adb reboot")
#sleep(40)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device = EasyMonkeyDevice(device)
print 'Create EasyMonkeyDevice'

cci_device = CCIEasyMonkeyDevice(device)
print 'create CCIEasyMonkey device'

print 'ensureUnlock'
Telephony.ensureUnlock(device) 


print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#enter settings

#device.touch(495, 851, MonkeyDevice.DOWN_AND_UP)
#sleep(1)

device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(5)
#device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
cci_device.clickText("Settings")
sleep(5)

#enable delivery report
print 'enable delivery report'
device.touch(360, 750, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#back to message AP screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

#send a msg
#add sms button
device.touch(60,1130, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#start to input Tele num,  
device.touch(150,200, MonkeyDevice.DOWN_AND_UP);
sleep(1)


device.type(num)
sleep(1)

#focus on body 
device.touch(150,690, MonkeyDevice.DOWN_AND_UP);
sleep(1)

device.type('Delivery')
device.press('KEYCODE_SPACE', MonkeyDevice.DOWN_AND_UP)
device.type('Report')
sleep(1)

device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'send message, wait 15 second'
#send button
device.touch(660,1130, MonkeyDevice.DOWN_AND_UP);
sleep(1)

PASS=False
for i in range(10):
  sleep(2)

  print "==============\nmo sms:\n=============="
  print 'check database '
  string = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT address from sms where type = 1\'')

#compare number
#num = sys.argv[4]
  telen=num[3:10]
  string=string[6:13]

  p = re.compile(telen)
  m = p.search(string)
  if m :
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
    print 'Pass'
    PASS=True
    break
    
if not PASS :
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');
    print 'Failed' 

# twice back key, return to messaging screen, for more than twice test
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click delete all button
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(1)

device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click yes to delete all msgs
device.touch(510, 735, MonkeyDevice.DOWN_AND_UP)
sleep(1)
# back to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)


