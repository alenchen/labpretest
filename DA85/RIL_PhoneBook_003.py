from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands
from PScript import *

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device=EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device=CCIEasyMonkeyDevice("ss",device)
print "Create CCIEasyMonkeyDevice"

print 'wake up device'
device.wake()
sleep(2)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press Dialer Icon on Launch'
cci_device.clickText('Phone')
sleep(1)
print 'press Contacts Tab in Dialer'
device.touch(630, 95, MonkeyDevice.DOWN_AND_UP);
sleep(1)
device.touch(620, 97, MonkeyDevice.DOWN_AND_UP);
sleep(1)
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)
if cci_device.isTextVisible('Shinjyo'):
     print 'Dialer Contacts pass'
     Common.updateResult('Pass');
else:
     print 'Dialer Contacts failure'
     Common.updateResult('Fail');
sleep(1)



