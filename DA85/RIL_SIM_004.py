from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys

wrongPIN = '5678'


#Test4: Unit should be locked after entering wrong PIN code


device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"


print 'ensureUnlock'
Telephony.ensureUnlock(device) 


#input Wrong SIM PIN
print "input wrong SIM PIN"
device.type(wrongPIN)
sleep(3)


#click ok
print "Click OK"
cci_device.clickText('OK')
sleep(1)


#Sim 004 Result
print "Snap shot xxx_004_IncorrectPIN.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'













