
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device=EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device=CCIEasyMonkeyDevice("ss",device)
print "Create CCIEasyMonkeyDevice"

print 'wake up device'
device.wake()
sleep(2)

print 'ensureUnlock'
Telephony.ensureUnlock(device)
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press Contacts Icon'
cci_device.clickText('People')
sleep(2)
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])

cci_device.clickText('Andy')
sleep(1)

print 'press menu'
device.touch(675, 120, MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'press Contacts icon'
cci_device.clickText('Delete')
sleep(1)

print 'press OK'
cci_device.clickText('OK')
sleep(1)
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)
if not cci_device.isTextVisible('Andy'):
      print 'Delete Contacts pass'
      Common.updateResult('Pass')
else:
      print 'Delete Contacts failure'
      Common.updateResult('Fail')
sleep(1)

