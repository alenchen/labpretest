from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys


#Test8 : select a foreign network


device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"


print 'ensureUnlock'
Telephony.ensureUnlock(device) 
sleep(1)

print 'touch 4th item'
device.touch(457, 727, MonkeyDevice.DOWN_AND_UP);

print "Snap shot"
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])

sleep(60)

print 'ensureUnlock'
Telephony.ensureUnlock(device) 
sleep(1)


#Network 008 Result
print "Snap shot"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'


print 'click No'
device.touch(230, 778, MonkeyDevice.DOWN_AND_UP);
sleep(1)

print "Click Choose automatically (back to home network)"
cci_device.clickText('Choose automatically')
sleep(60)








