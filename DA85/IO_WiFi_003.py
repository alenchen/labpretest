from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

print 'wake up device'
device.wake()
sleep(2)

print 'press power key to lock'
device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'wake up device'
device.wake()
sleep(2)
print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
device.drag(lock_start,lock_end)
sleep(2)


if LockScreen.isLock(device):
  print 'unlock screen'
  LockScreen.unLock(device)
  LockScreen.waitUnLock(device)  


print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch system settings'
device.touch(352, 1155, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'scroll the screen'
lock_start = (355, 180)
lock_end = (355, 1150)
device.drag(lock_start,lock_end)
sleep(2)


print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'click Wi-Fi to enter wifi settings menu'
cci_device.clickText('Wi-Fi')
sleep(5)

print 'switch Wi-Fi button'
print 'turn off Wi-Fi as default'
if Wifi.isWifiOn(device) :
	print "Wi-Fi is ON, turn off it"
	device.touch(550, 95, MonkeyDevice.DOWN_AND_UP);
	sleep(5)
else :
	print 'Wi-Fi is already OFF'

#for x in range(1,6):
print 'turn on Wi-Fi'
device.touch(550, 95, MonkeyDevice.DOWN_AND_UP);
#	print(x)
sleep(5)


print "check Wi-Fi status:"
if Wifi.isWifiOn(device) :
    print 'Wi-Fi is ON'
else :
    print 'Wi-Fi is OFF'

sleep(5)

print "Keep Wi-Fi on during sleep as Never"
device.touch(660, 1150, MonkeyDevice.DOWN_AND_UP);
sleep(3)
cci_device.clickText('Advanced')
sleep(3)
cci_device.clickText('Keep Wi-Fi on during sleep')
sleep(3)
cci_device.clickText('Never')
sleep(3)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(5)


print 'clean connected AP list'
if cci_device.isTextVisible('Connected'):
	print 'found connected AP'
#	device.touch(130, 210, MonkeyDevice.DOWN_AND_UP);
	cci_device.clickText('Connected')
	sleep(5)
	device.touch(500, 950, MonkeyDevice.DOWN_AND_UP);
else:
        print "there is no connected AP"


print 'wait wifi scan result (15sec)'
sleep(15)


print 'select AP"0016018E1590" to associate with'
if cci_device.isTextVisible('0016018E1590'):
	print "0016018E1590 AP found"
        cci_device.clickText('0016018E1590')
else:
        print "Cannot find 0016018E1590 AP"

sleep(8)


if cci_device.isTextVisible('Connected'):
	print "AP is connected, press power key to suspend"
	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(15)
else:
	print "didn't connected"


print 'wake up device'
device.wake()
sleep(2)
print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
device.drag(lock_start,lock_end)
sleep(2)


print '================ \ncheck connection result\n==============='
if cci_device.isTextVisible('Connected'):
#	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Pass');
	print "Pass"
else:
#	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Fail');
	print "Fail"



