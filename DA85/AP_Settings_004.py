from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
import filecmp
import sys,os,commands
from PScript import *

device = MonkeyRunner.waitForConnection()

easy_device = EasyMonkeyDevice(device)

cci_device = CCIEasyMonkeyDevice(device)

cci_device.setIsFullTextMatch(True)

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  MonkeyRunner.sleep(2)
  device.wake()
  MonkeyRunner.sleep(2)
  device.drag(lock_start,lock_end)

print 'wake up device'
device.wake()
MonkeyRunner.sleep(2)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
MonkeyRunner.sleep(2)

#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

#click Menu key
device.press('KEYCODE_MENU','downAndUp')
MonkeyRunner.sleep(2)

#come to setting
cci_device.clickText('System settings')
MonkeyRunner.sleep(2)

#drag to Developer options location
device.drag((359,1019),(359,497),0.1,10)
MonkeyRunner.sleep(1)

#come to setting
cci_device.clickText('Developer options')
MonkeyRunner.sleep(2)

#drag to Apps location
device.drag((359,497),(359,1019),0.1,10)
MonkeyRunner.sleep(1)

if((cci_device.isTextVisible('Stay awake')) and (cci_device.isTextVisible('USB debugging')) and (cci_device.isTextVisible('Allow mock locations'))):
    print "Pass"
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
else:
    print "Fail"
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');

#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)
