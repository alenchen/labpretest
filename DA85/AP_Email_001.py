from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands, filecmp
from PScript import *

print 'Wait connect to device'
device = MonkeyRunner.waitForConnection()

print 'Wake up device'
device.wake()
sleep(1)

print 'Create easymonkeydevice'
easy_device = EasyMonkeyDevice(device)
sleep(1)

print 'Create CCI easymonkeydevice'
cci_device = CCIEasyMonkeyDevice(device)
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

Telephony.turnOffAirplaneMode(device, cci_device)
sleep(5)

Email.enterEmailAP(device, easy_device, cci_device)
res = Email.waitText(cci_device, 'Inbox')
if res == False:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'
  sys.exit(1)

####################
# test case start
####################

print 'take Snapshot and check'
image = device.takeSnapshot()
rect = (30, 1110, 370, 55) # x, y ,w ,h
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res1 = filecmp.cmp('.tmp', 'DA85/PScript/img/email_icon_01.png')
os.remove(".tmp")

rect = (650, 1110, 20, 50) # x, y ,w ,h
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res2 = filecmp.cmp('.tmp', 'DA85/PScript/img/email_icon_03.png')
os.remove(".tmp")

if res1 and res2:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass')
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'