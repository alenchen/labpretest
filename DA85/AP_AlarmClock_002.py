import sys,os
from PScript import *


a = AlarmClock.getInstance()

a.init_log(os.path.basename(__file__).split(".")[0])

a.waitForConnection()

a.wake()

a.unLockIfScreenLock()

a.goHome()

a.launchAppList()

a.info('search and click Clock')
res=False
if a.search_and_click('Clock'):
  a.info('launch Clock')
  a.info("touch Set alarm")
  ori_count = a.count()
  a.lauchAlarmsPage()
  a.launchAddPage()
  a.done()
  a.info("add a alarm")
  if a.count() == ori_count + 1 :
    a.touchTopitem()
    a.delete()
    a.info("del a alarm")
    a.touchOK()
    if a.count() == ori_count :
      res=True

a.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
if res:
  a.info("TestResult: Pass")
  a.updateResult('Pass')
else:
  a.info("TestResult: Fail")
  a.updateResult('Fail')
