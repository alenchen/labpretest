from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys


print 'wait connect to device'
device = MonkeyRunner.waitForConnection()

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

device.touch(618, 1108, MonkeyDevice.DOWN_AND_UP);
sleep(2)

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"
 
cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)
cci_device.clickText('Refresh')
sleep(2)
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)
cci_device.clickText('Stop')
sleep(2)
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(0.5)
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
print "==============\nAP_Browser_001:\n=============="

Common.updateResult('Manual');
print 'Manual'

