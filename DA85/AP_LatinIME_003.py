from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
import filecmp
import sys,os,commands
from PScript import *

device = MonkeyRunner.waitForConnection()

easy_device = EasyMonkeyDevice(device)

cci_device = CCIEasyMonkeyDevice(device)

cci_device.setIsFullTextMatch(True)

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  MonkeyRunner.sleep(2)
  device.wake()
  MonkeyRunner.sleep(2)
  device.drag(lock_start,lock_end)

print 'wake up device'
device.wake()
MonkeyRunner.sleep(2)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
MonkeyRunner.sleep(2)

#go to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

#click Messaging
device.touch(491,1097,'downAndUp')
MonkeyRunner.sleep(2)

#click New Messaging
device.touch(54,1133,'downAndUp')
MonkeyRunner.sleep(2)

#Long press menu
device.touch(144,1119,'down')
MonkeyRunner.sleep(2)

device.touch(144,1119,'up')
MonkeyRunner.sleep(2)

#click Android keyboard settings
cci_device.clickText('Android keyboard settings')
MonkeyRunner.sleep(2)

#check Android keyboard settings items
if((cci_device.isTextVisible('Input languages')) and (cci_device.isTextVisible('Auto-capitalization')) and (cci_device.isTextVisible('Vibrate on keypress'))):
    print "Pass"
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
else:
    print "Fail"
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');
#go to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)
