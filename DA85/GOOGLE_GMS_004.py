import sys,os
from PScript import *

g = GoogleGms.getInstance()

g.init_log(os.path.basename(__file__).split(".")[0])

g.waitForConnection()

g.wake()

g.unLockIfScreenLock()

g.goHome()

g.launchAppList()

g.info('search and click google map')
res=False
if g.search_and_click('Maps'):
  g.info("launch Maps")
  g.sleep(5)
  res=True

g.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
if res:
  g.info("TestResult: Manual")
  g.updateResult('Manual')
else:
  g.info("TestResult: Fail")
  g.updateResult('Fail')

