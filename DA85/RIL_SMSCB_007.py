from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys
import re

num=sys.argv[4]

print 'RIL_SMSCB_011 test'
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#send a msg to DUT itself
#click new sms button
device.touch(60,1130, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#focus on phone number editor  
device.touch(150,200, MonkeyDevice.DOWN_AND_UP);
sleep(1)
    
#input phone number
#device.type(sys.argv[4])
device.type(num)
sleep(1)

#focus on text body editor 
device.touch(150,690, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#input text body
device.type('Forward')
device.press('KEYCODE_SPACE', MonkeyDevice.DOWN_AND_UP)
device.type('test')
sleep(1)

print 'send message, wait 15s'
#send buttom
device.touch(660,610, MonkeyDevice.DOWN_AND_UP);
sleep(15)

#back to hide keyboard
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

#long press received msg 
device.drag((370,1000), (370,1000), 10, 1) 
sleep(1)

print 'forward message'
#press Forward button
device.touch(360, 580, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#focus on phone number editor  
device.touch(150,200, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#input phone number
#device.type(sys.argv[4])
device.type(num)
sleep(1)

#forward msg out to DUT itself
#send buttom
print 'send and wait 15s'
device.touch(660,690, MonkeyDevice.DOWN_AND_UP);

PASS=False
for i in range(10):
  sleep(2)


  print "==============\nmo sms:\n=============="
#print 'check database '
  string = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT address from sms where type=2\'')
  count = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT count(*) from sms where type=1\'')
  count1 = int(count)


#compare number
#num = sys.argv[4]
  telen=num[0:4]+' '+num[4:7]+' '+num[7:10]
  p = re.compile(telen)
  m = p.search(string)

  if m :  
    if count1 == 2 :
        Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
        Common.updateResult('Pass');
	print 'Pass'
        PASS=True
        break
  else :
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Failed');
    print 'Failed'
    break

if not PASS:
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Failed');
    print 'Failed'

# twice back key, return to messaging screen, for more than twice test
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click delete all button
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(1)

device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click yes to delete all msgs
device.touch(510, 735, MonkeyDevice.DOWN_AND_UP)
sleep(1)

# back to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

