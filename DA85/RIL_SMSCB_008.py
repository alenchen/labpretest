from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys
import re
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice

num=sys.argv[4]

print 'mo_sms_012 test'
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

cci_device = CCIEasyMonkeyDevice(device)
print 'create CCIEasyMonkey device'

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#Telephony.clearSms(device)

#press settings
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(5)
#device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
cci_device.clickText("Settings")
sleep(5)


#press manage SIM, wait longer for refreshing
device.touch(360,890, MonkeyDevice.DOWN_AND_UP);
sleep(4)

#long press msg
device.drag((370,1110), (370,1110), 10, 1) 
sleep(1)

#press copy to phone memory
device.touch(360,570, MonkeyDevice.DOWN_AND_UP);
sleep(5)

# twice back key, return to messaging screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print "==============\nmo sms:\n=============="
print 'check database' 
string1 = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT address from sms where type=2\'')

#compare last 7 digits of the phone number
#num = sys.argv[4]
telen=num[3:10]
string=string1[6:13]

p = re.compile(telen)
m = p.search(string)

if m :
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
    print 'Pass'

else :
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');
    print 'Failed' 


#click delete all button
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(1)

device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click yes to delete all msgs
device.touch(510, 735, MonkeyDevice.DOWN_AND_UP)
sleep(1)
# back to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)



