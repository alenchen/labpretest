from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys
import re

num=sys.argv[4]

print 'RIL_SMSCB_008 test'
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

print 'ensureUnlock'
Telephony.ensureUnlock(device) 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

print 'send 3 msg'
#send 3 msgs to differnt recipients
for x in range(3):

    #click new sms button
    device.touch(60,1130, MonkeyDevice.DOWN_AND_UP);
    sleep(1)

    #focus on phone number editor  
    device.touch(150,200, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
    
    #input phone number
    device.type(str(x+1))
    sleep(1)

    #focus on text body editor 
    device.touch(150,690, MonkeyDevice.DOWN_AND_UP);
    sleep(1)

    #input text body
    device.type('auto-test')
    device.type(str(x+1))
    
    print 'send message ' + str(x+1)
    #send buttom
    device.touch(660,610, MonkeyDevice.DOWN_AND_UP);
    sleep(5)

    #back twice to message AP screen
    device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
    sleep(1)
    device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
    sleep(1)

sleep(1)

#click delete all button
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(1)

device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'delete all threads'
#click yes to delete all msgs
device.touch(510, 735, MonkeyDevice.DOWN_AND_UP)
sleep(1)

print "==============\nmo sms:\n=============="
#print 'check database '
count = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT count(*) from sms\'')

#compare number
#telen=num[0:4]+' '+num[4:7]+' '+num[7:10]

#p = re.compile(telen)
#m = p.search(string)

if int(count) :
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');
    print 'Failed' 
else :
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
    print 'Pass'

# back to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

