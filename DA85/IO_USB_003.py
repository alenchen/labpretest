from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
import sys,os,commands
from time import sleep
from PScript import *

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

model = device.getSystemProperty('ro.product.model')
print "The ro.product.model property on device = " + model


cci_device.setIsFullTextMatch(False)
#Battery test , Charging status check
#setting
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(1)

device.press('KEYCODE_MENU','downAndUp')
MonkeyRunner.sleep(1)

print "System settings location = " + cci_device.getTextLocation('System settings')
MonkeyRunner.sleep(1)
cci_device.clickText('System settings')

MonkeyRunner.sleep(1)

device.drag((450,860),(450,120),0.1,10)

MonkeyRunner.sleep(1)

cci_device.clickText('About phone')
MonkeyRunner.sleep(1)

cci_device.clickText('Status')
MonkeyRunner.sleep(1)

if cci_device.isTextVisible('Charging (USB)'):
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Pass');
	print "Pass"
else:
	if cci_device.isTextVisible('Full'):
		Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
		Common.updateResult('Pass');
		print "Pass"
	else:
		Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
		Common.updateResult('Fail');
		print "Failed"

MonkeyRunner.sleep(1)

