import sys,os
from PScript import *

g = GoogleGms.getInstance()

Log = Log.getInstance(TAG=os.path.basename(__file__).split(".")[0], levelfilter='iw')

g.waitForConnection()

g.wake()

g.unLockIfScreenLock()

g.goHome()

g.launchAppList()

Log.i('search and click google Talk')
res=False
if g.search_and_click('Talk'):
  Log.i("launch Talk")

  if g.clickText("Offline"):
    g.sleep(5)

  res=True

g.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
if res:
  Log.i("TestResult: Manual")
  g.updateResult('Manual')
else:
  Log.i("TestResult: Fail")
  g.updateResult('Fail')

