from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys


#Test 10 Insert a new list with wrong PIN2
worngPIN2 = '7777'


device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 


#click :
print "Click :"
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
#device.touch(520, 50, MonkeyDevice.DOWN_AND_UP);
sleep(1)


#click Add contact
print "Click :"
device.touch(529, 193, MonkeyDevice.DOWN_AND_UP);
sleep(1)


#input name
print "input name"
device.type('CCI2')
sleep(1)


#touch edit for number
easy_device.touch(By.id('id/fdn_number'), 'downAndUp')
sleep(1)

#input name
print "input number 2-8751-6228"
device.type('287516229')
sleep(1)


#click Save
cci_device.clickText('Save')
sleep(1)


#enter wrong pin2
print "input PIN2"
device.type(worngPIN2)
sleep(2)


#click Done
print "Click Done"
device.touch(667, 1157, MonkeyDevice.DOWN_AND_UP);
sleep(5)



#Sim 010 Result
print "Snap shot xxx_010_FdnAddFail.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'






