from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys


#Test 9 Enable FDN, Insert a new list with correct PIN2
PIN2 = '1234'

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'press call icon'
device.touch(105, 1099, MonkeyDevice.DOWN_AND_UP);
sleep(1)

print 'press call tab'
device.touch(115, 84, MonkeyDevice.DOWN_AND_UP);
sleep(1)

print 'click :'
device.touch(678, 1165, MonkeyDevice.DOWN_AND_UP);
sleep(3)


print 'Settings'
device.touch(490, 1046, MonkeyDevice.DOWN_AND_UP);
sleep(1)


print "Click Fixed Dialing Numbers"
cci_device.clickText('Fixed Dialing Numbers')
sleep(1)


print "Click Enable FDN"
cci_device.clickText('Enable FDN')
sleep(1)

#Input PIN2
print "Input PIN2"
device.type(PIN2)
sleep(1)


#click ok
print "Click OK"
device.touch(538, 551, MonkeyDevice.DOWN_AND_UP);
sleep(1)



#click FDN list
print "Click  FDN list"
cci_device.clickText('FDN list')
sleep(1)


#click :
print "Click :"
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
#device.touch(520, 50, MonkeyDevice.DOWN_AND_UP);
sleep(1)


#click Add contact
print "Click :"
device.touch(529, 193, MonkeyDevice.DOWN_AND_UP);
sleep(1)


#input name
print "input name"
device.type('CCI')
sleep(1)


#touch edit for number
easy_device.touch(By.id('id/fdn_number'), 'downAndUp')
sleep(1)

#input name
print "input number 2-8751-6228"
device.type('287516228')
sleep(1)


#click Save
cci_device.clickText('Save')
sleep(1)


#enter pin2
print "input PIN2"
device.type(PIN2)
sleep(2)


#click Done
print "Click Done"
device.touch(667, 1157, MonkeyDevice.DOWN_AND_UP);
sleep(5)


#Sim 009 Result
print "Snap shot xxx_009_FdnAddOk.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'










