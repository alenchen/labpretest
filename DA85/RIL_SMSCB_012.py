from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys
import re
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper

num=sys.argv[4]

print 'RIL_SMSCB_018 test'
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'
print 'create EasyMonkey device'
easy_device = EasyMonkeyDevice(device)
print 'create CCIEasyMonkey device'
cci_device = CCIEasyMonkeyDevice(device)
print 'ensureUnlock'
Telephony.ensureUnlock(device) 

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#click new sms button
device.touch(60,1130, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#focus on phone number editor  
device.touch(150,200, MonkeyDevice.DOWN_AND_UP);
sleep(1)
    
#input phone number
device.type(num)
sleep(1)

#focus on text body editor 
device.touch(150,690, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#input text body
device.type('Draft')
device.press('KEYCODE_SPACE', MonkeyDevice.DOWN_AND_UP)
device.type('test')
sleep(1)

# twice back key, return to messaging screen, for more than twice test
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

#check database to see if msg is stored as draft
count = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT count(*) from sms where type=3\'')
sleep(1)

draft = int(count)
if draft :  
   Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
   Common.updateResult('Pass')
   print 'Pass'
else :
   Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
   Common.updateResult('Fail')
   print 'Failed'


#click delete all button
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(1)

device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click yes to delete all msgs
device.touch(510, 735, MonkeyDevice.DOWN_AND_UP)
sleep(1)

# back to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

