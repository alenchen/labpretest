from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice

from PScript import *
from time import sleep
import sys
import os

os.system("tools/adb remount")
os.system("tools/adb push DA85/mms/databases /data/data/com.android.providers.telephony/databases")
os.system("tools/adb push DA85/mms/app_parts /data/data/com.android.providers.telephony/app_parts")
os.system("tools/adb reboot")
print 'wait 30 seconds for reboot'
sleep(30)
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device = EasyMonkeyDevice(device)
print 'Create EasyMonkeyDevice'

cci_device = CCIEasyMonkeyDevice(device)
print 'Create CCIEasyMonkeyDevice'

print 'unlock screen'
Telephony.ensureUnlock(device) 

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'Click Mms Application'
cci_device.clickText("Messaging")
sleep(1)

Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual' 
