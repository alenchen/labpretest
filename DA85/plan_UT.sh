#!/bin/bash
#
# Test Plan : Auto Regular CM Process Demo
#
#

# Set delay time between two test case
DELAY=5
MAILDELAY=1
PROJECT="DA85"
REPORT_NAME="${1}_${2}.html"
REPORT_LOG="${1}_${2}.log"
REPORT_SW_VERSION="${3}"
REPORT_HW_VERSION="EVT1-2"
TEST_SERIAL="${2}"
TEL_NUMBER="${4}"
PIN1="${5}"
PIN2="${6}"
PUK="${7}"
PLAN_DESCRIPTION="DA85 Unit Test"

. common/plan_basic.sh

. DA85/plan_UT_SW1.sh
. DA85/plan_UT_MM.sh
. DA85/plan_UT_SYS.sh
. DA85/plan_UT_RIL.sh
. DA85/plan_UT_IO.sh

# Start to run test plan
run_test_plan "${PROJECT}" "${2}"
