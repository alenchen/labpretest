from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys
import re
import string
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper

num=sys.argv[4]

print 'RIL_SMSCB_009 test'
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

print 'create EasyMonkey device'
easy_device = EasyMonkeyDevice(device)
print 'create CCIEasyMonkey device'
cci_device = CCIEasyMonkeyDevice(device)

print 'ensureUnlock'
Telephony.ensureUnlock(device) 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#click new sms button
device.touch(60,1130, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#focus on phone number editor  
device.touch(150,200, MonkeyDevice.DOWN_AND_UP);
sleep(1)
    
#input phone number
#device.type(sys.argv[4])
device.type('123')
sleep(1)

#focus on text body editor 
device.touch(150,690, MonkeyDevice.DOWN_AND_UP);
sleep(1)

print 'send 2 msg'
#send 3 msg to same number for test
for x in range(4):

    #input text body
    device.type('test')
    device.type(str(x+1))
    
    print 'send messaeg ' + str(x+1)
    #send buttom
    device.touch(660,610, MonkeyDevice.DOWN_AND_UP);
    sleep(5)

sleep(20)
#count msg sent
string = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT count(*) from sms\'')
print 'messages sent ' + string

#back again to hide keyboard
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

#long press on any single msg 
#2012/10/16 change pixel
device.drag((340,1000), (340,1000), 10, 1) 
sleep(1)

#delete the msg selected
device.touch(360,970, MonkeyDevice.DOWN_AND_UP) 
sleep(1)

#click yes to delete msg
device.touch(520,660, MonkeyDevice.DOWN_AND_UP) 
sleep(1)

#count msg sent again, it should be 1 less
string1 = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT count(*) from sms\'')
print 'messages sent ' + string1

print "==============\nmo sms:\n=============="

if int(string) != int(string1):
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
    print 'Pass'

else:
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');
    print 'Failed'

#back key, return to messaging screen, for more than twice test
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click delete all button
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(1)

device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click yes to delete all msgs
device.touch(510, 735, MonkeyDevice.DOWN_AND_UP)
sleep(1)
# back to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)




