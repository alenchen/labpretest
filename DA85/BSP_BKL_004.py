from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
import filecmp
from PScript import *
from time import sleep

def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 1018)
	lock_end = (620, 1018)
	device.drag(lock_start,lock_end)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

Screen_unlock()

print 'check sdcard in device'
pathExist = device.shell("ls mnt/sdcard").split('\r\n')
if pathExist is None :
	print 'No sdcard found.'
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Fail');
	sys.exit(1)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press menu key'
device.touch(365, 1110, MonkeyDevice.DOWN_AND_UP);
sleep(2)

for i in range(0,3):
	Common.scrollLeft(device)
	sleep(0.5)
sleep(2)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'click Gallery'
cci_device.clickText('Gallery')
sleep(5)

print 'remove all media files in device'
device.shell("rm -r /storage/sdcard0/Pictures/*")
device.shell("rm -r /storage/sdcard0/Music/*")
device.shell("rm -r /storage/sdcard0/DCIM/Camera/*")
device.shell("rm -r /storage/sdcard0/Movies/*")
sleep(1)
print 'Send Intent'
device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")
sleep(2)

print 'push video into device'
os.system("adb push DA85/video/1.* /storage/sdcard0/Movies/")
sleep(5)
pictureExist = device.shell("ls storage/sdcard0/Movies").split('\r\n')
if pictureExist:
	print 'Send Intent'
	device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")
	sleep(2)		
else :
	print 'push video fail'

print 'click Album "video"'
device.touch(345,700, MonkeyDevice.DOWN_AND_UP)
sleep(3)
device.touch(345,700, MonkeyDevice.DOWN_AND_UP)
sleep(3)

print 'play video'
device.touch(355,620, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'brightness value'
res = device.shell('cat /sys/class/leds/lcd-backlight/brightness')
print res

print "=======================\nBrightness is checked\n======================="
if res:
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
    print 'pass'
else :
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');
    print 'fail'
    
for x in range (0, 5):
  device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)
  sleep(0.5)