from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys
 
device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"
 
cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"
print 'ensureUnlock'
Telephony.ensureUnlock(device) 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch browser ap'
device.touch(618, 1108, MonkeyDevice.DOWN_AND_UP);
sleep(2)

#tabs
print 'touch tabs'
device.touch(573, 102, MonkeyDevice.DOWN_AND_UP);
sleep(2)
print 'takeSnapshot(1/2)'
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])


#center x
device.touch(537, 457, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'takeSnapshot(2/2)'
#should appear only one window
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2]) 

print "==============\nAP_Browser_004:\n=============="
Common.updateResult('Manual');
print 'Manual'

