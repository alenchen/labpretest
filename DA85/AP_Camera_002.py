from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands,re
from PScript import *

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
easy_device = EasyMonkeyDevice(device)
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def screenUnlock(device,lock_start, lock_end):
  print 'screen unlock'
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  sleep(2)
  device.wake()
  sleep(1)
  device.drag(lock_start,lock_end)

print 'wake up device'
device.wake()
sleep(1)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
sleep(2)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(1)


print 'launch app list'
device.touch(360, 1105, MonkeyDevice.DOWN_AND_UP)
sleep(1)


for x in range(1, 8):
  print 'scroll left'
  Common.scrollLeft(device);
  sleep(1)


print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'Launch camera'
cci_device.clickText('Camera')
sleep(3)

print 'Launch modeSelect button'
device.touch(50, 1065, MonkeyDevice.DOWN_AND_UP);
#easy_device.touch(By.id('id/mode_selection'),'downAndUp')
sleep(1)

print 'select video icon'
#device.touch(175, 825, MonkeyDevice.DOWN_AND_UP);
easy_device.touch(By.id('id/mode_video'),'downAndUp')
sleep(1)

print 'click video button'
device.touch(175, 825, MonkeyDevice.DOWN_AND_UP);
sleep(2)

device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')
old_videoList = device.shell("ls sdcard/DCIM/Camera/*.3gp").split('\r\n')

print 'Start recording'
#easy_device.touch(By.id('id/shutter_button'),'downAndUp')
device.touch(335, 1070, MonkeyDevice.DOWN_AND_UP);
sleep(4)

print 'Stop recording'
#easy_device.touch(By.id('id/shutter_button'),'downAndUp')
device.touch(335, 1070, MonkeyDevice.DOWN_AND_UP);
sleep(1)

new_videoList = device.shell("ls sdcard/DCIM/Camera/*.3gp").split('\r\n')


new_video = []
i = 0
for video in new_videoList : 
  if video not in old_videoList :
    new_video.append(video)
    i += 1
    print 'new video : sdcard/DCIM/Camera/' + video
os.system("platform-tools/adb pull mnt/sdcard/DCIM/Camera/ public/" + sys.argv[1] + "/" + sys.argv[2] + "/" + sys.argv[3] + "/")

os.system
print "==============\nrecorder a video:\n=============="
if 0 != i:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Fail'


for x in range(1, 5):
  device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
  sleep(1)

device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')

device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")