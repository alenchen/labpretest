from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands
from PScript import *

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
easy_device = EasyMonkeyDevice(device)
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def takeSnapshot():
	print 'take a picture'
	#device.touch(268, 500, MonkeyDevice.DOWN);
	#sleep(2)
	device.touch(335, 1070, MonkeyDevice.DOWN_AND_UP);
	sleep(2)

#def checkResoluction(format):
#	print 'check Resoluction '
#	#print 'click thumbnail button'
#	easy_device.touch(By.id('id/thumbnail'),'downAndUp')
#	sleep(1)
#	#print 'Click gallery button'
#	easy_device.touch(By.id('id/goto_gallery_button'),'downAndUp')
#	sleep(1)
#	#print 'click function button'
#	device.touch(497, 36, MonkeyDevice.DOWN_AND_UP);
#	sleep(1)
#
#	device.touch(375, 545, MonkeyDevice.DOWN_AND_UP);
#	sleep(1)
#
#	device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP);
#	sleep(1)
#	device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP);
#	sleep(1)
#
#	rect = (60, 240, 145, 75) # x, y ,w ,h
#
#	image = device.takeSnapshot()
#   subimage = image.getSubImage(rect)
#   subimage.writeToFile('.tmp','png')
#   res = filecmp.cmp('.tmp', 'DA80/PScript/img/' + str(format) + '.png');
#   os.remove(".tmp")
#    if res :
#      return True
#  return False

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  sleep(2)
  device.wake()
  sleep(1)
  device.drag(lock_start,lock_end)


print 'wake up device'
device.wake()
sleep(2)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
sleep(2)

device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'launch app list'
device.touch(360, 1105, MonkeyDevice.DOWN_AND_UP)
sleep(1)

for x in range(1, 8):
  print 'scroll left'
  Common.scrollLeft(device);
  sleep(1)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'Launch camera'
cci_device.clickText('Camera')
sleep(3)

print 'click id/second_level_indicator_bar_icon'
device.touch(50, 945, MonkeyDevice.DOWN_AND_UP)
#easy_device.touch(By.id('id/second_level_indicator_bar_icon'),'downAndUp')
sleep(1)

print  'click camera settings'
device.touch(230, 910, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#from smallest resoluction
for x in range(1, 16):
  device.touch(415, 258, MonkeyDevice.DOWN_AND_UP)
  sleep(1)

device.touch(230, 910, MonkeyDevice.DOWN_AND_UP)

resoluction = ['QCIF', 'QVGA', 'CIF', 'VGA', 'WVGA', '2M', 'SVGA', '1M', 'HD720', 'WXGA', 'HD1080', '3M', '5M', '8M', '12M']

result = True

for i in range(0,15):
	#device.touch(168, 683, MonkeyDevice.DOWN_AND_UP)
	print 'select' + resoluction[i]
	takeSnapshot()
	#if not checkResoluction(resoluction[i]):
	#	result = False
	sleep(1)
	device.touch(230, 910, MonkeyDevice.DOWN_AND_UP)
	sleep(1)

	device.touch(620, 260, MonkeyDevice.DOWN_AND_UP)
	sleep(1)

	device.touch(230, 910, MonkeyDevice.DOWN_AND_UP)
	sleep(1)


os.system("platform-tools/adb pull mnt/sdcard/DCIM/Camera/ public/" + sys.argv[1] + "/" + sys.argv[2] + "/" + sys.argv[3] + "/")

print "==============\nresoluction setting:\n=============="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'

device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')

print 'back to home'
for x in range(1, 5):
  device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
  sleep(1)
device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")