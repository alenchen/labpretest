from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands,re
from PScript import *

def selectVideoQuality(x,y):
	device.touch(x, y, MonkeyDevice.DOWN_AND_UP);
	sleep(2)
	print 'Start recording'
	easy_device.touch(By.id('id/shutter_button'),'downAndUp')
	sleep(4)

	print 'Stop recording'
	easy_device.touch(By.id('id/shutter_button'),'downAndUp')
	sleep(2)

def videoQualitySetting():
	print 'select second_level_indicator_bar_icon '
	#easy_device.touch(By.id('id/second_level_indicator'),'downAndUp')
	device.touch(55, 950, MonkeyDevice.DOWN_AND_UP);
	sleep(2)
	print 'select videoQuality setting '
	device.touch(240, 910, MonkeyDevice.DOWN_AND_UP);
	sleep(2)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
easy_device = EasyMonkeyDevice(device)
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

print 'wake up device'
device.wake()
sleep(1)

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  sleep(2)
  device.wake()
  sleep(1)
  device.drag(lock_start,lock_end)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
sleep(2)


print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(1)


print 'launch app list'
device.touch(360, 1105, MonkeyDevice.DOWN_AND_UP)
sleep(1)

for x in range(1, 8):
  print 'scroll left'
  Common.scrollLeft(device);
  sleep(1)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'Launch camera'
cci_device.clickText('Camera')
sleep(3)

device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')

print 'Launch modeSelect button'
device.touch(50, 1065, MonkeyDevice.DOWN_AND_UP);
#easy_device.touch(By.id('id/mode_selection'),'downAndUp')
sleep(2)

print 'select video icon'
device.touch(150, 1080, MonkeyDevice.DOWN_AND_UP);
#easy_device.touch(By.id('id/mode_video'),'downAndUp')
sleep(2)

print 'select videoQuality QCIF '
videoQualitySetting()
sleep(2)
start = (350, 500)
end = (350, 300)
device.drag(start, end)
sleep(2)
selectVideoQuality(350,800)

print 'select videoQuality QVGA '
videoQualitySetting()
sleep(2)
start = (350, 500)
end = (350, 300)
device.drag(end, start)
selectVideoQuality(350,765)

print 'select videoQuality CIF '
videoQualitySetting()
sleep(2)
selectVideoQuality(350,660)

print 'select videoQuality VGA '
videoQualitySetting()
sleep(2)
selectVideoQuality(350,560)

print 'select videoQuality WVGA '
videoQualitySetting()
sleep(2)
selectVideoQuality(350,450)

print 'select videoQuality D1 '
videoQualitySetting()
sleep(2)
selectVideoQuality(350,350)

print 'select videoQuality 720P '
videoQualitySetting()
sleep(2)
selectVideoQuality(350,250)

print 'select videoQuality 1080P '
videoQualitySetting()
sleep(2)
selectVideoQuality(350,150)

os.system("platform-tools/adb pull mnt/sdcard/DCIM/Camera/ public/" + sys.argv[1] + "/" + sys.argv[2] + "/" + sys.argv[3] + "/")

sleep(2)
device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')
sleep(2)

print "==============\nrecorder all quality videos:\n=============="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual' 

for x in range(1, 5):
  device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
  sleep(1)

  device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")

