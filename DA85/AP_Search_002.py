from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
import filecmp
import sys,os,commands
from PScript import *

device = MonkeyRunner.waitForConnection()

easy_device = EasyMonkeyDevice(device)

cci_device = CCIEasyMonkeyDevice(device)

cci_device.setIsFullTextMatch(True)

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  MonkeyRunner.sleep(2)
  device.wake()
  MonkeyRunner.sleep(2)
  device.drag(lock_start,lock_end)

print 'wake up device'
device.wake()
MonkeyRunner.sleep(2)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
MonkeyRunner.sleep(2)

#back to home
print 'back to home'
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

#click google search 
device.touch(360,116,'downAndUp')
MonkeyRunner.sleep(8)

#click Menu key
device.press('KEYCODE_MENU','downAndUp')
MonkeyRunner.sleep(2)

#click Settings
cci_device.clickText('Settings')
MonkeyRunner.sleep(2)

#click Phone search items
cci_device.clickText('Phone search')
MonkeyRunner.sleep(2)

#click Music items
print 'check had [Music] items on the google search'
image = device.takeSnapshot()
rect = (607, 580, 31, 31) # x, y ,w ,h
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res = filecmp.cmp('.tmp', 'DA85/sys/img/AP_Search_002/AP_Search_002_01.png')
os.remove(".tmp")
if(res == False):
  print "Music had not click"
  #click Automatic date & time
  device.touch(623,596,'downAndUp')
  MonkeyRunner.sleep(2)

#compare Play Music items
image = device.takeSnapshot()
rect = (607, 1096, 31, 31) # x, y ,w ,h
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp','png')
res = filecmp.cmp('.tmp', 'DA85/sys/img/AP_Search_002/AP_Search_002_02.png')
os.remove(".tmp")
if(res == False):
  print "Play Music had not click"
  #click Automatic date & time
  device.touch(623,1112,'downAndUp')
  MonkeyRunner.sleep(2)

#click Back key
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)

#click Back key
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)

#copy file
os.system("adb push DA85/sys/data/AP_Search_002/Sundial_Dreams.mp3 /storage/sdcard0/Sundial_Dreams.mp3")
device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")
MonkeyRunner.sleep(2)

#send 'Sundial_Dream'
device.type('S')
device.type('u')
device.type('n')
device.type('d')
device.type('i')
device.type('a')
device.type('l')


#compare music 
if(cci_device.isTextVisible('Sundial Dreams')):
  print "Pass"
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
else:
  print "Fail" 
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');

#remove .mp3 file
device.shell("rm /storage/sdcard0/Sundial_Dreams.mp3")

#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)
