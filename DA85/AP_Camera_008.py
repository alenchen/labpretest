from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands
from PScript import *

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
easy_device = EasyMonkeyDevice(device)
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  sleep(2)
  device.wake()
  sleep(1)
  device.drag(lock_start,lock_end)

print 'wake up device'
device.wake()
sleep(1)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
sleep(2)

device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'launch app list'
device.touch(360, 1105, MonkeyDevice.DOWN_AND_UP)
sleep(1)

for x in range(1, 8):
  print 'scroll left'
  Common.scrollLeft(device);
  sleep(1)

device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'Launch camera'
cci_device.clickText('Camera')
sleep(3)


old_pictureList = device.shell("ls sdcard/DCIM/Camera/*.jpg").split('\r\n')

print 'camera focus'
device.touch(268, 806, MonkeyDevice.DOWN);
sleep(3)

print 'camera take a picture'
device.touch(335, 1070, MonkeyDevice.DOWN_AND_UP);
#easy_device.touch(By.id('id/shutter_button'),'downAndUp')
sleep(2)
#device.touch(335, 1070, MonkeyDevice.DOWN_AND_UP);
#sleep(2)
#device.touch(335, 1070, MonkeyDevice.DOWN_AND_UP);
#sleep(2)

new_pictureList = device.shell("ls sdcard/DCIM/Camera/*.jpg")

new_pic = []
i = 0
for pic in new_pictureList : 
  if pic not in old_pictureList :
    new_pic.append(pic)
    i += 1

#if i != 1:
#    print "==============\ntake a picture:\n=============="
#    Common.updateResult('Fail');
#    print 'Fail'

print 'Click thumbnail button'
device.touch(640, 1040, MonkeyDevice.DOWN_AND_UP)
#easy_device.touch(By.id('id/thumbnail_button'),'downAndUp')
sleep(1)


print 'click delete button'
#easy_device.touch(By.id('id/action_delete'),'downAndUp')
device.touch(550, 30, MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'click check_delete button'
device.touch(510, 660, MonkeyDevice.DOWN_AND_UP);
sleep(1)

res_pictureList = device.shell("ls sdcard/DCIM/Camera/*.jpg").split('\r\n')

res_pic = []
i = 0
for pic in res_pictureList : 
  if pic not in old_pictureList :
    res_pic.append(pic)
    i += 1
    print 'new picture : sdcard/DCIM/Camera/' + pic
print "==============\ntake a picture and delete:\n=============="
if 0 == i:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'failed'

device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')

for x in range(1, 5):
  device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
  sleep(1)
device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")