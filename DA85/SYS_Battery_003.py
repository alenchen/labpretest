from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
import sys

device = MonkeyRunner.waitForConnection()
print "Connect to device "

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

model = device.getSystemProperty('ro.product.model')
print "The ro.product.model property on device = " + model

device.shell("setprop sys.batteryfullpopindicator 1")

cci_device.setIsFullTextMatch(False)

#Battery test , Full charge check
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)

device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

device.shell("echo 1 > /sys/module/pm8921_charger/parameters/cci_fake_flag")
device.shell("echo 1 > /sys/module/pm8921_charger/parameters/cci_fake_charger_status")
MonkeyRunner.sleep(2)
device.shell("echo 4 > /sys/module/pm8921_charger/parameters/cci_fake_charger_status")

MonkeyRunner.sleep(2)

if cci_device.isTextVisible('Battery Full'):
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Pass');
	print 'Pass'
else:
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Fail');
	print 'Failed'

MonkeyRunner.sleep(2)
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)
#Original setting recovery
device.shell("echo 0 > /sys/module/pm8921_charger/parameters/cci_fake_flag")
device.shell("echo -1 > /sys/module/pm8921_charger/parameters/cci_fake_charger_status")

device.shell("setprop sys.batteryfullpopindicator 0")
