from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice

from PScript import *
from time import sleep
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device =EasyMonkeyDevice(device)
print 'Create EasyMonkeyDevice'

cci_device = CCIEasyMonkeyDevice(device)
print 'Create CCIEasyMonkeyDevice'

print 'check lockscreen'
Telephony.ensureUnlock(device) 

device.startActivity(component='com.android.mms/.ui.ComposeMessageActivity')
sleep(1)

device.press("KEYCODE_MENU", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click Add subject'
cci_device.clickText('Add subject')
sleep(2)

device.type('Item7')
sleep(2)

#focus on To
#cci_device.clickText('To')
device.touch(270,220, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'enter phone number'
device.type(sys.argv[4])
sleep(2)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press send'
cci_device.clickText("MMS")

PASS=False
for i in range(5):
  sleep(20)

  print 'wake up device'
  device.wake()
  sleep(2)

  print 'check lockscreen'
  if LockScreen.isLock(device):
    print 'unlock screen'
    LockScreen.unLock(device)
    LockScreen.waitUnLock(device)

  if LockScreen.isLock(device):
    print 'unlock screen'
    Telephony.ensureUnlock(device) 

  #print 'check database '
  if Telephony.SentOK(device, "Item7") and Telephony.RecvSubOK(device, "Item7"):
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
    print 'Pass'
    PASS=True
    break

if not PASS :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Manual');
  print 'Manual'

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

