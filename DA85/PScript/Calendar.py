from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from time import sleep
import Common, os, filecmp
class getInstance(Common.getInstance):
  db = "/data/data/com.android.providers.calendar/databases/calendar.db"
  def __init__ (self, common=None):
    """ void __init__ (Common.getInstance common) 
    """
    if common:
      self.device = common.device
      self.ccidevice = common.ccidevice
      self.deviceId = common.deviceId
    else:
      Common.getInstance.__init__(self)

  def __del__ (self):
    pass

  def Events_count(self):
    """ int Events_count ()
        get the number of alarms.
    """
    self.debug("Events_count()")
    return int(self.query(self.db, "count(*)", "Events"))
 
  def menu(self, text, setIsFullTextMatch=True):
    self.debug("menu(text=%s, setIsFullTextMatch=%s)", text, setIsFullTextMatch)
    self.touch(660, 100)
    res = self.clickText(text, setIsFullTextMatch=setIsFullTextMatch)
    if res :
      sleep(1)
    return res

  def done(self):
    self.debug("done()")
    self.touch(id="id/action_done")

  def cancel(self):
    self.debug("cancel()")
    self.touch(id="id/action_cancel")
