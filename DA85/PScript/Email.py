from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands, filecmp

def waitText(cci_device, lText, maxWaitTime=30):
  i = 0
  while not cci_device.isTextVisible(lText):
    i += 1
    if i > maxWaitTime:
      sys.stderr.write('warning: time is over when waitting text\n')
      return False

    print '"%s" not display, wait 1 second. (%i/%i)' % (lText, i, maxWaitTime)
    sleep(1)
  return True
  
def enterEmailAP(device, easy_device, cci_device):
  print 'press Home key'
  device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
  sleep(2)

  print 'launch Email using startActivity'
  device.startActivity(component='com.android.email/.activity.Welcome')
  sleep(5)

  if cci_device.isTextVisible('Account setup'):
    print 'email Account not exist'

    print 'input Account'
    easy_device.touch(By.id('id/account_email'),'downAndUp')
    input_account = 'test.compalcomm@gmail.com'
    device.type(input_account)
    sleep(2)

    print 'input Password'
    easy_device.touch(By.id('id/account_password'),'downAndUp')
    input_password = 'qwertyu123'
    device.type(input_password)
    sleep(2)

    print 'click Next'
    cci_device.clickText('Next')
    sleep(5)

    res = waitText(cci_device, 'Account settings')
    if res == False:
      return False

    print 'click Next'
    cci_device.clickText('Next')
    sleep(5)

    waitText(cci_device, 'Account setup')
    if res == False:
      return False

    print 'input Display name'
    easy_device.touch(By.id('id/account_name'),'downAndUp')
    input_name = 'GMAIL'
    device.type(input_name)
    sleep(2)

    print 'click Next'
    cci_device.clickText('Next')
    sleep(5)

  return True
