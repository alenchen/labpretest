import logging
LEVEL={ 'd'   : logging.DEBUG,
        'i'    : logging.INFO,
        'w' : logging.WARNING,
        'e'   : logging.ERROR,
        'c': logging.CRITICAL,
        'n'  : logging.NOTSET,
      }

class LevelFilter(logging.Filter):  
  def __init__(self, level, *args, **kwargs):
    self.leveldict = {}
    for i in range(0, len(level)):
      self.leveldict[LEVEL[level[i]]] = True
   
  def filter(self, record): 
    return self.leveldict.get(record.levelno, False)

class getInstance:
  isLogOn    = False
  def __init__(self, TAG=__name__, level='i', levelfilter='i', fmt='%(asctime)s %(name)-12s %(levelname)-8s %(message)s', datefmt='%m-%d %H:%M:%S'):
    console = logging.StreamHandler()
    console.setLevel(LEVEL[level])
    # set a format which is simpler for console use
    formatter = logging.Formatter(fmt, datefmt=datefmt)
    # tell the handler to use this format
    console.setFormatter(formatter)
 
    if levelfilter :
      console.addFilter(LevelFilter(levelfilter))
   
    # add the handler to the root logger
    self.logger = logging.getLogger(TAG)
    self.logger.addHandler(console)
    
    self.logger.setLevel(level=LEVEL[level])
    self.isLogOn = True
    self.logger.info('info    : on')
    self.logger.warning('warning : on')
    self.logger.debug('debug   : on')
    self.logger.error('error   : on')
    self.logger.critical('critical: on')

  def i(self, fmt, *arg):
    if self.isLogOn:
      self.logger.info(fmt % arg);

  def w(self, fmt, *arg):
    if self.isLogOn:
      self.logger.warning(fmt % arg);

  def d(self, fmt, *arg):
    if self.isLogOn:
      self.logger.debug(fmt % arg);

  def e(self, fmt, *arg):
    if self.isLogOn:
      self.logger.error(fmt % arg);

  def c(self, fmt, *arg):
    if self.isLogOn:
      self.logger.critical(fmt % arg);

  def __del__ (self):
    pass
