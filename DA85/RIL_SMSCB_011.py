from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys
import re
import filecmp
import os
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper

num=sys.argv[4]
#precondition: Notification is clicked.
#send sms
#check status bar

print 'RIL_SMSCB_017 test'
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'
print 'create EasyMonkey device'
easy_device = EasyMonkeyDevice(device)
print 'create CCIEasyMonkey device'
cci_device = CCIEasyMonkeyDevice(device)

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(5)

print 'capture subimg for later comparison'
img = device.takeSnapshot()
rect = (0, 0, 250, 50)
subimg = img.getSubImage(rect)
subimg.writeToFile('subimg', 'png')

print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#add sms button
device.touch(60,1130, MonkeyDevice.DOWN_AND_UP);
sleep(1)
#start to input Tele num,  
device.touch(150,200, MonkeyDevice.DOWN_AND_UP);
sleep(1)
device.type(num)
sleep(1)

#focus on body 
device.touch(150,690, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#send a longer msg
for x in range(3):
    device.type('Notification')
    device.press('KEYCODE_SPACE', MonkeyDevice.DOWN_AND_UP)
    device.type('test')
    device.press('KEYCODE_SPACE', MonkeyDevice.DOWN_AND_UP)
    sleep(1)

print 'send message'
#send buttom
device.touch(660,610, MonkeyDevice.DOWN_AND_UP);
sleep(1)

# back 3 times to home screen
# twice back key, return to messaging screen, wait longer to capture picture
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)





PASS=False
for i in range(10):
  sleep(2)

  #capture picture of status bar
  #maybe we can pre-save a img for comparison
  print 'capture subimg2'
  img2 = device.takeSnapshot()
  rect = (0, 0, 250, 50)
  subimg2 = img2.getSubImage(rect)
  subimg2.writeToFile('subimg2', 'png')

  #compare 2 imgs
  if not filecmp.cmp('subimg', 'subimg2'):
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass')
    print 'Pass'
    PASS=True
    break


if not PASS:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Failed'









