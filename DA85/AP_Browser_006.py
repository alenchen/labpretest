from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys
 
device = MonkeyRunner.waitForConnection()
print "Connect to device "
 
easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"
 
cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"
print 'ensureUnlock'
Telephony.ensureUnlock(device) 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch browser ap'
device.touch(618, 1108, MonkeyDevice.DOWN_AND_UP);
sleep(2)

#touch web address line
device.touch(343, 107, MonkeyDevice.DOWN_AND_UP);
print 'webaddress'
#www.yahoo.com
device.type('www.yahoo.com')
device.press('KEYCODE_ENTER', MonkeyDevice.DOWN_AND_UP)
sleep(4)
print 'takeSnapshot(1/3): go to yahoo'
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)
print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'click settings'
cci_device.clickText('Settings')
sleep(2)
print 'click Privacy & security'
cci_device.clickText('Privacy & security')
sleep(2)

print 'click Clear history'
cci_device.clickText('Clear history')
sleep(2)

print 'click OK'
cci_device.clickText('OK')
sleep(2)
print 'takeSnapshot(2/3): clear history option pressed'
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)
print 'double back key go back to browser for check clear history'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)
cci_device.clickText('Bookmarks')
sleep(2)
cci_device.clickText('HISTORY')
sleep(2)
print 'takeSnapshot(3/3): No browser history shown'
Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

print "==============\nAP_Browser_006:\n=============="
Common.updateResult('Manual');
print 'Manual'
 

