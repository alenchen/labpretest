from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands, filecmp
from PScript import *

print 'Wait connect to device'
device = MonkeyRunner.waitForConnection()

print 'Wake up device'
device.wake()
sleep(1)

print 'Create easymonkeydevice'
easy_device = EasyMonkeyDevice(device)
sleep(1)

print 'Create CCI easymonkeydevice'
cci_device = CCIEasyMonkeyDevice(device)
sleep(1)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

Email.enterEmailAP(device, easy_device, cci_device)
res = Email.waitText(cci_device, 'Inbox')
if res == False:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'
  sys.exit(1)

####################
# local function
####################

def waitRefreshIcon(maxWaitTime=30):
  i = 0
  res2 = False
  while not res2:
    i = i + 1
    if i > maxWaitTime:
      sys.stderr.write('warning: time is over when waitting refresh icon\n')
      return False
    print 'Sleep 1 second and get picture. (%i/%i)' % (i, maxWaitTime)
    sleep(1)
    image = device.takeSnapshot()
    rect = (480, 1105, 60, 60) # x, y ,w ,h
    subimage = image.getSubImage( rect )
    subimage.writeToFile('.tmp','png')
    res2 = filecmp.cmp('.tmp', 'DA85/PScript/img/email_icon_02.png')
    os.remove(".tmp")
  return True

####################
# test case start
####################

print "touch Refresh icon"
easy_device.touch(By.id('id/refresh'),'downAndUp')
sleep(2)

res = waitRefreshIcon()

if res:
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass')
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail')
  print 'Failed'