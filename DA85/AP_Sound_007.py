from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep


def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

#print 'wake up device'
#device.wake()
#sleep(2)
#
#if LockScreen.isLock(device):
#  print 'unlock screen'
#  LockScreen.unLock(device)
#  LockScreen.waitUnLock(device)  

print 'unlock screen'
Screen_unlock()
sleep(2)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'click Settings'
device.startActivity(component='com.android.settings/.Settings')
sleep(3)

print 'click Sound'
cci_device.clickText('Sound')
sleep(5)

if Sound.isAutoUnChecked(device) :
	print 'check it'
	device.touch(436, 860, MonkeyDevice.DOWN_AND_UP);
	sleep(2)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'Press phone AP icon'
device.touch(98 ,1128, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'Focus on phone tab'
device.touch(127 ,97, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'Dial phone number'
device.touch(131, 448, MonkeyDevice.DOWN_AND_UP);
sleep(2)
device.touch(382, 448, MonkeyDevice.DOWN_AND_UP);
sleep(2)
device.touch(617, 448, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print "==============\nCheck DTMF tone Enable:\n=============="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual\n\n'



print 'click Settings'
device.startActivity(component='com.android.settings/.Settings')
sleep(3)

print 'click Sound'
cci_device.clickText('Sound')
sleep(5)

if Sound.isAutoChecked(device) :
	print 'uncheck it'
	device.touch(436, 860, MonkeyDevice.DOWN_AND_UP);
	sleep(2)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'Press phone AP icon'
device.touch(98 ,1128, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'Focus on phone tab'
device.touch(127 ,97, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'Dial phone number'
device.touch(131, 448, MonkeyDevice.DOWN_AND_UP);
sleep(2)
device.touch(382, 448, MonkeyDevice.DOWN_AND_UP);
sleep(2)
device.touch(617, 448, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print "==============\nCheck DTMF tone Disable:\n=============="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'


