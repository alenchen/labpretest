from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys


#Test 11 delete one FDN item with wrong PIN2
worngPIN2 = '7777'

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 

#click one contact
print "Click one contact"
device.touch(263, 208, MonkeyDevice.DOWN_AND_UP);
#device.clickText('CCI');
sleep(1)


#click :
print "Click :"
device.touch(680, 113, MonkeyDevice.DOWN_AND_UP);
sleep(1)


#click Delete contact
print "Click Delete contact"
#device.clickText('Delete contact');
device.touch(516, 297, MonkeyDevice.DOWN_AND_UP);
sleep(1)


#enter wrong pin2
print "input wrong PIN2"
device.type(worngPIN2)
sleep(2)


#click Done
print "Click Done"
device.touch(667, 1157, MonkeyDevice.DOWN_AND_UP);
sleep(3)



#Sim 011 Result
print "Snap shot xxx_011_FdnDelFail.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'









