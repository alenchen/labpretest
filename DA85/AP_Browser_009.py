from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys
 
device = MonkeyRunner.waitForConnection()
print "Connect to device "
easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"
cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"
print 'ensureUnlock'
Telephony.ensureUnlock(device) 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'touch browser ap'
device.touch(618, 1108, MonkeyDevice.DOWN_AND_UP);
sleep(2)

device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click Bookmarks'
cci_device.clickText('Bookmarks')
sleep(2)

print 'click HISTORY'
cci_device.clickText('HISTORY')
sleep(2)

print "Most visited"
cci_device.clickText('Most visited')
location=cci_device.getTextLocation('Most visited')
#print location[5:-1]
#(360,468)
#print location
print "==============\nAP_Browser_009:\n=============="
if Telephony.isMostV(device, 24, int(location[5:-1])-26):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Failed'
