from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands
from PScript import *

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
easy_device = EasyMonkeyDevice(device)
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  sleep(2)
  device.wake()
  sleep(1)
  device.drag(lock_start,lock_end)



def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  sleep(2)
  device.wake()
  sleep(1)
  device.drag(lock_start,lock_end)


print 'wake up device'
device.wake()
sleep(2)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
sleep(2)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'launch app list'
device.touch(360, 1105, MonkeyDevice.DOWN_AND_UP)
sleep(2)

for x in range(1, 8):
  print 'scroll left'
  Common.scrollLeft(device);
  sleep(1)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'Launch camera'
cci_device.clickText('Camera')
sleep(3)

#remove all file in camera folder
device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')

print 'click id/second_level_indicator_bar_icon'
device.touch(50, 945, MonkeyDevice.DOWN_AND_UP)
#easy_device.touch(By.id('id/second_level_indicator_bar_icon'),'downAndUp')
sleep(1)

print  'click camera settings'
device.touch(230, 910, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#from smallest resoluction
for x in range(1, 11):
  device.touch(410, 760, MonkeyDevice.DOWN_AND_UP)
  sleep(1)

#device.touch(168, 683, MonkeyDevice.DOWN_AND_UP)
# 1. None. 2. Mono 3. Sepia 4. Negative 5. Solarize 6. Posterize  7. Aqua 8. Emboss 9. Sketch 10. Neon 
colorEffect = ['Neon', 'Sketch', 'Emboss', 'Aqua', 'Posterize', 'Solarize', 'Negative', 'Sepia', 'Mono', 'None']

#result = True

for i in range(0,10):
	#device.touch(168, 683, MonkeyDevice.DOWN_AND_UP)
	print 'select ' + colorEffect[i]
	#takeSnapshot()

	#if not checkResoluction(colorEffect[i]):
	#	result = False
	
	print'take a picture'
	device.touch(335, 1070, MonkeyDevice.DOWN_AND_UP)
	sleep(3)

	device.touch(230, 910, MonkeyDevice.DOWN_AND_UP)
	sleep(1)

	device.touch(640, 760, MonkeyDevice.DOWN_AND_UP)
	sleep(1)

	#device.touch(168, 683, MonkeyDevice.DOWN_AND_UP)
	#sleep(1)


os.system("platform-tools/adb pull mnt/sdcard/DCIM/Camera/ public/" + sys.argv[1] + "/" + sys.argv[2] + "/" + sys.argv[3] + "/")

print "==============\nColor Effect:\n=============="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'


device.shell("rm sdcard/DCIM/Camera/*").split('\r\n')
device.shell('rm -r storage/sdcard0/DCIM/Camera/').split('\r\n')

print 'back to home'
for x in range(1, 5):
  device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
  sleep(1)



device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")