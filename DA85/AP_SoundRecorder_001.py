from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep

def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

Screen_unlock()

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click Sound Recorder'
device.startActivity(component='com.android.soundrecorder/.SoundRecorder')
sleep(2)

print 'press record'
device.touch(231, 1142, MonkeyDevice.DOWN_AND_UP);
sleep(10)
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

print 'press record stop'
device.touch(516, 1142, MonkeyDevice.DOWN_AND_UP);
sleep(2)
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

print 'click Done'
device.touch(568, 868, MonkeyDevice.DOWN_AND_UP);
sleep(5)

print 'click Music'
device.startActivity(component='com.android.music/com.android.music.MusicBrowserActivity')
sleep(2)

print 'press Playlists'
device.touch(655, 101, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press My recordings'
device.touch(390, 387, MonkeyDevice.DOWN)
sleep(2)
Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)

print 'press Delete'
device.touch(377, 698, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press Artists'
device.touch(100, 105, MonkeyDevice.DOWN_AND_UP)
sleep(2)

for x in range(1, 3):
  print 'press back key'
  device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
  sleep(1)

print "=========================================="
Common.updateResult('Manual');
print 'Manual'

os.system("adb shell am force-stop com.android.music")  
