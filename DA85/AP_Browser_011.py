from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys
 
 
device = MonkeyRunner.waitForConnection()
print "Connect to device " 
easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"
cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"
print 'ensureUnlock'
Telephony.ensureUnlock(device) 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'touch browser ap'
device.touch(618, 1108, MonkeyDevice.DOWN_AND_UP);
sleep(2)

device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print "Settings"
cci_device.clickText('Settings')
sleep(2)

print "General"
cci_device.clickText('General')
sleep(2)

# set homepage to blank 
print "Set homepage"
cci_device.clickText('Set homepage')
sleep(2)

print "Blank page"
cci_device.clickText('Blank page')
sleep(2)

# back to browser page
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'scrollUp'
Common.scrollUp(device)
sleep(2)
#Common.scrollDown(device)
#sleep(2)

#tabs
print 'touch 580, 120'
device.touch(580, 120, MonkeyDevice.DOWN_AND_UP);
sleep(2)

#+
print 'touch 479, 102'
device.touch(479, 102, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print "==============\nAP_Browser_011:\n=============="
if Telephony.isAboutBlank(device,105,95):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Failed'
