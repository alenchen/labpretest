from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys

#restore: must do test_0 first

PIN = '0000' 
#sys.argv[5]

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 
sleep(1)

#Disable Lock SIM Card (must do pin_test_0.py first)
easy_device.touch(By.id('id/checkbox'), 'downAndUp')
sleep(1)


#input correct SIM PIN
print "input correct simPIM"
device.type(PIN)
sleep(3)


#click ok
print "Click OK"
device.touch(528,590,'downAndUp')
#cci_device.clickText('OK')
#easy_device.touch(By.id('id/button1'), 'downAndUp')
sleep(3)


