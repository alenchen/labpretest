import os,sys
from PScript import *

g = GoogleGms.getInstance()

Log = Log.getInstance(TAG=os.path.basename(__file__).split(".")[0], levelfilter='iw')

g.waitForConnection()

g.wake()

g.unLockIfScreenLock()

g.goHome()

g.launchAppList()

Log.i('search and click gmail')
res=False
if g.search_and_click('Gmail'):
  try:
    Log.i("launch Gmail")
    g.sleep(3)
    """
    id="id/next_button"
    g.touch(id, wait=2) 

    Log.i('fill email info')

    id="id/username_edit"
    g.type('cci.sys.account@gmail.com', id)

    id="id/password_edit"
    g.type('ilovecci', id)

    id="id/next_button"
    g.touch(id)
    Log.i("touch next button")

    g.clickText("OK")
    Log.i("touch OK")
    g.sleep(20)

    id="id/skip_button"
    if g.visible(id):
      Log.i("touch skip button (Not now)")
      g.touch(id, wait=5)

    # Backup and restore
    id="id/done_button"
    Log.i("touch done button")
    g.touch(id, wait=10)
    """
    res=True
  except ValueError:
    Log.w("not find id=%s", id)
    pass

g.takeSnapshot(filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
if res:
  Log.i("TestResult: Manual")
  g.updateResult('Manual')
else:
  Log.i("TestResult: Fail")
  g.updateResult('Fail')

