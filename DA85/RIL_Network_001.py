from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys


#Test1 : Check Mobile network UI


device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

print 'ensureUnlock'
Telephony.ensureUnlock(device) 
sleep(1)

print "Click Mobile networks"
cci_device.clickText('Mobile networks')
sleep(3)



#Network 001 Result
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'




