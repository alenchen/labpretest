from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys
import re
import string
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper

num=sys.argv[4]

print 'RIL_SMSCB_010 test'
print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'
print 'create EasyMonkey device'
easy_device = EasyMonkeyDevice(device)
print 'create CCIEasyMonkey device'
cci_device = CCIEasyMonkeyDevice(device)

print 'ensureUnlock'
Telephony.ensureUnlock(device) 
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

#put setting button on home screen and enter settings
#print 'press Settings'
#device.startActivity( component='com.android.settings/.WirelessSettings' )
#sleep(1)

print 'enable airplane mode'
#device.touch(370,200, MonkeyDevice.DOWN_AND_UP);
Telephony.turnOnAirplaneMode(device, cci_device)
sleep(1)
Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
#20121024

#back twice to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#Telephony.clearSms(device)

#add sms button
device.touch(60,1130, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#start to input Tele num,  
device.touch(150,200, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#device.type(sys.argv[4])
device.type(num)
sleep(1)

#focus on body 
device.touch(150,690, MonkeyDevice.DOWN_AND_UP);
sleep(1)

device.type('Airplane')
device.press('KEYCODE_SPACE', MonkeyDevice.DOWN_AND_UP)
device.type('mode')
device.press('KEYCODE_SPACE', MonkeyDevice.DOWN_AND_UP)
device.type('enabled')

print 'send message, wait 15s'
#send buttom
device.touch(660,610, MonkeyDevice.DOWN_AND_UP);

#sleep 5 seconds to ensure msg not sent out
sleep(5)

#check db make sure msg is pending
type1 = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT type from sms where body = \"Airplane mode enabled\" AND type = 6\'')
#status1 = string.atoi(status1)

print "==============\nmo sms:\n=============="
print 'check database '
string = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT address from sms where type=6\'')

#compare number
#num = sys.argv[4]
telen=num[0:4]+' '+num[4:7]+' '+num[7:10]
p = re.compile(telen)
m = p.search(string)
print 'type1: ' + type1 

if m :
    if type1 == "":
        print 'message not sent, please check image' 
    elif int(type1) == 6 :
        print 'message not sent'
    else :
        Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
        #Common.updateResult('Fail'); 
        print 'type1 Failed' 

else :
    Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
    #Common.updateResult('Fail');
    print 'address Failed' 


#back twice to messaging ap, delete msg
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click delete all button
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(1)

device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click yes to delete all msgs
device.touch(510, 735, MonkeyDevice.DOWN_AND_UP)
sleep(1)
# back to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

#put setting button on home screen and enter settings
#print 'press Settings'
#device.startActivity( component='com.android.settings/.Settings' )
#sleep(1)

#print 'click more'
#device.touch(370,450, MonkeyDevice.DOWN_AND_UP);
#sleep(1)

#print 'disable airplane mode'
#device.touch(370,200, MonkeyDevice.DOWN_AND_UP);
#print 'enable airplane mode'
#device.touch(370,200, MonkeyDevice.DOWN_AND_UP);

Telephony.turnOffAirplaneMode(device, cci_device)
sleep(1)
Common.takeSnapshot(device, filename=sys.argv[3]+'-4', path=sys.argv[1]+'/'+sys.argv[2])
#20121024

#back twice to messaging ap, delete msg
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)

#send msg again
print 'touch sms application'
device.touch(500,1100, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#Telephony.clearSms(device)

#add sms button
device.touch(60,1130, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#start to input Tele num,  
device.touch(150,200, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#device.type(sys.argv[4])
device.type(num)
sleep(1)

#focus on body 
device.touch(150,690, MonkeyDevice.DOWN_AND_UP);
sleep(1)

device.type('Airplane')
device.press('KEYCODE_SPACE', MonkeyDevice.DOWN_AND_UP)
device.type('mode')
device.press('KEYCODE_SPACE', MonkeyDevice.DOWN_AND_UP)
device.type('disabled')

#send buttom
device.touch(660,610, MonkeyDevice.DOWN_AND_UP);


#sleep 5 seconds to ensure msg not sent out
PASS=False
for i in range(10):
  sleep(2)

#check db make sure msg is sent
  type2 = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT type from sms where body = \"Airplane mode disabled\" AND type = 2\'')

  print "==============\nmo sms:\n=============="
  print 'check database '
  string = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT address from sms where type=2\'')

#compare number
#num = sys.argv[4]
  telen=num[0:4]+' '+num[4:7]+' '+num[7:10]
  p = re.compile(telen)
  m = p.search(string)

  print 'type2: ' + type2

  if m :
    #Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
    #Common.updateResult('Pass');
    if type2 == "":
        print 'waiting to receive message' 
        break

    if int(type2) == 2:
#carrie 2012/10/13 add update pass result
        Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
        Common.updateResult('Pass');
        print 'Pass'
        PASS=True
        break

  #else :
    #Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
    #Common.updateResult('Fail');
    #print 'Failed'
    #break

if not PASS:
    Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');
    print 'Failed'

# twice back key, return to messaging screen, for more than twice test
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)
#click delete all button
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(1)

device.touch(500, 940, MonkeyDevice.DOWN_AND_UP)
sleep(1)

#click yes to delete all msgs
device.touch(510, 735, MonkeyDevice.DOWN_AND_UP)
sleep(1)
# back to home screen
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
sleep(1)


