from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep


def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

#print 'wake up device'
#device.wake()
#sleep(2)
#
#if LockScreen.isLock(device):
#  print 'unlock screen'
#  LockScreen.unLock(device)
#  LockScreen.waitUnLock(device)  

print 'unlock screen'
#Screen_unlock()
sleep(2)


print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'launch app list'
device.touch(369, 1110, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'scroll Right'
Common.scrollRight(device);
sleep(1)

print 'touch system settings'
device.touch(659, 240, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'click Sound'
cci_device.clickText('Sound')
sleep(5)

print 'click Default notification'
cci_device.clickText('Default notification')
sleep(5)

print 'scroll Up'
for x in range(0, 7):
	Common.scrollUp(device);
	sleep(1)


print 'Select ringtone'
#device.touch(328, 240, MonkeyDevice.DOWN_AND_UP);
cci_device.clickText('Pixie Dust')
sleep(2)

print 'click OK'
device.touch(508, 1127, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'click Default notification'
cci_device.clickText('Default notification')
sleep(5)

print 'Select any other ringtone'
#device.touch(360, 736, MonkeyDevice.DOWN_AND_UP);
cci_device.clickText('Procyon')
sleep(2)

print 'click OK'
device.touch(508, 1127, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'Make a MMS/SMS to DUT'
sleep(5)

print "==============\nCheck notification ringtone:\n=============="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'


