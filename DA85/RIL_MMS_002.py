from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice

from PScript import *
from time import sleep
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device = EasyMonkeyDevice(device)
print 'Create EasyMonkeyDevice'

cci_device = CCIEasyMonkeyDevice(device)
print 'Create CCIEasyMonkeyDevice'

print 'unlock screen' 
Telephony.ensureUnlock(device) 

device.startActivity(component='com.android.mms/.ui.ConversationList')
sleep(2)
#forbid click none, change sleep 1 to 2
#20121029

print 'click item 2:picture'
cci_device.clickText("1, Draft")
sleep(2)

print 'click 1,'
#cci_device.clickText("1,")
device.touch(270,220, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'delete 1,'
for i in range(5):
    device.press("KEYCODE_DEL", MonkeyDevice.DOWN_AND_UP)

sleep(2)

print 'enter phone number'
device.type(sys.argv[4])
sleep(2)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press send, change to 1m'
cci_device.clickText("MMS")

PASS=False
#10242012 Little K -- change range from 5 to 10
for i in range(3):
  sleep(20)

  print 'wake up device'
  device.wake()
  sleep(2)

  print 'check lockscreen'
  Telephony.ensureUnlock(device) 

  print 'check database '
  if Telephony.SentOK(device, "Item2:picture"):
    Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
    #10242012 Little K -- change 'Pass' to 'Manual'
    Common.updateResult('Manual');
    print 'Pass'
    PASS = True
    break

if not PASS:
  Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Manual');
  print 'Manual'

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click item 2:video'
cci_device.clickText("2, Draft")
sleep(2)

print 'click 2,'
#cci_device.clickText('2,')
device.touch(270,220, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'delete 2,'
for i in range(5):
    device.press("KEYCODE_DEL", MonkeyDevice.DOWN_AND_UP)

sleep(2)

print 'enter phone number'
device.type(sys.argv[4])
sleep(2)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press send, change to 1m'
cci_device.clickText("MMS")

PASS=False
#10242012 Little K -- change range from 5 to 10
for i in range(3):
  sleep(20)

  print 'wake up device'
  Telephony.ensureUnlock(device)  

  #print 'check database '
  if Telephony.SentOK(device, "Item2:video"):
    Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
    #10242012 Little K -- change 'Pass' to 'Manual'
    Common.updateResult('Manual');
    print 'Pass'
    PASS=True
    break

if not PASS:
  Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
  #10242012 Little K -- change 'Pass' to 'Manual'
  Common.updateResult('Manual');
  print 'Manual'

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

device.startActivity(component='com.android.mms/.ui.ConversationList')
sleep(1)

print 'click item 2:audio'
cci_device.clickText("3, Draft")
sleep(2)

print 'click 3,'
#cci_device.clickText('3,')
device.touch(270,220, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'delete 3,'
for i in range(5):
    device.press("KEYCODE_DEL", MonkeyDevice.DOWN_AND_UP)

sleep(2)

print 'enter phone number'
device.type(sys.argv[4])
sleep(2)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press send, change to 1 min'
cci_device.clickText("MMS")

PASS=False
#10242012 Little K -- change range from 5 to 10
for i in range(3):
  sleep(20)

  print 'wake up device'
  device.wake()
  sleep(2)

  print 'check lockscreen'
  Telephony.ensureUnlock(device) 

  #print 'check database '
  if Telephony.SentOK(device, "Item2:audio"):
    Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
    #10242012 Little K -- change 'Pass' to 'Manual'
    Common.updateResult('Manual');
    print 'Pass'
    PASS=True
    break

if not PASS:
  Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Manual');
  print 'Manual'

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

