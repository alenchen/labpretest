
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from PScript import *
from time import sleep
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device=EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device=CCIEasyMonkeyDevice("ss",device)
print "Create CCIEasyMonkeyDevice"

print 'wake up device'
device.wake()
sleep(2)

print 'ensureUnlock'
Telephony.ensureUnlock(device)

print 'press head icon'
cci_device.clickText('Shinjyo')
sleep(2)

print 'press remove favorite'
device.touch(575, 100, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'press back'
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP);
sleep(2)

Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(2)
if cci_device.isTextVisible('No favorites.'):
     print 'Remove a favorite pass'
     Common.updateResult('Pass')
else:
     print 'Remove a favorite failure'
     Common.updateResult('Fail')
sleep(2)
print 'scroll Right'
Common.scrollLeft(device)
sleep(2)
