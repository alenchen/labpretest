from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

print 'wake up device'
device.wake()
sleep(2)

if LockScreen.isLock(device):
  print 'unlock screen'
  LockScreen.unLock(device)
  LockScreen.waitUnLock(device)  


print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch system settings'
device.touch(352, 1155, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'click More'
cci_device.clickText('More')
sleep(5)

if NFC.isAutoChecked(device) :
	print "==============\ndefault NFC is checked\n=============="
	print 'click NFC to disable'
	cci_device.clickText('NFC')
	sleep(5)
	print 'click NFC to enable again'
	cci_device.clickText('NFC')
	sleep(5)
else :		
	print "==============\ndefault NFC is unchecked\n=============="
	print 'click NFC to enable'
	cci_device.clickText('NFC')
	sleep(5)
	
print "==============\nTest result\n=============="
if NFC.isAutoChecked(device) :
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
    print 'pass'
else :
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');
    print 'false'
