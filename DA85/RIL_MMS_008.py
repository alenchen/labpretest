from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice

from PScript import *
from time import sleep
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

easy_device =EasyMonkeyDevice(device)
print 'Create EasyMonkeyDevice'

cci_device = CCIEasyMonkeyDevice(device)
print 'Create CCIEasyMonkeyDevice'

print 'check lockscreen'
Telephony.ensureUnlock(device) 

##
print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)
#disable auto retrieve
print 'startActivity ComposeMessageActivity'
device.startActivity( component='com.android.mms/.ui.ComposeMessageActivity' )
sleep(2)

print 'press menu key'
device.press("KEYCODE_MENU", MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'click Settings'
cci_device.clickText('Settings')
sleep(2)
cci_device.clickText('Auto-retrieve')

print 'scrollDown'
Common.scrollDown(device)
sleep(2)

print 'click Auto-retrieve'
cci_device.clickText('Auto-retrieve')
sleep(2)

device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)
sleep(2)
device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)
sleep(2)

#compose mms and send it

device.startActivity( component='com.android.mms/.ui.ComposeMessageActivity' )
sleep(2)
#(focused on To.)
# enter caller number
device.press("KEYCODE_MENU", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click Add subject'
cci_device.clickText('Add subject')
sleep(2)

#subject 
device.type('Item8')
sleep(2)

#focus on To
#cci_device.clickText('To')
device.touch(270,220, MonkeyDevice.DOWN_AND_UP);
sleep(2)
#To
device.type(sys.argv[4])
sleep(2)

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press send, change to 1m'
cci_device.clickText("MMS")

PASS=False
for i in range(5):
  sleep(20)

  print 'wake up device'
  device.wake()
  sleep(2)

  print 'check lockscreen'
  Telephony.ensureUnlock(device) 

  #print 'check database '
  if Telephony.SentOK(device, "Item8") and Telephony.RecvSubOK(device, "Item8"):
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
    print 'Pass'
    PASS=True
    break

if not PASS :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Manual');
  print 'Manual'

print 'press BACK key'
device.press("KEYCODE_BACK", MonkeyDevice.DOWN_AND_UP)
sleep(2)

