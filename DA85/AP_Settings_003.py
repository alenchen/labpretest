from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
import filecmp
import sys,os,commands
from PScript import *

device = MonkeyRunner.waitForConnection()

easy_device = EasyMonkeyDevice(device)

cci_device = CCIEasyMonkeyDevice(device)

cci_device.setIsFullTextMatch(True)

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def dragToLeft():
    #drag to left
    device.drag((181,756),(543,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((181,756),(543,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((181,756),(543,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((181,756),(543,756),0.1,10)
    MonkeyRunner.sleep(1)

    device.drag((181,756),(543,756),0.1,10)
    MonkeyRunner.sleep(1)

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  MonkeyRunner.sleep(2)
  device.wake()
  MonkeyRunner.sleep(2)
  device.drag(lock_start,lock_end)

print 'wake up device'
device.wake()
MonkeyRunner.sleep(2)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
MonkeyRunner.sleep(2)

#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

#install apk
os.system("adb install DA85/sys/data/AP_Settings_003/LEDTest.apk")

###### search and excute apk
#click all app 
device.touch(359,1106,'downAndUp')
MonkeyRunner.sleep(2)

#click APPS 
device.touch(65,106,'downAndUp')
MonkeyRunner.sleep(2)

dragToLeft()

#search Calculator
for i in range(5):
    if (cci_device.isTextVisible('LEDTest')):
        cci_device.clickText('LEDTest')
        MonkeyRunner.sleep(2)
        cci_device.clickText('Base Test')
        MonkeyRunner.sleep(2)
        device.touch(87,275,'downAndUp')
        MonkeyRunner.sleep(2)
        #selected yallow color
        device.touch(195,614,'downAndUp')
        MonkeyRunner.sleep(2)
        device.touch(155,683,'downAndUp')
        MonkeyRunner.sleep(2)
        cci_device.clickText('OK')
        MonkeyRunner.sleep(2)
        #click back key
        device.press('KEYCODE_BACK','downAndUp')
        MonkeyRunner.sleep(2)
        device.press('KEYCODE_BACK','downAndUp')
        MonkeyRunner.sleep(2)
        #back to home
        device.press('KEYCODE_HOME','downAndUp')
        MonkeyRunner.sleep(2)
        break
    else:
        device.drag((543,756),(181,756),0.1,10)
        MonkeyRunner.sleep(1)

#click Menu key
device.press('KEYCODE_MENU','downAndUp')
MonkeyRunner.sleep(2)

#come to setting
cci_device.clickText('System settings')
MonkeyRunner.sleep(2)

#drag to Apps location
device.drag((359,497),(359,1019),0.1,10)
MonkeyRunner.sleep(1)

#enter Apps
cci_device.clickText('Apps')
MonkeyRunner.sleep(2)

#click APK
cci_device.clickText('LEDTest')
MonkeyRunner.sleep(2)

#Clear data
#saving before clear data
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])

cci_device.clickText('Clear data')
MonkeyRunner.sleep(2)

cci_device.clickText('OK')
MonkeyRunner.sleep(2)

#saving after clear data
Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])

Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');

#click back key
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)
#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)
        
#uninstall apk
os.system("adb uninstall com.cci.ledtest")

