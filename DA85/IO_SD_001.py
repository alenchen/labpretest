from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
import sys,os,commands
from time import sleep
from PScript import *

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"

model = device.getSystemProperty('ro.product.model')
print "The ro.product.model property on device = " + model


cci_device.setIsFullTextMatch(False)

device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(1)

device.press('KEYCODE_MENU','downAndUp')
MonkeyRunner.sleep(1)

print "System settings location = " + cci_device.getTextLocation('System settings')
MonkeyRunner.sleep(1)
cci_device.clickText('System settings')

MonkeyRunner.sleep(1)

cci_device.clickText('Storage')
MonkeyRunner.sleep(1)

device.drag((450,860),(450,120),0.1,10)

MonkeyRunner.sleep(1)

if cci_device.isTextVisible('Unmount SD card'):
	
	cci_device.clickText('Unmount SD card')
	print "Unmounting SD card"
	MonkeyRunner.sleep(1)
	cci_device.clickText('OK')
	MonkeyRunner.sleep(5)
	cci_device.clickText('Mount SD card')
	print "Mounting SD card"
	print "Pass"
	Common.updateResult('Pass');
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
else:
	print "Failed"
	Common.updateResult('Fail');
	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	

MonkeyRunner.sleep(1)

device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(1)
