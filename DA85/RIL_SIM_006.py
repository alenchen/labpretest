from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice


from PScript import *
from time import sleep
import sys

wrongPUK='12345678'
#PIN = sys.argv[5]
PIN = '0000'

#Test6: Unit can't unlock and change PIN with Wrong PUK

device = MonkeyRunner.waitForConnection()
print "Connect to device "

easy_device = EasyMonkeyDevice(device)
print "Create EasyMonkeyDevice"

cci_device = CCIEasyMonkeyDevice(device)
print "Create CCIEasyMonkeyDevice"


print 'ensureUnlock'
Telephony.ensureUnlock(device) 

#input Wrong PUK
print "input wrong SIM PUK"
device.type(wrongPUK)
sleep(1)


#click Edit for input PIN
#easy_device.touch(By.id('id/pinDisplay'), 'downAndUp')
device.touch(267,384,'downAndUp')
sleep(1)


#input New SIM PIN
print "input New SIM PIN"
device.type(PIN)
sleep(1)


#click ok
print "Click OK"
cci_device.clickText('OK')
sleep(3)


#Sim 006 Result
print "Snap shot xxx_006_ErrorPUK.png"
print "=========================================="
Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
Common.updateResult('Manual');
print 'Manual'
sleep(3)












