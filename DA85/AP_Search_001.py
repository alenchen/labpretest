from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
import filecmp
import sys,os,commands
from PScript import *

device = MonkeyRunner.waitForConnection()

easy_device = EasyMonkeyDevice(device)

cci_device = CCIEasyMonkeyDevice(device)

cci_device.setIsFullTextMatch(True)

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  MonkeyRunner.sleep(2)
  device.wake()
  MonkeyRunner.sleep(2)
  device.drag(lock_start,lock_end)

print 'wake up device'
device.wake()
MonkeyRunner.sleep(2)

print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
screenUnlock(device,lock_start, lock_end)
MonkeyRunner.sleep(2)

#back to home
print 'back to home'
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

#click "people" ap
print 'click [people] ap'
device.touch(229,1109,'downAndUp')
MonkeyRunner.sleep(2)

#check had not "Make it Google"
print 'check had not [Make it Google] item'
if(cci_device.isTextVisible('Make it Google')):
    cci_device.clickText('Not now')
    MonkeyRunner.sleep(2)

print 'check had [Create a new contact] item'
if(cci_device.isTextVisible('Create a new contact')):
    #click "Add people" ap
    cci_device.clickText('Create a new contact')
    MonkeyRunner.sleep(2)

    #click "Keep local" ap
    cci_device.clickText('Keep local')
    MonkeyRunner.sleep(2)
else:
    print "Had contact case"
    device.touch(359,1136,'downAndUp')
    MonkeyRunner.sleep(2)
    
    #click "OK"
    cci_device.clickText('OK')
    MonkeyRunner.sleep(2)

#click 'aaa'
device.type('a')
device.type('a')
device.type('a')
device.type('a')
MonkeyRunner.sleep(2)

#back to people
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)
device.press('KEYCODE_BACK','downAndUp')
MonkeyRunner.sleep(2)

#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

#click google search 
device.touch(360,116,'downAndUp')
MonkeyRunner.sleep(8)


print 'check had account on the google search'
res=False
for i in range (0, 50):
    if(cci_device.isTextVisible('Discover Google Now!')):
        print 'had find [Discover Google Now!] item'
        res=True
        break;
    MonkeyRunner.sleep(5)

if(res):
    #enter 'Next'
    for i in range (0, 50):
        if(cci_device.isTextVisible('Next')):
            print 'had find [Next] item'
            cci_device.clickText('Next')
            break;
        MonkeyRunner.sleep(5)
    
    #enter 'Next'
    for i in range (0, 50):
        if(cci_device.isTextVisible('Next')):
            print 'had find [Next] item'
            cci_device.clickText('Next')
            break;
        MonkeyRunner.sleep(5)
    
    #enter 'Next'
    for i in range (0, 50):
        if(cci_device.isTextVisible('Next')):
            print 'had find [Next] item'
            cci_device.clickText('Next')
            break;
        MonkeyRunner.sleep(5)
    
    cci_device.setIsFullTextMatch(False)
    #enter 'Yes,I'm in.'
    for i in range (0, 50):
        if(cci_device.isTextVisible('Yes')):
            print 'had find [Yes] item'
            cci_device.clickText('Yes')
            break;
        MonkeyRunner.sleep(5)
    
    #enter 'Location service is off'
    for i in range (0, 50):
        if(cci_device.isTextVisible('Location service is off')):
            print 'had find [Location service is off] item'
            break;
        MonkeyRunner.sleep(5)
    
    cci_device.setIsFullTextMatch(True)

    #back to home
    device.press('KEYCODE_BACK','downAndUp')
    MonkeyRunner.sleep(2)

    #click google search 
    device.touch(360,116,'downAndUp')
    MonkeyRunner.sleep(2)

#send 'aaa'
device.type('a')
device.type('a')
device.type('a')
MonkeyRunner.sleep(8)

#compare contact
if(cci_device.isTextVisible('aaaa')):
  print "Pass"
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
else:
  print "Fail" 
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');   
#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)

#click "people" ap
device.touch(229,1109,'downAndUp')
MonkeyRunner.sleep(2)

#enter 'aaaa' contact
cci_device.clickText('aaaa')
MonkeyRunner.sleep(2)

#click Menu key
device.press('KEYCODE_MENU','downAndUp')
MonkeyRunner.sleep(2)

#click Delete
cci_device.clickText('Delete')
MonkeyRunner.sleep(2)

#click ok
cci_device.clickText('OK')
MonkeyRunner.sleep(2)

#back to home
device.press('KEYCODE_HOME','downAndUp')
MonkeyRunner.sleep(2)
