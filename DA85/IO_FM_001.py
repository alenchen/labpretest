from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

print 'wake up device'
device.wake()
sleep(2)

print 'press power key to lock'
device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
sleep(2)
print 'wake up device'
device.wake()
sleep(2)
print 'screen unlock'
lock_start = (355, 890)
lock_end = (620, 890)
device.drag(lock_start,lock_end)
sleep(2)


if LockScreen.isLock(device):
  print 'unlock screen'
  LockScreen.unLock(device)
  LockScreen.waitUnLock(device)  



print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press menu'
device.touch(375, 1100, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'click FM Radio'
cci_device.clickText('FM Radio')
sleep(3)

print 'check headset'
if cci_device.isTextVisible('please plug'):
	print "headset didn't plug in!"
else:
	print "found headset"


print 'press FM option'
device.touch(665, 100, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'click FM Settings'
cci_device.clickText('Settings')
sleep(3)

print 'click Revert to Factory Defaults'
cci_device.clickText('Revert to Factory Defaults')
sleep(3)

print 'press OK'
device.touch(500, 750, MonkeyDevice.DOWN_AND_UP);
sleep(2)


#print 'turn on FM'
#device.touch(500, 1050, MonkeyDevice.DOWN_AND_UP);
#sleep(2)

print 'set frequency'
device.touch(350, 370, MonkeyDevice.DOWN);
sleep(2)

print 'set frequency'
device.touch(285, 610, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'input 88 frequency'
device.type('88')
sleep(2)

print 'set frequency'
device.touch(450, 400, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'clear frequency'
device.touch(670, 1020, MonkeyDevice.DOWN_AND_UP);
sleep(2)


print 'input 9 frequency'
device.type('9')
sleep(2)

print 'set frequency'
device.touch(200, 650, MonkeyDevice.DOWN_AND_UP);
sleep(3)

print 'scan'
device.touch(667, 375, MonkeyDevice.DOWN);
sleep(5)

print 'scan result:'
if cci_device.isTextVisible('89.3'):
#	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Pass');
        print "pass"
else:
#	Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
	Common.updateResult('Fail');
        print "fail"

