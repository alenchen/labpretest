from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
from time import sleep
import sys,os,commands
from PScript import *
import filecmp

def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)

def DisplaySleep(option ):
	print 'press home key'
	device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
	sleep(0.5)

	print 'Find and click Settings'
	device.startActivity(component='com.android.settings/.Settings')

	print 'Click display'
	cci_device.clickText('Display')
	sleep(1)

	print 'Click Sleep'
	cci_device.clickText('Sleep')
	sleep(1)

	cci_device.clickText(option)
	sleep(1)

	print 'press home key'
	device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
	sleep(0.5)

	print 'Launch Gallery'
	device.startActivity(component='com.android.gallery3d/.app.Gallery')

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

Screen_unlock()
  
print 'check sdcard in device'
pathExist = device.shell("ls storage/sdcard0").split('\r\n')
if pathExist is None :
	print 'No sdcard found.'
	Common.updateResult('Fail');
	sys.exit(1)
		
print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)
cci_device.setIsFullTextMatch(True)

print 'Launch Gallery'
device.startActivity(component='com.android.gallery3d/.app.Gallery')

print 'remove all media files in device'
device.shell("rm -r /storage/sdcard0/Pictures/*")
device.shell("rm -r /storage/sdcard0/Music/*")
device.shell("rm -r /storage/sdcard0/DCIM/Camera/*")
device.shell("rm -r /storage/sdcard0/Movies/*")
sleep(1)
print 'Send Intent'
device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")
sleep(2)

print 'push video into device'
os.system("adb push DA85/video/1.* /storage/sdcard0/Movies/")
sleep(5)
pictureExist = device.shell("ls storage/sdcard0/Movies").split('\r\n')
if pictureExist:
	print 'Send Intent'
	device.shell("am broadcast -a android.intent.action.MEDIA_MOUNTED --ez read-only faulse -d file:///sdcard")
	sleep(2)		
else :
	print 'push video fail'

print 'click Album "video"'
device.touch(345, 700, MonkeyDevice.DOWN_AND_UP)
sleep(3)
device.touch(345, 700, MonkeyDevice.DOWN_AND_UP)
sleep(3)

print 'play video'
device.touch(355, 620, MonkeyDevice.DOWN_AND_UP)
sleep(10)

print 'set display sleep time to 15 seconds'
DisplaySleep('15 seconds')

print 'continue playing video...'

sleep(2)

print 'Wait 20 seconds...'

sleep(20)

print 'Device should not sleep when playing video'

image = device.takeSnapshot()
rect =  (0, 280, 720, 720)
subimage = image.getSubImage( rect )
subimage.writeToFile('.tmp1','png')
Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])

res = filecmp.cmp('.tmp1','DA85/PScript/img/display_off.png')
if not res:
	print 'Pass'
	Common.updateResult('Pass');
else :
	print 'Fail'
	Common.updateResult('Fail');

Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])	

os.remove(".tmp1")

print 'set display sleep time to 30 minutes'
DisplaySleep('30 minutes')

for x in range(0, 5):
	device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)
	sleep(0.5)