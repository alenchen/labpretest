# Part 1: Define mail list
add_maillist aaron_chen
add_maillist alen2_chen
add_maillist alumincan_leu
add_maillist andy_chen
add_maillist colby_lee
add_maillist edward_chou
add_maillist figo_chang
add_maillist jeffrey_huang
add_maillist jim_lai
add_maillist linus_huang
add_maillist ouyang_jeng
add_maillist tinny_tseng

# Part 2: Define test case
# ====== Group - AP_Gallery ======
insert_testcase "AP_Gallery_001" "MM" "AP_Gallery_001.py" "" 1
insert_testcase "AP_Gallery_002" "MM" "AP_Gallery_002.py" "" 1
insert_testcase "AP_Gallery_003" "MM" "AP_Gallery_003.py" "" 1
insert_testcase "AP_Gallery_004" "MM" "AP_Gallery_004.py" "" 1
#(not auto)insert_testcase "AP_Gallery_005" "MM" "AP_Gallery_005.py" "" 1
#(not auto)insert_testcase "AP_Gallery_006" "MM" "AP_Gallery_006.py" "" 1
insert_testcase "AP_Gallery_007" "MM" "AP_Gallery_007.py" "" 1
insert_testcase "AP_Gallery_008" "MM" "AP_Gallery_008.py" "" 1
insert_testcase "AP_Gallery_009" "MM" "AP_Gallery_009.py" "" 1
insert_testcase "AP_Gallery_010" "MM" "AP_Gallery_010.py" "" 1
insert_testcase "AP_Gallery_011" "MM" "AP_Gallery_011.py" "" 1
insert_testcase "AP_Gallery_012" "MM" "AP_Gallery_012.py" "" 1
insert_testcase "AP_Gallery_013" "MM" "AP_Gallery_013.py" "" 1
insert_testcase "AP_Gallery_014" "MM" "AP_Gallery_014.py" "" 1
insert_testcase "AP_Gallery_015" "MM" "AP_Gallery_015.py" "" 1
insert_testcase "AP_Gallery_016" "MM" "AP_Gallery_016.py" "" 1
insert_testcase "AP_Gallery_017" "MM" "AP_Gallery_017.py" "" 1
insert_testcase "AP_Gallery_018" "MM" "AP_Gallery_018.py" "" 1
insert_testcase "AP_Gallery_019" "MM" "AP_Gallery_019.py" "" 1

# ====== Group - AP_Music ======
insert_testcase "AP_Music_001" "MM" "AP_Music_001.py" "" 1
insert_testcase "AP_Music_002" "MM" "AP_Music_002.py" "" 1
insert_testcase "AP_Music_003" "MM" "AP_Music_003.py" "" 1
insert_testcase "AP_Music_004" "MM" "AP_Music_004.py" "" 1
insert_testcase "AP_Music_005" "MM" "AP_Music_005.py" "" 1
insert_testcase "AP_Music_006" "MM" "AP_Music_006.py" "" 1
#(not auto)insert_testcase "AP_Music_007" "MM" "AP_Music_007.py" "" 1
insert_testcase "AP_Music_008" "MM" "AP_Music_008.py" "" 1
#(not auto)insert_testcase "AP_Music_009" "MM" "AP_Music_009.py" "" 1
insert_testcase "AP_Music_010" "MM" "AP_Music_010.py" "" 1
insert_testcase "AP_Music_011" "MM" "AP_Music_011.py" "" 1
insert_testcase "AP_Music_012" "MM" "AP_Music_012.py" "" 1

# ====== Group - AP_Sound ======
#(not auto)insert_testcase "AP_Sound_001" "MM" "AP_Sound_001.py" "" 1
#(not auto)insert_testcase "AP_Sound_002" "MM" "AP_Sound_002.py" "" 1
#(not auto)insert_testcase "AP_Sound_003" "MM" "AP_Sound_003.py" "" 1
insert_testcase "AP_Sound_004" "MM" "AP_Sound_004.py" "" 1
#(not auto)insert_testcase "AP_Sound_005" "MM" "AP_Sound_005.py" "" 1
#(not auto)insert_testcase "AP_Sound_006" "MM" "AP_Sound_006.py" "" 1
insert_testcase "AP_Sound_007" "MM" "AP_Sound_007.py" "" 1

# ====== Group - AP_SoundRecorder ======
insert_testcase "AP_SoundRecorder_001" "MM" "AP_SoundRecorder_001.py" "" 1

# ====== Group - AP_Video ======
#(not auto)insert_testcase "AP_Video_001" "MM" "AP_Video_001.py" "" 1
insert_testcase "AP_Video_002" "MM" "AP_Video_002.py" "" 1
insert_testcase "AP_Video_003" "MM" "AP_Video_003.py" "" 1
insert_testcase "AP_Video_004" "MM" "AP_Video_004.py" "" 1
insert_testcase "AP_Video_005" "MM" "AP_Video_005.py" "" 1
insert_testcase "AP_Video_006" "MM" "AP_Video_006.py" "" 1
insert_testcase "AP_Video_007" "MM" "AP_Video_007.py" "" 1
insert_testcase "AP_Video_008" "MM" "AP_Video_008.py" "" 1
insert_testcase "AP_Video_009" "MM" "AP_Video_009.py" "" 1
#(not auto)insert_testcase "AP_Video_010" "MM" "AP_Video_010.py" "" 1
insert_testcase "AP_Video_011" "MM" "AP_Video_011.py" "" 1
insert_testcase "AP_Video_012" "MM" "AP_Video_012.py" "" 1
insert_testcase "AP_Video_013" "MM" "AP_Video_013.py" "" 1
insert_testcase "AP_Video_014" "MM" "AP_Video_014.py" "" 1
#(not auto)insert_testcase "AP_Video_015" "MM" "AP_Video_015.py" "" 1
#(not auto)insert_testcase "AP_Video_016" "MM" "AP_Video_016.py" "" 1

# ====== Group - BSP_Audio ======
#(not auto)insert_testcase "BSP_Audio_001" "MM" "BSP_Audio_001.py" "" 1
#(not auto)insert_testcase "BSP_Audio_002" "MM" "BSP_Audio_002.py" "" 1
#(not auto)insert_testcase "BSP_Audio_003" "MM" "BSP_Audio_003.py" "" 1
#(not auto)insert_testcase "BSP_Audio_004" "MM" "BSP_Audio_004.py" "" 1
#(not auto)insert_testcase "BSP_Audio_005" "MM" "BSP_Audio_005.py" "" 1
#(not auto)insert_testcase "BSP_Audio_006" "MM" "BSP_Audio_006.py" "" 1
#(not auto)insert_testcase "BSP_Audio_007" "MM" "BSP_Audio_007.py" "" 1
#(not auto)insert_testcase "BSP_Audio_008" "MM" "BSP_Audio_008.py" "" 1
#(not auto)insert_testcase "BSP_Audio_009" "MM" "BSP_Audio_009.py" "" 1
#(not auto)insert_testcase "BSP_Audio_010" "MM" "BSP_Audio_010.py" "" 1

# ====== Group - BSP_BKL ======
insert_testcase "BSP_BKL_001" "MM" "BSP_BKL_001.py" "" 1
#(not auto)insert_testcase "BSP_BKL_002" "MM" "BSP_BKL_002.py" "" 1
#(not auto)insert_testcase "BSP_BKL_003" "MM" "BSP_BKL_003.py" "" 1
insert_testcase "BSP_BKL_004" "MM" "BSP_BKL_004.py" "" 1

# ====== Group - BSP_Camera ======
insert_testcase "BSP_Camera_001" "MM" "BSP_Camera_001.py" "" 1
insert_testcase "BSP_Camera_002" "MM" "BSP_Camera_002.py" "" 1
insert_testcase "BSP_Camera_003" "MM" "BSP_Camera_003.py" "" 1
insert_testcase "BSP_Camera_004" "MM" "BSP_Camera_004.py" "" 1
insert_testcase "BSP_Camera_005" "MM" "BSP_Camera_005.py" "" 1
insert_testcase "BSP_Camera_006" "MM" "BSP_Camera_006.py" "" 1
insert_testcase "BSP_Camera_007" "MM" "BSP_Camera_007.py" "" 1
insert_testcase "BSP_Camera_008" "MM" "BSP_Camera_008.py" "" 1

# ====== Group - BSP_LCD ======
insert_testcase "BSP_LCD_001" "MM" "BSP_LCD_001.py" "" 1
#(not auto)insert_testcase "BSP_LCD_002" "MM" "BSP_LCD_002.py" "" 1
#(not auto)insert_testcase "BSP_LCD_003" "MM" "BSP_LCD_003.py" "" 1
insert_testcase "BSP_LCD_004" "MM" "BSP_LCD_004.py" "" 1

# ====== Group - BSP_Touch ======
#(not auto)insert_testcase "BSP_Touch_001" "MM" "BSP_Touch_001.py" "" 1
#(not auto)insert_testcase "BSP_Touch_002" "MM" "BSP_Touch_002.py" "" 1
#(not auto)insert_testcase "BSP_Touch_003" "MM" "BSP_Touch_003.py" "" 1
#(not auto)insert_testcase "BSP_Touch_004" "MM" "BSP_Touch_004.py" "" 1
#(not auto)insert_testcase "BSP_Touch_005" "MM" "BSP_Touch_005.py" "" 1
#(not auto)insert_testcase "BSP_Touch_006" "MM" "BSP_Touch_006.py" "" 1

# ====== Group - AP_Camera ======
insert_testcase "AP_Camera_001" "MM" "AP_Camera_001.py" "" 1
insert_testcase "AP_Camera_002" "MM" "AP_Camera_002.py" "" 1
insert_testcase "AP_Camera_003" "MM" "AP_Camera_003.py" "" 1
insert_testcase "AP_Camera_004" "MM" "AP_Camera_004.py" "" 1
insert_testcase "AP_Camera_005" "MM" "AP_Camera_005.py" "" 1
insert_testcase "AP_Camera_006" "MM" "AP_Camera_006.py" "" 1
insert_testcase "AP_Camera_007" "MM" "AP_Camera_007.py" "" 1
insert_testcase "AP_Camera_008" "MM" "AP_Camera_008.py" "" 1
insert_testcase "AP_Camera_009" "MM" "AP_Camera_009.py" "" 1
