import os
import sys
import commands
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1) 
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]


print "[EXPORT] Connect to " + device_id
device = MonkeyRunner.waitForConnection(10, device_id)
print "[EXPORT] Connect ok " + device_id

print "Create easymonkeydevice"
easy_device = EasyMonkeyDevice(device)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device) #To setup internal data structure
							#Hard-code location search funcion in this stage

device.press('KEYCODE_HOME','DOWN_AND_UP')
MonkeyRunner.sleep(2)

device.touch(270,828,'DOWN_AND_UP')
MonkeyRunner.sleep(2)

device.drag((10,420),(520,420),0.1,10)
MonkeyRunner.sleep(1)
device.drag((10,420),(520,420),0.1,10)
MonkeyRunner.sleep(1)
device.drag((10,420),(520,420),0.1,10)
MonkeyRunner.sleep(1)

device.drag((520,420),(10,420),0.1,10)
MonkeyRunner.sleep(1)

cci_device.clickText('messag')
MonkeyRunner.sleep(2)

device.press('KEYCODE_MENU','DOWN_AND_UP','')
MonkeyRunner.sleep(2)

cci_device.clickText('setting')
MonkeyRunner.sleep(2)

cci_device.clickText('delivery report')
MonkeyRunner.sleep(2)

cci_device.clickText('Notifications')
MonkeyRunner.sleep(2)


device.press('KEYCODE_BACK','DOWN_AND_UP','')
MonkeyRunner.sleep(3)


cci_device.clickText('new conv')
MonkeyRunner.sleep(2)

easy_device.touch(By.id('id/recipients_editor'), 'downAndUp')
MonkeyRunner.sleep(2)

device.type('1')
device.type('2')
device.type('3')
device.type('4')
device.type('5')
device.type('6')
device.type('7')
MonkeyRunner.sleep(2)

easy_device.touch(By.id('id/conversation_edit_text'), 'downAndUp')
MonkeyRunner.sleep(2)

device.type('a')
device.type('b')
device.type('c')
device.type('d')
device.type('e')
device.type('f')
device.type('g')
MonkeyRunner.sleep(2)

cci_device.clickText('send')
MonkeyRunner.sleep(5)

device.press('KEYCODE_BACK','DOWN_AND_UP','')
MonkeyRunner.sleep(2)

device.press('KEYCODE_HOME','DOWN_AND_UP','')
MonkeyRunner.sleep(2)

device.press('KEYCODE_MENU','DOWN_AND_UP','')
MonkeyRunner.sleep(2)

cci_device.clickText('setting')
MonkeyRunner.sleep(2)

cci_device.clickText('Display')
MonkeyRunner.sleep(2)

cci_device.clickText('rotate')
MonkeyRunner.sleep(2)

cci_device.clickText('sleep')
MonkeyRunner.sleep(2)

cci_device.clickText('2 minute')
MonkeyRunner.sleep(2)

device.press('KEYCODE_BACK','DOWN_AND_UP','')
MonkeyRunner.sleep(2)

device.drag((450,860),(450,120),0.1,10)
MonkeyRunner.sleep(1)
device.drag((450,860),(450,120),0.1,10)
MonkeyRunner.sleep(1)

cci_device.clickText('about phone')
MonkeyRunner.sleep(5)

device.drag((450,860),(450,120),0.1,10)
MonkeyRunner.sleep(1)

if cci_device.isTextVisible('012-World Wide-Dell', True):
	print "Build number correct"
else:
	print "Build number incorrect"
