var key_list = [["Home", "device.press('KEYCODE_HOME',MonkeyDevice.DOWN_AND_UP)"],
                ["Back", "device.press('KEYCODE_BACK',MonkeyDevice.DOWN_AND_UP)"],
                ["Menu", "device.press('KEYCODE_MENU',MonkeyDevice.DOWN_AND_UP)"],
                ["Power", "device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)"],
                ["Vol+", "device.press('KEYCODE_VOLUME_UP',MonkeyDevice.DOWN_AND_UP)"],
                ["Vol-", "device.press('KEYCODE_VOLUME_DOWN',MonkeyDevice.DOWN_AND_UP)"]];

var ap_list = [["Browser", "device.startActivity(component='com.android.browser/.BrowserActivity')"],
               ["Calculator", "device.startActivity(component='com.android.calculator2/.Calculator')"],
               ["Calendar", "device.startActivity( component='com.android.calendar/.AllInOneActivity'"],
               ["Camera", "cci_device=CCIEasyMonkeyDevice(device)", "cci_device.clickText('Camera')", "sleep(3)"],
               ["Contacts", "device.startActivity(component='com.android.contacts/.activities.PeopleActivity')"],
               ["Email", "device.startActivity(component='com.android.email/.activity.Welcome')"],
               ["Gallery", "device.startActivity(component='com.android.gallery3d/.app.Gallery')"],
               ["Music", "device.startActivity(component='com.android.music/com.android.music.MusicBrowserActivity')"],
               ["Phone", "device.startActivity(component='com.android.contacts/.activities.DialtactsActivity')"],
               ["Settings", "device.startActivity(component='com.android.settings/.Settings')"],
               ["Recorder", "device.startActivity(component='com.android.soundrecorder/.SoundRecorder')"],
               ["Alarm", "device.startActivity(component='com.android.deskclock/.SetAlarm')"],
               ["Message", "device.startActivity( component='com.android.mms/.ui.ComposeMessageActivity')"],
               ["Other"]];

var swipe_list = [["Right to Left", "device.drag((265,596),(500,596),0.1,10)"],
                  ["Left to Right", "device.drag((500,596),(265,596),0.1,10)"],
                  ["Top to Bottom", "device.drag((412,430),(412,750),0.1,10)"],
                  ["Bottom to Top", "device.drag((412,750),(412,430),0.1,10)"]];

var device_list = [["Unlock", "device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)", "sleep(2)", "device.wake()", "sleep(1)", "device.shell('input keyevent 82')"],
                   ["Reboot", "device.shell('reboot')"],
                   ["Screenshot", "device.takeSnapshot()"],
                   ["Suspend", "device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)"],
                   ["Wake up", "device.wake()"],
                   ["Touch"]];

var sleep_list = [["0 second", "sleep(0)"],
                  ["1 second", "sleep(1)"],
                  ["2 seconds", "sleep(2)"],
                  ["3 seconds", "sleep(3)"],
                  ["4 seconds", "sleep(4)"],
                  ["5 seconds", "sleep(5)"],
                  ["6 seconds", "sleep(6)"],
                  ["7 seconds", "sleep(7)"],
                  ["8 seconds", "sleep(8)"],
                  ["9 seconds", "sleep(9)"],
                  ["10 seconds", "sleep(10)"]];

var preview_header = ["from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView",
                      "from com.android.monkeyrunner.easy import EasyMonkeyDevice",
                      "from com.android.monkeyrunner.easy import By",
                      "from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper",
                      "import sys,os,commands",
                      "from PScript import *",
                      "from time import sleep"];

function preview_tc() {
  $("#testcase_div").show();
  $("#testcase_preview").hide();
  return true;
}

function generate_tc() {
  $("#testcase_div").hide();
  $("#testcase_preview").show();
  return false;
}

function about_tc() {
  //alert("about_tc");
  return false;
}

function add_sleep() {
  var sleep_time;
  sleep_time = $("#sleep_bar>input").attr("value");
  add_step("sleep", sleep_time);
  return false;
}

function del_step(index) {
  var teststep_count = window.localStorage.getItem("teststep_count");
  var next_content;
  for(var i = index + 1; i <= teststep_count; i++) {
    next_content = window.localStorage.getItem(i);
    window.localStorage.setItem(index, next_content);
  }
  window.localStorage.removeItem(teststep_count);
  teststep_count--;
  window.localStorage.setItem("teststep_count", teststep_count);
  testcase_init("reset");
  return false;
}

function add_step(type, index) {
  var content = "";
  var preview = "";
  var testcase_entry = "";
  var testcase_value = "";
  var pFunction;
  var verb;
  teststep_count = window.localStorage.getItem("teststep_count");
  if(teststep_count == null) {
    teststep_count = 0;
  }
  teststep_count++;
  if (type == "key_list") {
    pFunction = key_list;
    testcase_value = "key_list-" + index;
    verb = "Press";
  } else if (type == "ap_list") {
    pFunction = ap_list;
    testcase_value = "ap_list-" + index;
    verb = "Launch";
  } else if (type == "swipe_list") {
    pFunction = swipe_list;
    testcase_value = "swipe_list-" + index;
    verb = "Swipe from";
  } else if (type == "device_list") {
    pFunction = device_list;
    testcase_value = "device_list-" + index;
    verb = "Do";
  } else if (type == "sleep") {
    pFunction = sleep_list;
    testcase_value = "sleep_list-" + index;
    verb = "Sleep";
  }
  content += "<div data-role=\"collapsible\" data-collapsed-icon=\"arrow-r\" data-expanded-icon=\"arrow-d\" data-theme=\"a\" data-content-theme=\"c\" class=\"testcase_class\">";
  content += "  <h3><span style=\"color:yellow\">Step " + teststep_count + "</span> " + verb + " " + pFunction[index][0] + "</h3>";
  content += "  <pre>";
  preview += "#Step " + teststep_count + " " + verb + " " + pFunction[index][0] + '\n';
  preview += "print '" + verb + " " + pFunction[index][0] + "'" + '\n';
  for(var i = 1; i < pFunction[index].length; i++) {
    content += pFunction[index][i];
    preview += pFunction[index][i];
    preview += '\n';
  }
  content += "  </pre>";
  preview += '\n';
  content += "<a href=\"#\" data-role=\"button\" data-inline=\"true\" data-mini=\"true\" data-theme=\"a\" onclick=\"del_step(" + teststep_count + ")\">Delete this step</a></div>";
  content += "</div>";

  $("#testcase_div").append(content).trigger("create");
  $("#testcase_preview").append(preview).trigger("create");
  testcase_entry = teststep_count.toString();
  window.localStorage.setItem("teststep_count", teststep_count);
  window.localStorage.setItem(testcase_entry, testcase_value);
}

function testcase_init(type) {
  var teststep_count = window.localStorage.getItem("teststep_count");
  var content;
  var preview = "";
  var teststep_id;
  var teststep_value;
  var index;
  var pFunction;
  var verb;

  if(type == "reset") {
    $("#testcase_div").html("").trigger("create");
    $("#testcase_preview").html("").trigger("create").hide();
  }

  for(var i = 0; i < preview_header.length; i++) {
    preview += preview_header[i];
    preview += '\n';
  }
  preview += '\n';
  $("#testcase_preview").append(preview);

  if(teststep_count != null) {
    for(var j = 1; j <= teststep_count; j++) {
      content = "";
      preview = "";
      teststep_id = window.localStorage.getItem(j);
      if(teststep_id != null) {
        teststep_value = teststep_id.split("-");
      } else {
        continue;
      }
      index = teststep_value[1];
      step = j;

      if (teststep_value[0]  == "key_list") {
        pFunction = key_list;
        verb = "Press";
      } else if (teststep_value[0] == "ap_list") {
        pFunction = ap_list;
        verb = "Launch";
      } else if (teststep_value[0] == "swipe_list") {
        pFunction = swipe_list;
        verb = "Swipe from";
      } else if (teststep_value[0] == "device_list") {
        pFunction = device_list;
        verb = "Do";
      } else if (teststep_value[0] == "sleep_list") {
        pFunction = sleep_list;
        verb = "Sleep";
      }

      content += "<div data-role=\"collapsible\" data-collapsed-icon=\"arrow-r\" data-expanded-icon=\"arrow-d\" data-theme=\"a\" data-content-theme=\"c\" class=\"testcase_class\">";
      content += "  <h3><span style=\"color:yellow\">Step " + step.toString() + "</span> " + verb + " " + pFunction[index][0] + "</h3>";
      content += "  <pre>";
      preview += "#Step " + step.toString() + " " + verb + " " + pFunction[index][0] + '\n';
      preview += "print '" + verb + " " + pFunction[index][0] + "'" + '\n';
      for(var i = 1; i < pFunction[index].length; i++) {
        content += pFunction[index][i];
        preview += pFunction[index][i];
        preview += '\n';
      }
      content += "  </pre>";
      preview += '\n';
      content += "<a href=\"#\" data-role=\"button\" data-inline=\"true\" data-mini=\"true\" data-theme=\"a\" onclick=\"del_step(" + step + ")\">Delete this step</a></div>";
      $("#testcase_div").append(content).trigger("create");
      $("#testcase_preview").append(preview).trigger("create").hide();
    }
  }
}

function component_init(component, theme, row_count, div_id) {
  var content = "";
  var div_left;
  var add_step;
  var list;
  switch(row_count) {
    case 2:
      div_left = "<div class=\"ui-grid-a\">";
    break;
    case 3:
      div_left = "<div class=\"ui-grid-b\">";
    break;
    default:
      div_left = "<div class=\"ui-grid-a\">";
    break;
  }

  if(component == "key_list") {
    add_step = 'key_list';
    list = key_list;
  } else if (component == "ap_list") {
    add_step = 'ap_list';
    list = ap_list;
  } else if (component == "swipe_list") {
    add_step = 'swipe_list';
    list = swipe_list;
  } else {
    add_step = 'device_list';
    list = device_list;
  }

  for(var i = 0; i < list.length; i = i + row_count) {
    content += div_left;
    if (list[i] != null) {
      content += "  <div class=\"ui-block-a\">";
      content += "    <button type=\"button\" data-mini=\"true\" data-theme=\"" + theme + "\" onClick=\"add_step('" + add_step + "', " + i + ")\">" + list[i][0] + "</button>";
      content += "  </div>";
    }
    if (list[i+1] != null) {
      content += "  <div class=\"ui-block-b\">";
      content += "    <button type=\"button\" data-mini=\"true\" data-theme=\"" + theme + "\" onClick=\"add_step('" + add_step + "', " + (i+1) + ")\">" + list[i+1][0] + "</button>";
      content += "  </div>";
    }
    if(row_count > 2 && list[i+2] != null) {
      content += "  <div class=\"ui-block-c\">";
      content += "    <button type=\"button\" data-mini=\"true\" data-theme=\"" + theme + "\" onClick=\"add_step('" + add_step + "', " + (i+2) + ")\">" + list[i+2][0]+ "</button>";
      content += "  </div>";
    }
    content += "</div>";
  }
  $("#"+div_id).append(content);
}

function generator_init() {
  component_init("key_list", "a" , 3, "press_key_div");
  component_init("ap_list", "b" , 3, "launch_ap_div");
  component_init("swipe_list", "e", 2, "swipe_div");
  component_init("device_list", "a", 3, "device_div");
  testcase_init("init");
  preview_tc();
  $("button").button();
}
