var jenkins_ip = ["10.113.70.203"];
var test_stations_ip = [];

function update_test_station_ip() {
    var str = document.location.href;
    var server_ip, x;
    if (str.indexOf("http://") == 0) {
        x = str.indexOf(":", 7);
        if ( x == -1 ) {
            x = str.length;
        }
        server_ip = str.substring(7, x);
    } else if (str.indexOf("https://") == 0) {
        x = str.indexOf(":", 8);
        if ( x == -1 ) {
            x = str.length;
        }
        server_ip = str.substring(8, x);
    }
    test_stations_ip.push(server_ip);
}

function update_modal(jenkins_ip, token, type) {
    //type = job or node
    var toG = 1024*1024*1024;
    if(type == 'job') {
        $.getJSON("http://"+jenkins_ip+":8080/job/"+token+"/api/json?jsonp=?", function(data) {
            $("#myModalLabel").html(data.displayName);
            var content="<p>";
            if(data.color=="blue_anime") {
                $.getJSON("http://"+jenkins_ip+":8080/job/"+token+"/"+data.lastBuild.number+"/api/json?jsonp=?", function(building) {
                    content+="<span class=\"label label-important\">Building images</span> ";
                    content+=" started from "+building.id+" by "+building.actions[1].causes[0].userName+"<hr/>";
                    if(data.healthReport.length != 0){
                        content+="<span class=\"label label-warning\">Health status</span> ";
                        content+=data.healthReport[0].description;
                    }
                    if(data.lastStableBuild != null) {
                        content+="<br/><hr/><span class=\"label label-success\">Last stable build</span> <a href='";
                        content+=data.lastStableBuild.url;
                        content+="'>";
                        content+=data.lastStableBuild.url;
                        content+="</a><a href='";
                        content+=data.lastStableBuild.url;
                        content+="consoleText'> <span class=\"label label-inverse\">Log</span></a></td></tr>";
                    }
                    content+="<br/><hr/><span class=\"label\">Old builds</span><br/><table class=\"table table-condensed\">";
                    for(var i = 1; i < data.builds.length; i++) {
                        content+="<tr><td><a href='";
                        content+=data.builds[i].url;
                        content+="'>";
                        content+=data.builds[i].url;
                        content+="</a><a href='";
                        content+=data.builds[i].url;
                        content+="consoleText'> <span class=\"label label-inverse\">Log</span></a></td></tr>";
                    }
                    content+="</table>";
                    content+="</p>";
                    $("#myModalBody").html(content);
                    $('#myModal').modal('show');
                });
            } else {
                if(data.healthReport.length != 0){
                    content+="<span class=\"label label-warning\">Health status</span> ";
                    content+=data.healthReport[0].description;
                }
                if(data.lastStableBuild != null) {
                    content+="<br/><hr/><span class=\"label label-success\">Last stable build</span> <a href='";
                    content+=data.lastStableBuild.url;
                    content+="'>";
                    content+=data.lastStableBuild.url;
                    content+="</a><a href='";
                    content+=data.lastStableBuild.url;
                    content+="consoleText'> <span class=\"label label-inverse\">Log</span></a></td></tr>";
                }
                content+="<br/><hr/><span class=\"label\">Old builds</span><br/><table class=\"table table-condensed\">";
                for(var i = 1; i < data.builds.length; i++) {
                    content+="<tr><td><a href='";
                    content+=data.builds[i].url;
                    content+="'>";
                    content+=data.builds[i].url;
                    content+="</a><a href='";
                    content+=data.builds[i].url;
                    content+="consoleText'> <span class=\"label label-inverse\">Log</span></a></td></tr>";
                }
                content+="</table>";
                content+="</p>";
                $("#myModalBody").html(content);
                $('#myModal').modal('show');
            }
        });
    } else if(type == 'node') {
        if(token == "master") {
            token = "(master)";
        }
        $.getJSON("http://"+jenkins_ip+":8080/computer/"+token+"/api/json?jsonp=?", function(data) {
            $("#myModalLabel").html(data.displayName);
            content="<table class=\"table table-condensed\">";
            if(data.monitorData['hudson.node_monitors.SwapSpaceMonitor'] != null) {
                content+="<tr><th><span class=\"label\">Memory</span></th><th>Available physical memory</th><td>"+(data.monitorData['hudson.node_monitors.SwapSpaceMonitor'].availablePhysicalMemory/toG).toFixed(2)+" GB</td></tr>";
                content+="<tr><th><span class=\"label\">Memory</span></th><th>Total physical memory</th><td>"+(data.monitorData['hudson.node_monitors.SwapSpaceMonitor'].totalPhysicalMemory/toG).toFixed(2)+" GB</td></tr>";
                content+="<tr><th><span class=\"label\">Memory</span></th><th>Available swap space</th><td>"+(data.monitorData['hudson.node_monitors.SwapSpaceMonitor'].availableSwapSpace/toG).toFixed(2)+" GB</td></tr>";
                content+="<tr><th><span class=\"label\">Memory</span></th><th>Total swap space</th><td>"+(data.monitorData['hudson.node_monitors.SwapSpaceMonitor'].totalSwapSpace/toG).toFixed(2)+" GB</td></tr>";
                content+="<tr><th><span class=\"label\">Disk</span></th><th>Temporary space</th><td>"+(data.monitorData['hudson.node_monitors.TemporarySpaceMonitor'].size/toG).toFixed(2)+" GB</td></tr>";
                content+="<tr><th><span class=\"label\">Disk</span></th><th>Free disk space</th><td>"+(data.monitorData['hudson.node_monitors.DiskSpaceMonitor'].size/toG).toFixed(2)+" GB</td></tr>";
            }
            content+="</table>";
            $("#myModalBody").html(content);
            $('#myModal').modal('show');
        });
    }
}

function format_time(input_mseconds) {
    var mseconds = input_mseconds % 1000;
    var round_seconds = (input_mseconds - mseconds) / 1000;
    var seconds = round_seconds % 60;
    var round_minutes = (round_seconds - seconds ) / 60;
    var minutes = round_minutes % 60;
    var hours = (round_minutes - minutes) / 60;
    return hours + " hours " + minutes + " minutes " + seconds + " seconds"
}

function cal_time(jobs) {
    var input_mseconds = -1;
    if (jobs.lastSuccessfulBuild != null) {
        input_mseconds = jobs.lastSuccessfulBuild.estimatedDuration;
    }
    if (input_mseconds == -1) {
        return "N/A";
    }
    return format_time(input_mseconds);
}

function cal_percentage(jobs) {
    if(jobs.lastSuccessfulBuild == null) {
        return "N/A";
    }
    var input_mseconds = jobs.lastSuccessfulBuild.estimatedDuration;
    var utc_start_time;
    var utc_finish_time;
    var utc_current_time;

    var mseconds = input_mseconds % 1000;
    var round_seconds = (input_mseconds - mseconds) / 1000;
    var seconds = round_seconds % 60;
    var round_minutes = (round_seconds - seconds ) / 60;
    var minutes = round_minutes % 60;
    var hours = (round_minutes - minutes) / 60;
    var build_datetime = jobs.lastBuild.id.split("_");
    var build_date = build_datetime[0].split("-");
    var build_time = build_datetime[1].split("-");
    var carry_hour = 0;
    var carry_minute = 0;
    var carry_day = 0;
    var build_time_sec = parseInt(build_time[2], 10) + seconds;
    var percent = 0;
    utc_start_time = Date.UTC(build_date[0], build_date[1], build_date[2], build_time[0], build_time[1], build_time[2]);
    utc_finish_time = utc_start_time + input_mseconds;
    utc_current_time = new Date().getTime();
    if(utc_current_time >= utc_finish_time) {
        percent = 99.9;
    } else {
        percent = ((utc_current_time - utc_finish_time) / input_mseconds).toFixed(1);
    }
    var time ="";
    if(build_time_sec > 59) {
        carry_minute = 1;
        build_time_sec = build_time_sec - 60;
    }
    var build_time_min = parseInt(build_time[1], 10) + minutes + carry_minute;
    if(build_time_min > 59) {
        carry_hour = 1;
        build_time_min = build_time_min - 60;
    }
    var build_time_hour = parseInt(build_time[0], 10) + hours + carry_hour;
    if(build_time_hour > 24) {
        build_time_hour = build_time_hour - 24;
    }

    if (jobs.lastSuccessfulBuild != null) {
        if(build_time_hour<10) {
            time+="0";
        }
        time+=build_time_hour + ":";
        if(build_time_min<10) {
            time+="0";
        }
        time+=build_time_min + ":";
        if(build_time_sec<10) {
            time+="0";
        }
        time+=build_time_sec;
        if((percent >= 0) && (percent <= 100)) {
            return "Estimate finish time : " + time + " (" + percent + "%)";
        } else if ( percent < 0 ) {
            return "Estimate finish time : " + time + " (" + 0.1 + "%)";
        } else if ( percent > 100 ) {
            return "Estimate finish time : " + time + " (" + 99.9 + "%)";
        }

    } else {
        return "N/A";
    }
}

function query_jenkins(jenkins_ip, update_id) {
    $.getJSON("http://"+jenkins_ip+":8080/api/json?depth=2&jsonp=?", function(data) {
        var content="";
        for(var i = 0; i < data.jobs.length; i = i+2) {
            content+="<div class=\"row-fluid\">";
            content+="<div class=\"span6\">";
            content+="<h2>"+data.jobs[i].displayName+"</h2>";
            content+="<h4>"+data.jobs[i].description+" from "+jenkins_ip+"</h4>";
            if(data.jobs[i].color=="blue_anime") {
                content+="<span class=\"label label-important\">Building images, started from " + data.jobs[i].lastBuild.id + "<br />" + cal_percentage(data.jobs[i])+ "</span><br/>";
            }
            content+="<span class=\"label label-info\">Last time uses : " + cal_time(data.jobs[i])+ "</span><br/><br/>";
            content+="<p><a class=\"btn\" href=\"#\" role=\"button\" data-toggle=\"modal\" onClick=\"update_modal(\'"+jenkins_ip+"\',\'"+data.jobs[i].name+"\', 'job');\">View details &raquo;</a></p>";
            content+="</div>";
            if(i < (data.jobs.length - 1)) {
                content+="<div class=\"span6\">";
                content+="<h2>"+data.jobs[i+1].displayName+"</h2>";
                content+="<h4>"+data.jobs[i+1].description+" from "+jenkins_ip+"</h4>";
                if(data.jobs[i+1].color=="blue_anime") {
                    content+="<span class=\"label label-important\">Building images, started from " + data.jobs[i+1].lastBuild.id + "<br />" + cal_percentage(data.jobs[i+1])+ "</span><br/>";
                }
                content+="<span class=\"label label-info\">Last time uses : " + cal_time(data.jobs[i+1])+ "</span><br/><br/>";
                content+="<p><a class=\"btn\" href=\"#\" role=\"button\" data-toggle=\"modal\" onClick=\"update_modal(\'"+jenkins_ip+"\',\'"+data.jobs[i+1].name+"\', 'job');\">View details &raquo;</a></p>";
                content+="</div>";
            }
            content+="</div>";
        }
        content+="<hr/>";
        $(update_id).append(content);
    });
}

function query_teststation(station_ip, update_id) {
    $.getJSON("http://"+station_ip+":4567/test_station.json?jsonp=?", function(data) {
        var content="";
        for(var i = 0; i < data.length; i = i+2) {
            content+="<div class=\"row-fluid\">";
            content+="<div class=\"span6\">";
            content+="<h2>"+data[i].device_name+" ("+data[i].device_id+")</h2>";
            content+="<h4>connected to "+station_ip+" is "+data[i].device_status.state+" "+data[i].device_status.data+"</h4>";
            content+="</div>";
            if(i < (data.length - 1)) {
                content+="<div class=\"span6\">";
                content+="<h2>"+data[i+1].device_name+" ("+data[i+1].device_id+")</h2>";
                content+="<h4>connected to "+station_ip+" is "+data[i+1].device_status.state+" "+data[i+1].device_status.data+"</h4>";
                content+="</div>";
            }
            content+="</div>";
        }
        content+="<hr/>";
        $(update_id).append(content);
    });
}

function query_buildmachine(jenkins_ip, update_id) {
    $.getJSON("http://"+jenkins_ip+":8080/computer/api/json?jsonp=?", function(data) {
        var content="";
        for(var i = 0; i < data.computer.length; i=i+2) {
            content+="<div class=\"row-fluid\">";
            content+="<div class=\"span6\">";
            content+="<h2>"+data.computer[i].displayName+"</h2>";
            content+="<h4> is a node of "+jenkins_ip+"</h4>";
            content+="<p><a class=\"btn\" href=\"#\" role=\"button\" data-toggle=\"modal\" onClick=\"update_modal(\'"+jenkins_ip+"\',\'"+data.computer[i].displayName+"\', 'node');\">View details &raquo;</a></p>";
            content+="</div>";
            if(i < (data.computer.length - 1)) {
                content+="<div class=\"span6\">";
                content+="<h2>"+data.computer[i+1].displayName+"</h2>";
                content+="<h4> is a node of "+jenkins_ip+"</h4>";
                content+="<p><a class=\"btn\" href=\"#\" role=\"button\" data-toggle=\"modal\" onClick=\"update_modal(\'"+jenkins_ip+"\',\'"+data.computer[i+1].displayName+"\', 'node');\">View details &raquo;</a></p>";
                content+="</div>";
            }
            content+="</div>";
        }
        content+="<hr/>";
        $(update_id).append(content);
    });
}

function update_nav_nodes(jenkins_ip, nav_id) {
    var content="";
    $.getJSON("http://"+jenkins_ip+":8080/computer/api/json?jsonp=?", function(data) {
        for(var j = 0; j< data.computer.length; j++){
            $(nav_id).append("<li class=\"active\"><a href=\"#node_list\">"+data.computer[j].displayName+" (on "+jenkins_ip+")</a></li>");
        }
    });
}

function update_navbar(jenkins_ip, nav_id) {
    var content="";
    content="<li class=\"nav-header\">Jenkins</li>";
    for(var i = 0; i < jenkins_ip.length; i++) {
        content+="<li class=\"active\"><a href=\"#jenkins_list\">"+jenkins_ip[i]+"</a></li>";
    }
    $(nav_id).append(content);

    content="<li class=\"nav-header\">Test Station</li>";
    for(var i = 0; i < test_stations_ip.length; i++) {
        content+="<li class=\"active\"><a href=\"#station_list\">"+test_stations_ip[i]+"</a></li>";
    }
    $(nav_id).append(content);

    $(nav_id).append("<li class=\"nav-header\">Build Machine</li>");
    for(var i = 0; i < jenkins_ip.length; i++) {
        setTimeout("update_nav_nodes('"+jenkins_ip[i]+"', '"+nav_id+"');", 500);
    }
}

function station_loader() {
    $("#playground>#station_list").append("<button class=\"btn btn-block btn-inverse\" type=\"button\">Test Station</button>");
    for(var i = 0; i < test_stations_ip.length; i++) {
        query_teststation(test_stations_ip[i], "#playground>#station_list");
    }
}

function node_loader() {
    $("#playground>#node_list").append("<button class=\"btn btn-block btn-inverse\" type=\"button\">Build Machine</button>");
    for(var i = 0; i < jenkins_ip.length; i++) {
        query_buildmachine(jenkins_ip[i], "#playground>#node_list");
    }
    setTimeout("station_loader();", 500);
}

function jenkins_loader() {
    $("#playground>#jenkins_list").append("<button class=\"btn btn-block btn-inverse\" type=\"button\">Jenkins</button>");
    for(var i = 0; i < jenkins_ip.length; i++) {
        query_jenkins(jenkins_ip[i], "#playground>#jenkins_list");
    }
    setTimeout("node_loader();", 500);
}

function console_init() {
    update_test_station_ip();
    update_navbar(jenkins_ip, "#navbar");
    jenkins_loader();
}
