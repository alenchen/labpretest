from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep


def Screen_unlock():
	print 'wake up device'
	device.wake()
	sleep(1.5)

	device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
	sleep(1.5)
	print 'wake up device'
	device.wake()
	sleep(1.5)

	print 'screen unlock'

	lock_start = (355, 890)
	lock_end = (620, 890)
	device.drag(lock_start,lock_end)


print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

#print 'wake up device'
#device.wake()
#sleep(2)
#
#if LockScreen.isLock(device):
#  print 'unlock screen'
#  LockScreen.unLock(device)
#  LockScreen.waitUnLock(device)  

print 'unlock screen'
Screen_unlock()
sleep(2)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'click Settings'
device.startActivity(component='com.android.settings/.Settings')
sleep(3)

print 'click Sound'
cci_device.clickText('Sound')
sleep(5)

print 'click Volumes'
cci_device.clickText('Volumes')
sleep(5)

print 'Select "Music" bar Adjust Ringtone volume to Max'
device.drag((227,452),(620,452),0.3,10) 
sleep(5)

Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
sleep(3)

print 'click OK'
device.touch(355, 988, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'click Music'
device.startActivity(component='com.android.music/com.android.music.MusicBrowserActivity')
sleep(2)

print 'press Songs'
device.touch(468, 106, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press a music file to play'
device.touch(378, 260, MonkeyDevice.DOWN_AND_UP);
sleep(10)

print 'press pause'
device.press('KEYCODE_MEDIA_PAUSE', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print "==============\nAdjust media volume to Max:\n=============="

print 'click Settings'
device.startActivity(component='com.android.settings/.Settings')
sleep(3)

print 'click Sound'
cci_device.clickText('Sound')
sleep(5)

print 'click Volumes'
cci_device.clickText('Volumes')
sleep(5)

print 'Select "Music" bar Adjust Ringtone volume to Middle'
device.drag((620,452),(421,452),0.3,10)  
sleep(5)

Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
sleep(3)

print 'click OK'
device.touch(355, 988, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'click Music'
device.startActivity(component='com.android.music/com.android.music.MusicBrowserActivity')
sleep(2)

print 'press Songs'
device.touch(468, 106, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press a music file to play'
device.touch(378, 260, MonkeyDevice.DOWN_AND_UP);
sleep(10)

print 'press pause'
device.press('KEYCODE_MEDIA_PAUSE', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print "==============\nAdjust media volume to Middle:\n=============="

print 'click Settings'
device.startActivity(component='com.android.settings/.Settings')
sleep(3)

print 'click Sound'
cci_device.clickText('Sound')
sleep(5)

print 'click Volumes'
cci_device.clickText('Volumes')
sleep(5)

print 'Select "Music" bar Adjust Ringtone volume to Min'
device.drag((620,452),(227,452),0.3,10)
sleep(5)

Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
sleep(3)

print 'click OK'
device.touch(355, 988, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'click Music'
device.startActivity(component='com.android.music/com.android.music.MusicBrowserActivity')
sleep(2)

print 'press Songs'
device.touch(468, 106, MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press a music file to play'
device.touch(378, 260, MonkeyDevice.DOWN_AND_UP);
sleep(10)

print 'press pause'
device.press('KEYCODE_MEDIA_PAUSE', MonkeyDevice.DOWN_AND_UP)
sleep(2)


for x in range(1, 4):
  print 'press back key'
  device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
  sleep(1)

print "==============\nAdjust media volume to Min:\n=============="
Common.takeSnapshot(device, filename=sys.argv[3]+'-4', path=sys.argv[1]+'/'+sys.argv[2])

print 'Long Press "Power" key'
device.press('KEYCODE_POWER', MonkeyDevice.DOWN)
sleep(5)

Common.takeSnapshot(device, filename=sys.argv[3]+'-5', path=sys.argv[1]+'/'+sys.argv[2])

print 'click "Silent mod"'
device.touch(619, 755, MonkeyDevice.DOWN_AND_UP);
sleep(2)

Common.updateResult('Manual');
print 'Manual\n\n'

os.system("adb shell am force-stop com.android.music")  
