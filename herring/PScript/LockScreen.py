from com.android.monkeyrunner import MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
import filecmp,os,sys
from time import sleep

def isLock(device):
  """ boolean isLock ( MonkeyDevice device ) 
      capture picture, compare if the picture is the same with lock icon.

      Wanrring: the fucntion doesn't work proper when wallpaper is changed.
  """
  if isinstance(device, EasyMonkeyDevice):
    return device.visible(By.id("id/lock_screen"))
  else:
    rect = (338, 954, 382 - 338, 985 - 954) # x, y ,w ,h
    path = os.path.dirname(os.path.abspath(__file__))
    image = device.takeSnapshot()
    subimage = image.getSubImage( rect )
    subimage.writeToFile('.tmp','png')
    res = False
    for x in range(1, 6):
      res = filecmp.cmp('.tmp', os.path.join(path, "img", "lock_0" + str(x) + '.png'))
      if res :
        break
    os.remove(".tmp")
    return res

def unLock(device, duration=0.5, steps=10):
  """ void unLock (MonkeyDevice device ) 
      drag unlock icon to unlock lock screen, waste 0.5 sec to unlock.
  """
  start = (361, 966)
  end = (629, 966)
  device.drag(start, end)

def waitUnLock(device, maxWaitTime=5):
  i = 0
  while isLock(device):
    if i > maxWaitTime:
      sys.stderr.write('warning: time is over when try to unlock screen\n')
      return False
    i += 1
    sleep(1)
  return True

