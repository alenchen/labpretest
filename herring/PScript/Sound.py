from com.android.monkeyrunner import MonkeyDevice, MonkeyImage
import filecmp
import os
import sys
from time import sleep

def isAutoChecked(device):
  """ boolean isAutoChecked ( MonkeyDevice device )
  """
  image = device.takeSnapshot()
  rect = (44, 819, 600, 70) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA85/PScript/img/DTMF_checked.png');
  os.remove(".tmp")
  return res

def isAutoUnChecked(device):
  """ boolean isAutoChecked ( MonkeyDevice device )
  """
  image = device.takeSnapshot()
  rect = (44, 819, 600, 70) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA85/PScript/img/DTMF_unchecked.png');
  os.remove(".tmp")
  return res

