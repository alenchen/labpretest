import os

__all__ = []

my_dir = os.path.dirname(__file__)
for py in os.listdir(my_dir):
  if py == '__init__.py':
    continue

  if py.endswith('.py'):
    name = py[:-3]
    __all__.append(name)
