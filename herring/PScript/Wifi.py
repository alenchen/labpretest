from com.android.monkeyrunner import MonkeyDevice, MonkeyImage
import re
import sys
from time import sleep

def isWifiOn(device):
  """ boolean isWifiOn ( MonkeyDevice device )
  """
  string = device.shell('netcfg');
  p = re.compile('wlan0*')
  m = p.search(string)
  if m :
    return True
  else :
    return False

def waitWifiOn(device, maxWaitTime=30):
  """ boolean waitWifiOn ( MonkeyDevice device, int maxWaitTime = 30)
      block function.
  """
  i = 0
  while not isWifiOn(device):
    if i > maxWaitTime:
      sys.stderr.write('warning: time is over when waitting Wifi on\n')
      return False
    i += 1
    sleep(1)
  return True

