from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from time import sleep
import Common, os, filecmp
class getInstance(Common.getInstance):
  def __init__ (self, common=None):
    """ void __init__ (Common.getInstance common) 
    """
    if common:
      self.device = common.device
      self.ccidevice = common.ccidevice
      self.deviceId = common.deviceId
    else:
      Common.getInstance.__init__(self)

  def __del__ (self):
    pass
  
  def input (self, text, wait=1, typewait=0.5):
    for i in range(0, len(text)):
      self.keyevent(text[i])
      sleep(typewait)
    sleep(wait)

  def keyevent(self, code):
    keycode = {
      "a": 29,
      "b": 30,
      "c": 31,
      "d": 32, 
      "e": 33,
      "f": 34,
      "g": 35,
      "h": 36,
      "i": 37,
      "j": 38,
      "k": 39,
      "l": 40,
      "m": 41,
      "n": 42,
      "o": 43,
      "p": 44,
      "q": 45,
      "r": 46,
      "s": 47,
      "t": 48,
      "u": 49,
      "v": 50,
      "w": 51,
      "x": 52,
      "y": 53,
      "z": 54,
      " ": 62,
      "E": 66,    # enter
      "S": 84    # search
    }
    self.shell("input keyevent " + str(keycode[code]));
  
