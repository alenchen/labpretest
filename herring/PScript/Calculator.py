from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
import Common
class getInstance(Common.getInstance):
  def __init__ (self, common=None):
    """ void __init__ (MonkeyDevice device, CCIEasyMonkeyDevice ccidevice) 
    """
    if common:
      self.device = common.device
      self.ccidevice = common.ccidevice
      self.deviceId = common.deviceId
    else:
      Common.getInstance.__init__(self)

  def __del__ (self):
    pass

  def longClickCLR(self):
    """ void longClickCLR () 
        long click CLR will clear all strings.
    """
    self.debug("longClickCLR()")
    pos = (600, 360)
    self.touch(pos[0], pos[1],wait=2, touchType='down')
    self.touch(pos[0], pos[1], touchType='up')

  def isBasic(self):
    """ void isBasic()
        Notice: this function will clear current expression.
    """
    self.debug("isBasic()")
    #self.longClickCLR()
    #self.inputButton("123456789")
    #res = self.isTextVisible('123456789')
    #self.longClickCLR()
    return self.visible(id="id/simplePad")
    #return res

  def isAdvance(self):
    """ void isAdvance()
        Notice: this function will clear current expression.
    """
    self.debug("isAdvance()")
    return not self.isBasic()

  def menu(self, text):
    self.debug("menu(text=%s)", text)
    #self.touch(675, 177)
    self.touch(id="id/overflow_menu")
    res = self.clickText(text)
    if res:
      self.sleep(1)
    return res

  def goBasic(self):
    """ void goBasic() 
    .getInstance"""
    self.debug("goBasic()")
    self.scrollLeft(wait=2)

  def goAdvance(self):
    """ void goAdvance() 
    """
    self.debug("goAdvance()");
    self.scrollRight(wait=2)

  def touchButton(self, symbol):
    """ void touchButton (String symbol)
     
    === Basic panel ===
                CLR
        7  8  9  /
        4  5  6  *
        1  2  3  -
        .  0  =  +

    === Advanced panel ===
                CLR
        sin cos tan
        ln  log  !
        pi   e   ^
        (    )  sqr
    """
    self.debug("touchButton(symbol=%s)", symbol)
    time=0.5
    pos = ()
    isAdvance = False
    if symbol == "0" :
      pos = (252, 1100)
    elif symbol == "1" :
      pos = (82, 900)
    elif symbol == "2" :
      pos = (252, 900)
    elif symbol == "3" :
      pos = (444, 900)
    elif symbol == "4" :
      pos = (82, 700)
    elif symbol == "5" :
      pos = (252, 700)
    elif symbol == "6" :
      pos = (444, 700)
    elif symbol == "7" :
      pos = (82, 500)
    elif symbol == "8" :
      pos = (252, 500)
    elif symbol == "9" :
      pos = (444, 500)
    elif symbol == "." :
      pos = (82, 1100)
    elif symbol == "=" :
      pos = (444, 1100)
    elif symbol == "/" :
      pos = (600, 500)
    elif symbol == "*" :
      pos = (600, 700)
    elif symbol == "-" :
      pos = (600, 900)
    elif symbol == "+" :
      pos = (600, 1100)
    elif symbol == "CLR" :
      pos = (600, 360)
    else:
      isAdvance = True
      time = 1
      if symbol == "sin(":
        pos = (125, 500)
      elif symbol == "cos(":
        pos = (360, 500)
      elif symbol == "tan(":
        pos = (600, 500)
      elif symbol == "ln(":
        pos = (125, 700)
      elif symbol == "log(":
        pos = (360, 700)
      elif symbol == "!":
        pos = (600, 700)
      elif symbol == "pi":
        pos = (125, 900)
      elif symbol == "e":
        pos = (360, 900)
      elif symbol == "^":
        pos = (600, 900)
      elif symbol == "(":
        pos = (125, 1100)
      elif symbol == ")":
        pos = (360, 1100)
      elif symbol == "sqr":
        pos = (600, 1100)
      else:
        #print "Unknow input: " + symbol
        return
    if isAdvance:
      self.goAdvance()
  
    self.touch(pos[0], pos[1], wait=time)
  
  def inputButton(self, expr):
    """ void input (String expression) 
    """
    self.debug("inputButton(expr=%s)", expr)
    lengh = 0
    i = 0
    while i < len(expr):
      if i+4 <= len(expr) and (expr[i:i+4] == "sin(" or expr[i:i+4] == "cos(" or expr[i:i+4] == "tan(" or expr[i:i+4] == "log("):
        lengh=4
      elif i+3 <= len(expr) and (expr[i:i+3] == "ln(" or expr[i:i+3] == "sqr" or expr[i:i+3] == "CLR") :
        lengh=3
      elif i+2 <= len(expr) and expr[i:i+2] == "pi":
        lengh=2
      else:
        lengh=1
      #print "touch button:" + expr[i:i+lengh]
      self.touchButton(expr[i:i+lengh])
      i += lengh
