from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
import os, filecmp
import Common

class getInstance(Common.getInstance):
  db = "/data/data/com.android.deskclock/databases/alarms.db"
  def __init__ (self, common=None):
    """ void __init__ (MonkeyDevice device, CCIEasyMonkeyDevice ccidevice) 
    """
    if common:
      self.device = common.device
      self.ccidevice = common.ccidevice
      self.deviceId = common.deviceId
    else:
      Common.getInstance.__init__(self)

  def launchAddPage(self):
    self.debug("launchAddPage()")
    """ void launchAddPage () 
        call when in Alarms page
    """
    self.touch(id="id/menu_item_add_alarm")
  
  def done(self):
    self.debug("done()")
    """ void done () 
        call when in add Alarms page
    """
    self.touch(id="id/save_menu_item")

  
  def delete(self):
    self.debug("delete()")
    """ void del () 
        call when in add Alarms page
    """
    self.touch(id="id/menu_delete")

  def touchTopitem(self):
    self.debug("touchTopitem()")
    self.touch(350, 200)
  
  def touchOK(self):
    self.debug("touchOK()")
    self.touch(500, 750)


  def lauchAlarmsPage(self):
    self.debug("lauchAlarmsPage()")
    self.touch(id="id/nextAlarm")

  def count(self):
    self.debug("count()")
    """ int count ()
        get the number of alarms.
    """
    return int(self.query(self.db, "count(*)", "alarms"))

  """def query(self, select, fmt, *arg):
     string query( String select, string fmt, ...) 

        sample:
          query ( "count(*)", "where enabled = %d and hour=%d", 0, 9)

        return string:
          1|8|30|31|0|0|1||
          2|9|0|96|0|0|1||

        database: 
          alarms
            Name         Type
            _id          INTEGER PRIMARY KEY
            hour         INTEGER
            minutes      INTEGER
            daysofweek   INTEGER
            alarmtime    INTEGER
            enabled      INTEGER
            vibrate      INTEGER
            message      TEXT
            alert        TEXT

            daysofweek value:
              mon :  1
              tue :  2
              wed :  4
              thu :  8
              fri : 16
              sat : 32
              sun : 64
              mon+sun = 1 + 64 = 65
    if None == fmt:
      string = "sqlite3 %s 'SELECT %s from alarms'" % (self.db, select)
    else :
      string = "sqlite3 %s 'SELECT %s from alarms where %s'" % (self.db, select, fmt % arg)
    return self.device.shell(string)"""

  def parser(self, string):
    """ link parser(String string) 
        string = "1|8|30|31|0|0|1||
                  2|9|0|96|0|0|1||"
        output is 
        [
          {'_id':'1', 'hour':'8', 'minutes':'30', 'daysofweek':'31', 'alarmtime':'0', 'enabled':'0', 'vibrate':'1', 'message':'', 'alert':''},
          {'_id':'2', 'hour':'9', 'minutes':'30', 'daysofweek':'96', 'alarmtime':'0', 'enabled':'0', 'vibrate':'1', 'message':'', 'alert':''}
        ]
    """
    self.debug("parser(string=%s)", string)
    array = []
    for line in string.split("\n"):
      items = line.strip().split("|")
      data = {}
      data["_id"]   = items[0]
      data["hour"] = items[1]
      data["minutes"] = items[2]
      data["daysofweek"] = items[3]
      data["alarmtime"] = items[4]
      data["enabled"] = items[5]
      data["vibrate"] = items[6]
      data["message"] = items[7]
      data["alert"] = items[8]
      array.append(data)
    return array

  def compare(self):
    rect=(194, 247, 205 - 194, 256 - 246)
    image = self.device.takeSnapshot()
    subimage = image.getSubImage( rect )
    subimage.writeToFile('.tmp','png')
    path = os.path.dirname(os.path.abspath(__file__))
    res = filecmp.cmp('.tmp', os.path.join(path, 'img', 'AlarmClock_001.png'))
    os.remove(".tmp")
    self.debug("compare() : return %s", res)
    return res
  

