from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from com.android.monkeyrunner.easy import EasyMonkeyDevice, By
from com.cci.monkeyrunnerplugin import CCIEasyMonkeyDevice
from time import sleep
import os,commands,sys,re,logging
import LockScreen

def scrollDown(device, duration=0.3, steps=10):
  """ void scrollDown ( MonkeyDevice device )
  """
  start = (220, 800)
  end = (220, 200)
  device.drag(start, end, duration, steps)


def scrollUp(device, duration=0.3, steps=10):
  """ void scrollUp ( MonkeyDevice device )
  """
  start = (220, 500)
  end = (220, 800)
  device.drag(start, end, duration, steps)

def scrollLeft(device, duration=0.3, steps=10):
  """ void scrollLeft ( MonkeyDevice device )
  """
  start = (50, 500)
  end = (500, 500)
  device.drag(start, end, duration, steps)

def scrollRight(device, duration=0.3, steps=10):
  """ void scrollRight ( MonkeyDevice device )
  """
  start = (500, 500)
  end = (50, 500)
  device.drag(start, end, duration, steps)


def dragSwitchOn(device, x, y, width=118):
  """ void dragSwitchOn (MonkeyDevice device, int x, int y, int width = 118)
  """
  start = (x, y)
  end   = (x+width, y)
  device.drag(start, end)
  sleep(2)

def dragSwitchOff(device, x, y, width=118):
  """ void dragSwitchOff (MonkeyDevice device, int x, int y, int width = 118)
  """
  start = (x+width, y)
  end   = (x, y)
  device.drag(start, end)
  sleep(2)

def takeSnapshot(device, filename=None, path='errors'):
  """ void takeSnapshot ( MonkeyDevice device)
  """
  picfile = ''
  if not os.path.exists('public/'+path):
    os.makedirs('public/'+path)

  if filename :
    picfile = os.path.join ('public', path, filename) + ".png"
    thumb_picfile = os.path.join ('public', path, 'thumb_' + filename) + ".png"
  else :
    for x in range(1, 9999):
      filename = "screenshot%04d" % x
      picfile = os.path.join ('public', path, filename) + ".png"
      thumb_picfile = os.path.join ('public', path, 'thumb_' + filename) + ".png"
      if not os.path.isfile(picfile) :
        break
  image = device.takeSnapshot()
  image.writeToFile(picfile,'png')
  os.system('convert -thumbnail 100 '+picfile+' '+ thumb_picfile)

def getBrightness(device):
    """ NO use, backight is turned on when we use monkeyrunner.
    """
    return device.shell('cat /sys/class/leds/lcd-backlight/brightness')

def updateResult(result):
  """ void updateResult ( string result )
  """
  os.system("echo 'case_result_is=" + result + "' > case_result_is")

class LevelFilter(logging.Filter):
  LEVEL={ 'd'   : logging.DEBUG,
          'i'   : logging.INFO,
          'w'   : logging.WARNING,
          'e'   : logging.ERROR,
          'c'   : logging.CRITICAL,
          'n'   : logging.NOTSET,
        }
  def __init__(self, level, *args, **kwargs):
    self.leveldict = {}
    for i in range(0, len(level)):
      self.leveldict[self.LEVEL[level[i]]] = True

  def filter(self, record):
    return self.leveldict.get(record.levelno, False)

class getInstance:
  isCaseSens = False
  isFullText = False
  isLogOn    = False

  typeDict = {
     'up'  : MonkeyDevice.UP,
     'down' : MonkeyDevice.DOWN,
     'up_and_down': MonkeyDevice.DOWN_AND_UP,
  }

  def init_log(self, TAG=__name__, level='i', levelfilter='i', fmt='%(asctime)s %(name)-12s %(levelname)-8s %(message)s', datefmt='%m-%d %H:%M:%S'):
    LEVEL={ 'd'   : logging.DEBUG,
            'i'   : logging.INFO,
            'w'   : logging.WARNING,
            'e'   : logging.ERROR,
            'c'   : logging.CRITICAL,
            'n'   : logging.NOTSET,
           }
    console = logging.StreamHandler()
    console.setLevel(LEVEL[level])
    # set a format which is simpler for console use
    formatter = logging.Formatter(fmt, datefmt=datefmt)
    # tell the handler to use this format
    console.setFormatter(formatter)

    if levelfilter :
      console.addFilter(LevelFilter(levelfilter))

    # add the handler to the root logger
    self.logger = logging.getLogger(TAG)
    self.logger.addHandler(console)

    self.logger.setLevel(level=LEVEL[level])
    self.isLogOn = True
    self.logger.info('info    : on')
    self.logger.warning('warning : on')
    self.logger.debug('debug   : on')
    self.logger.error('error   : on')
    self.logger.critical('critical: on')

  def info(self, fmt, *arg):
    if self.isLogOn:
      self.logger.info(fmt % arg)
  def warning(self, fmt, *arg):
    if self.isLogOn:
      self.logger.warning(fmt % arg)
  def debug(self, fmt, *arg):
    if self.isLogOn:
      self.logger.debug(fmt % arg)
  def error(self, fmt, *arg):
    if self.isLogOn:
      self.logger.error(fmt % arg)
  def critical(self, fmt, *arg):
    if self.isLogOn:
      self.logger.critical(fmt % arg)

  def __init__ (self):
    #self.deviceId = self.getDeviceId()
    pass

  def __del__ (self):
    pass

  def waitForConnection(self):
    self.debug('waitforConnection()')
    #self.device = MonkeyRunner.waitForConnection(deviceId=self.deviceId)
    self.device = MonkeyRunner.waitForConnection()
    #self.ccidevice = CCIEasyMonkeyDevice(self.deviceId, self.device)
    self.ccidevice = CCIEasyMonkeyDevice(self.device)
    self.easydevice = EasyMonkeyDevice(self.device)

  def bash(self, text):
    res = commands.getoutput(text)
    self.debug("bash(text=%s) : return %s", text, res)
    return res

  def getDeviceId(self):
    self.debug('getDeviceId()')
    devices = []
    p = re.compile('device$')
    for s in self.bash('adb devices').strip().split('\n'):
      if p.search(s):
        devices.append(s)

    if len(devices) == 0:
      MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
      sys.exit(1)
    elif len(devices) == 1:
      choice = 0
    else:
      choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")
    return devices[choice].split('\t')[0]

  def goHome(self, backtimes=3):
    """ void goHome ()
    """
    self.debug('gohome()')
    for i in range(0, backtimes):
      self.press('KEYCODE_BACK')

    self.press('KEYCODE_HOME')

  def launchAppList(self):
    """ void launchAppList()
    """
    self.debug('launchiAppList()')
    self.touch(357, 1106)

  def search_and_getXY(self, text, turnLeft=10, turnRight=10, setIsFullTextMatch=True, isWidget=False):
    """ turple search_and_getXY (String text)
    """
    self.debug('search_and_getXY(text=%s, trunLeft=%d, turnRight=%d, setIsFullTextMatch=%s, isWidget=%s)', text, turnLeft, turnRight, setIsFullTextMatch, isWidget)
    self.scrollLeft(turnLeft)

    if isWidget :
      self.applist_widget()

    i = 0
    res = None
    while True:
      res = self.getTextLocation(text, setIsFullTextMatch=setIsFullTextMatch)
      if res:
        break
      if i >= turnRight:
        break
      i += 1
      self.scrollRight()

    return res

  def scroll_and_Snapshot(self, filename=None, path='error', filetype="png", before="scrollLeft", after="scrollRight", beforeTimes=3, afterTimes=3):
    self.debug("scroll_and_Snapshot(filename=%s, path=%s, filetype=%s, before=%s, after=%s, beforeTimes=%d, afterTimes=%d)", filename, path, filetype, before, after, beforeTimes, afterTimes)
    funcdict = { 'scrollLeft'  : self.scrollLeft,
                 'scrollRight' : self.scrollRight,
                 'scrollUp'    : self.scrollUp,
                 'scrollDown'  : self.scrollDown
    }
    funcdict[before](beforeTimes)
    res = False
    for i in range(0, afterTimes):
      self.takeSnapshot("%s-%d" % (filename, i), path, filetype)
      funcdict[after]()

  def search_and_click(self, text, before="scrollLeft", after="scrollRight", beforeTimes=10, afterTimes=3, setIsFullTextMatch=True, isWidget=False):
    """ boolean search_and_click (String text)
    """
    self.debug("search_and_click(text=%s, before=%s, after=%s, beforeTimes=%d, afterTimes=%d, setIsFullTextMatch=%s, isWidget=%s)", text, before, after, beforeTimes, afterTimes, setIsFullTextMatch, isWidget)
    funcdict = { 'scrollLeft'  : self.scrollLeft,
                 'scrollRight' : self.scrollRight,
                 'scrollUp'    : self.scrollUp,
                 'scrollDown'  : self.scrollDown
    }

    funcdict[before](beforeTimes)

    if isWidget :
      self.applist_widget()

    i = 0
    res = False
    while True:
      if self.clickText(text, setIsFullTextMatch=setIsFullTextMatch):
        sleep(1)
        res=True
        break
      if i >= afterTimes:
        break
      i += 1
      funcdict[after]()

    return res

  def applist_app(self):
    self.debug('applist_app()')
    self.touch(50, 100)

  def applist_widget(self):
    self.debug('applist_widget()')
    self.touch(220, 100)

  def scrollUp(self, times=1, duration=0.3, steps=10, wait=1):
    """ void scrollUp ()
    """
    self.debug('scrollUp(times=%d, duration=%.1f, steps=%d, wait=%.1f)', times, duration, steps, wait)
    for i in range(0, times):
      self.drag((220, 500), (220, 800), duration, steps)

  def scrollDown(self, times=1, duration=0.3, steps=10, wait=1):
    """ void scrollDown ()
    """
    self.debug('scrollDown(times=%d, duration=%.1f, steps=%d, wait=%.1f)', times, duration, steps, wait)
    for i in range(0, times):
      self.drag((220, 800), (220, 200), duration, steps)

  def scrollLeft(self, times=1, duration=0.3, steps=10, wait=1):
    """ void scrollLeft ()
    """
    self.debug('scrollLeft(times=%d, duration=%.1f, steps=%d, wait=%.1f)', times, duration, steps, wait)
    for i in range(0, times):
      self.drag((50, 500), (500, 500), duration, steps)

  def scrollRight(self, times=1, duration=0.3, steps=10, wait=1):
    """ void scrollRight ()
    """
    self.debug('scrollRight(times=%d, duration=%.1f, steps=%d, wait=%.1f)', times, duration, steps, wait)
    for i in range(0, times):
      self.drag((500, 500), (50, 500), duration, steps)

  def updateResult(self, result):
    """ void updateResult ( string result )
    """
    self.debug('updateResult(result=%s)', result)
    os.system("echo 'case_result_is=" + result + "' > case_result_is")

#""" MonkeyRunner method """
  def sleep(self, wait=1):
    self.debug("sleep(wait=%.1f)", wait)
    MonkeyRunner.sleep(wait)

#""" MonkeyDevice method """
  def type(self, text, id=None, wait=1):
    if id:
      self.debug("type(text=%s, id=%s, wait=%.1f)", text, id, wait)
      self.easydevice.type(By.id(id), text)
    else :
      self.debug("type(text=%s, wait=%.1f)", text, wait)
      self.device.type(text)

    sleep(wait)

  def press(self, keyCode, pressType='up_and_down', wait=1):
    self.debug("press(keyCode=%s, pressType=%s, wait=%.1f)", keyCode, pressType, wait)
    self.device.press(keyCode, self.typeDict[pressType])
    sleep(wait)

  def touch(self, x=0, y=0, id=None, touchType='up_and_down', wait=1):
    if id :
      self.debug("touch(id=%s, touchType=%s, wait=%.1f)", id, touchType, wait)
      self.easydevice.touch(By.id(id), self.typeDict[touchType])
    elif isinstance(x, basestring) :
      self.debug("touch(id=%s, touchType=%s, wait=%.1f)", x, touchType, wait)
      self.easydevice.touch(By.id(x), self.typeDict[touchType])
    else :
      self.debug("touch(x=%d, y=%d, touchType=%s, wait=%.1f)", x, y, touchType, wait)
      self.device.touch(x, y, self.typeDict[touchType])
    sleep(wait)

  def drag(self, start, end, duration=0.3, steps=10, wait=1):
    self.device.drag(start, end, duration, steps)
    sleep(wait)

  def takeSnapshot(self, filename=None, path='errors', filetype="png"):
    """ void takeSnapshot ()
    """
    self.debug("takeSnapshot(filename=%s, path=%s, filetype=%s)", filename, path, filetype)
    picfile = ''
    path = os.path.join("public", path)
    if not os.path.exists(path):
      os.makedirs(path)

    if filename :
      picfile = os.path.join (path, filename) + "." + filetype
      thumb_picfile = os.path.join (path, 'thumb_' + filename) + '.' + filetype
    else :
      for x in range(1, 9999):
        filename = "screenshot%04d" % x
        picfile = os.path.join (path, filename) + "." + filetype
        thumb_picfile = os.path.join (path, 'thumb_' + filename) + '.' + filetype
        if not os.path.isfile(picfile) :
          break
    image = self.device.takeSnapshot()
    image.writeToFile(picfile, filetype)
    os.system('convert -thumbnail 100 '+picfile+' '+thumb_picfile)


  def getProperty(self, key):
    self.debug("getProperty(key=%s)", key)
    return self.device.getProperty(key)

  def shell(self, cmds):
    self.debug("shell(cmds=%s)", cmds)
    return self.device.shell(cmds)

  def wake(self, wait=1):
    self.debug("wake(wait=%.1f)", wait)
    self.device.wake()
    sleep(1)

#""" MonkeyImage method """
  def getSubImage(self, rect, image=None):
    self.debug("getSubImage(rect=%s, image=%s)", rect, image)
    if not image:
      image = self.takeSnapshot()
    return image.getSubImage(rect)

#""" EeayMonkeyDevice method """
  def visible(self, id):
    """ boolean visible(String id)
    """
    self.debug("visible(id=%s)", id)
    return self.easydevice.visible(By.id(id))

  def getText(self, id):
    self.debug("getText(id=%s)", id)
    return self.easydevice.getText(By.id(id))

#""" CCIEasyMonkeyDevice method """
  def clickText(self, text, setIsFullTextMatch=None):
    ori_IsFullText = self.getIsFullTextMatch()
    self.setIsFullTextMatch(setIsFullTextMatch)
    res =  self.ccidevice.clickText(text)
    self.setIsFullTextMatch(ori_IsFullText)
    self.debug("clickText(text=%s, setIsFullTextMatch=%s): return %s", text, setIsFullTextMatch, res)
    return res

  def getTextLocation(self, text,  setIsFullTextMatch=None):
    self.debug("getTextLocation(text=%s, setIsFullTextMatch=%s)", text, setIsFullTextMatch)
    ori_IsFullText = self.getIsFullTextMatch()
    self.setIsFullTextMatch(setIsFullTextMatch)

    res = self.ccidevice.getTextLocation(text)
    if res:
      res = res[1:len(res)-1].split(",")
      res = (int(res[0]) , int(res[1]))

    self.setIsFullTextMatch(ori_IsFullText)
    return res

  def isTextVisible(self, text, setIsFullTextMatch=None):
    self.debug("isTextVisible(text=%s, setIsFullTextMatch=%s)", text, setIsFullTextMatch)
    ori_IsFullText = self.getIsFullTextMatch()
    self.setIsFullTextMatch(setIsFullTextMatch)
    res = self.ccidevice.isTextVisible(text)
    self.setIsFullTextMatch(ori_IsFullText)
    return res

  def setIsCaseSensitive(self, isCaseSens):
    self.debug("setIsCaseSensitive(isCaseSens=%s)", isCaseSens)
    if (isCaseSens != None and self.isCaseSens != isCaseSens):
      self.isCaseSens = isCaseSens
      self.ccidevice.setIsCaseSensitive(self.isCaseSens)

  def getIsCaseSensitive(self):
    self.debug("getIsCaseSensitive(): return %s", self.isCaseSens)
    return self.isCaseSens

  def setIsFullTextMatch(self, isFullText):
    self.debug("setIsFullTextMatch(isFulltext=%s)", isFullText)
    if(isFullText != None and self.isFullText != isFullText):
      self.isFullText = isFullText
      self.ccidevice.setIsFullTextMatch(self.isFullText)

  def getIsFullTextMatch(self):
    self.debug("getIsFullTextMatch() : return %s", self.isFullText)
    return self.isFullText

#""" LockScreen """
  def unLockIfScreenLock(self):
    self.debug("unLockIfScreenLock()")
    if self.isLock():
      LockScreen.unLock(self.device)
      self.waitUnLock()
    sleep(1)

  def isLock(self):
    res = self.easydevice.visible(By.id("id/lock_screen"))
    self.debug("isLock() : return %s", res)
    return res

  def waitUnLock(self, maxWaitTime=5):
    self.debug("waitUnLock(maxWaitTime=%d)", maxWaitTime)
    i = 0
    while self.isLock():
      if i > maxWaitTime:
        self.warning('time is over when try to unlock screen')
        return False
      i += 1
      sleep(1)
    return True

#""" Sqlite3 """
  def query(self, db, select, table, fmt=None, *arg):
    """ string query( String select, string fmt, ...)
    """
    self.debug("query(db=%s, select=%s, table=%s, fmt=%s, *arg=%s)", db, select, table, fmt, arg)
    if fmt :
      string = "sqlite3 %s 'SELECT %s from %s where %s'" % (db, select, table, fmt % arg)
    else :
      string = "sqlite3 %s 'SELECT %s from %s'" % (db, select, table)
    return self.shell(string)
