from com.android.monkeyrunner import MonkeyDevice, MonkeyImage
import re
import sys
import filecmp
import os
import string
from time import sleep
msg_inbox = "1"
msg_outbox = "2"
#MMS: start
def screenUnlock(device,lock_start, lock_end):
  device.press('KEYCODE_POWER',MonkeyDevice.DOWN_AND_UP)
  sleep(2)
  device.wake()
  sleep(1)
  print 'input keyevent 82'
  device.shell('input keyevent 82')
  #device.drag(lock_start,lock_end)

def ensureUnlock(device):
  print 'wake up device'
  device.wake()
  sleep(2)
  print 'screen unlock'
  lock_start = (355, 890)
  lock_end = (620, 890)
  screenUnlock(device,lock_start, lock_end)
  sleep(2)

def getMessageId(device, item, msgbox):
  message_id = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT m_id from pdu where (sub=\"'+item+'\" AND msg_box='+msgbox+')\'');
  return message_id

def getPduId(device, item, msgbox):
#  print "getPduId"
  _id = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT _id from pdu where (sub=\"'+item+'\" AND msg_box='+msgbox+')\'');
  return _id

def RecvTextOnlyOK(device):
#  m_id = getMessageId(device, "Item3", msg_outbox)
#  print m_id
#  item = "Item3"
#  sub = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT sub from pdu where (sub=\"'+item+'\" AND #msg_box=1)\'');
#  print sub
  mid = getPduId(device, "Item3", msg_inbox)
  sqlcmd = '\'SELECT text from part where (mid=\"'+mid+'\" AND ct=\"text/plain\")\''
  content = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db '+sqlcmd);
  print content
  p = re.compile("Item3")
  m = p.search(content)
  if m :
    return True
  else :
    return False

def RecvSubOK(device, item):
  sub = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT sub from pdu where (sub=\"'+item+'\" AND msg_box=1)\'');
  print sub
  p = re.compile(item)
  m = p.search(sub)
  if m :
    return True
  else :
    return False

def SentOK(device, item):
  m_id = getMessageId(device, item, msg_outbox)
  if len(m_id) > 0:
     return True
  else:
     return False

def getDataSize(device, mid, mimetype, msgbox):
  path = device.shell("sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT _data from part where (mid="+mid+" AND ct=\""+mimetype+"\")\'");
#  print "getDataSize"
  if len(path) == 0:
    return -1

  stat = device.shell('ls -l ' + path.strip('\r\n'))
#  print stat

  for i, c in enumerate(stat):
     if c.isdigit():
         start = i
         while i < len(stat) and stat[i].isdigit():
             i += 1
         #print 'Integer %d found at position %d' % (int(stat[start:i]), start)
         return int(stat[start:i])
         break

  return -1

def SendAndRecvOK(device, item, mimetype):
  m_id = getMessageId(device, item, msg_inbox)

  if len(m_id) == 0:
    return False 

  sentId = getPduId(device, item, msg_outbox)

  if len(sentId) == 0:
     return False

  sentsize = getDataSize(device, sentId, mimetype, msg_outbox)

  recvId = getPduId(device, item, msg_inbox)

  if len(recvId) == 0:
     return False

  recvsize = getDataSize(device, recvId, mimetype, msg_inbox)

  if sentsize == 0 or recvsize == 0:
     return False

  if sentsize == recvsize and len(m_id) > 0:
     return True
  else:
     return False
#MMS: end
def clearSms(device):
# pull db , after testing push back
  str1 = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT count(*) from sms\'');
  n = string.atoi(str1)
  if n > 0:
#use UI to delete all msg
#trigger submenu
    device.touch(496, 852, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
#Delete all thread
    device.touch(360, 779, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
#Delete
    device.touch(389, 555, MonkeyDevice.DOWN_AND_UP);
    sleep(1)

def isMOSMSOK(device, tele):
  """ boolean is is MO SMS OK ( MonkeyDevice device )
  """
  string = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT address from sms where (body=\"Qw\" AND type=2)\'');
#  print tele
  telen=tele[0:4]+' '+tele[4:7]+' '+tele[7:10]
#  print telen
  p = re.compile(telen)
  m = p.search(string)
  if m :
    return True
  else :
    return False

def touchP(device, c):
  c = string.atoi(c)
  if c == 0:
    device.touch(360, 920, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
  elif c >= 1 and c <= 9:
    c=c-1
    device.touch(160+(c%3)*225, 440+(c/3)*160, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
 
def isCallState(device):
  """ boolean isCallState ( MonkeyDevice device )
  """
  string = device.shell('service call phone 37');
  p = re.compile('00000002')
  m = p.search(string)
  if m :
    return True
  else :
    return False

def getX(x, Mx):
  """ boolean getX ( MonkeyDevice device, int x, int y)
  """
  x = string.atoi(x)
  if x == 0:
    x=10
  res = ((x*2)-1)*(Mx/20);
  return res

def isResetDefaultOK(device, x, y):
  """ boolean isResetDefaultOK ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 36, 35) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA85/PScript/img/tick');
  os.remove(".tmp")
  return res

def isMA(device, x, y):
  """ boolean isMA ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 230, 5) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA85/PScript/img/NM');
  os.remove(".tmp")
  return res

def isBookmarkMenuOK(device, x, y):
  """ boolean isBookmarkMenuOK ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 310, 655) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA85/PScript/img/bkmk_menu');
  os.remove(".tmp")
  return res

# S:Browser case 14
def isHISTORY(device, x, y):
  """ boolean isWebaddress ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 470, 379) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA85/PScript/img/14-1');
  
  if res:
    os.remove(".tmp")
    return res
  else:
    res = filecmp.cmp('.tmp', 'DA85/PScript/img/14-2');
    os.remove(".tmp")
    return res
# E:Browser case 14

def isWebaddress(device, x, y):
  """ boolean isWebaddress ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 45, 40) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA85/PScript/img/17');
  os.remove(".tmp")
  return res

def isAboutBlank(device, x, y):
  """ boolean isAboutBlank ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 295, 20) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA85/PScript/img/11');
  os.remove(".tmp")
  return res

def isMostV(device, x, y):
  """ boolean isAboutBlank ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 26, 26) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA85/PScript/img/16');
  os.remove(".tmp")
  return res

#S:Browser:case 12, check refresh 
def progressbar(device, x, y, name):
  """ boolean progressbar ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 430, 10) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile(name,'png')
  return 1
def theSame( a, b):
  res = filecmp.cmp(a, b);
  os.remove("a")
  os.remove("b")
  return res
#E:Browser:case 12, check refresh 

def isBuildNumOK(device, x, y):
  """ boolean isBuildNumOK ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 234, 96) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA80/PScript/img/build.png');
  os.remove(".tmp")
  return res

def turnOnAirplaneMode(device, cci_device):
  """ void turnOnAirplaneMode ( MonkeyDevice device, CCIEasyMonkeyDevice cci_device )
  """
  print 'press Home key'
  device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
  sleep(2)

  print 'long-press Power key'
  device.press('KEYCODE_POWER', MonkeyDevice.DOWN)
  sleep(2)
  device.press('KEYCODE_POWER', MonkeyDevice.UP)
  sleep(2)

  if cci_device.isTextVisible('Airplane mode is ON'):
    print 'Airplane mode is already ON'

  if cci_device.isTextVisible('Airplane mode is OFF'):
    cci_device.clickText('Airplane mode');
    print 'Airplane mode is ON'
    sleep(5)

def turnOffAirplaneMode(device, cci_device):
  """ void turnOffAirplaneMode ( MonkeyDevice device, CCIEasyMonkeyDevice cci_device )
  """
  print 'press Home key'
  device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
  sleep(2)

  print 'long-press Power key'
  device.press('KEYCODE_POWER', MonkeyDevice.DOWN)
  sleep(2)
  device.press('KEYCODE_POWER', MonkeyDevice.UP)
  sleep(2)

  if cci_device.isTextVisible('Airplane mode is OFF'):
    print 'Airplane mode is already OFF'

  if cci_device.isTextVisible('Airplane mode is ON'):
    cci_device.clickText('Airplane mode')
    print 'Airplane mode is OFF'
    sleep(5)

def CheckSDcard(device):
  """ boolean CheckSDcard ( MonkeyDevice device )
  """
  string = device.shell("ls -l storage/")
  str1 = string[1:10]
  if str1 == '---------':
    return False
  else:
    return True
