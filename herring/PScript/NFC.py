from com.android.monkeyrunner import MonkeyDevice, MonkeyImage
import filecmp
import os
import sys
from time import sleep

def isAutoChecked(device):
  """ boolean isAutoChecked ( MonkeyDevice device )
  """
  image = device.takeSnapshot()
  rect = (30, 540, 660, 140) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA85/PScript/img/NFC_checked.png');
  os.remove(".tmp")
  return res
