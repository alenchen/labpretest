from com.android.monkeyrunner import MonkeyDevice, MonkeyImage
import filecmp
import os
import sys
from time import sleep

def waitBtOn(device, x, y, maxWaitTime=60):
  """ void waitBtOn (MonkeyDevice device, int x, int y, int maxWaitTime = 60)
      block function.
  """
  rect = (x, y, 147, 35) # x, y ,w ,h
  i = 0
  while not isBtOn(device, x, y):
    if i > maxWaitTime:
      sys.stderr.write('warning: time is over when waitting Bt on\n')
      return False
    i += 1
    sleep(1)
  return True


def waitBtOn_DA80(device, maxWaitTime=60):
  """ void waitBtOn (MonkeyDevice device, int x, int y, int maxWaitTime = 60)
      block function.
  """
  i = 0
  while not isBtOn_DA80(device):
    if i > maxWaitTime:
      sys.stderr.write('warning: time is over when waitting Bt on\n')
      return False
    i += 1
    sleep(1)
  return True

def waitBtOn_byscan(device, maxWaitTime=60):
  """ void waitBtOn (MonkeyDevice device, int x, int y, int maxWaitTime = 60)
      block function.
  """
  i = 0
  while not isBtOn_byscan(device):
    if i > maxWaitTime:
      sys.stderr.write('warning: time is over when waitting Bt on\n')
      return False
    i += 1
    sleep(1)
  return True

def isBtOn(device, x, y):
  """ boolean isBtOn ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 147, 35) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA80/PScript/img/switch_on.png');
  os.remove(".tmp")
  return res

def isBtOn_DA80(device):
  """ boolean isBtOn_DA80 ( MonkeyDevice device)
       
       Warnning: Only for DA80 to use.
  """
  res = device.shell('hciconfig -a')
  if len(res) == 0 :
    return False
  else :
    return True

def isBtOn_byscan(device):
  """ boolean isBtOn ( MonkeyDevice device)
       
       Warnning: scanning bt devices, waste much time.
  """
  res = device.shell('hcitool scan')
  p = re.compile('Scanning')
  m = p.search(res)
  if m :
    return True
  else :
    return False

def isBtOn_DA85(device):
	image = device.takeSnapshot()
	rect = (490, 75, 190, 45) # x, y ,w ,h
	subimage = image.getSubImage( rect )
	subimage.writeToFile('tmppp','png')
	res = filecmp.cmp('tmppp', 'DA85/PScript/img/BT_on.png');
	os.remove("tmppp")
	return res

def isBtOff_DA85(device):
	image = device.takeSnapshot()
	rect = (490, 75, 190, 45) # x, y ,w ,h
	subimage = image.getSubImage( rect )
	subimage.writeToFile('tmppp','png')
	res = filecmp.cmp('tmppp', 'DA85/PScript/img/BT_off.png');
	os.remove("tmppp")
	return res
