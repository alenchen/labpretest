from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'
print 'wake up device'
device.wake()
sleep(2)
if LockScreen.isLock(device):
  print 'unlock screen'
  LockScreen.unLock(device)
  LockScreen.waitUnLock(device)  

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch phone application'
device.touch(87 ,829, MonkeyDevice.DOWN_AND_UP);
sleep(2)

device.touch(87 ,70, MonkeyDevice.DOWN_AND_UP);
sleep(2)
#focus on phone tab

for x in sys.argv[4]:
  Telephony.touchP(device, x)

device.touch(270, 830, MonkeyDevice.DOWN_AND_UP);
sleep(4)
#dial key, sleep 4 s wait for UI show dialing

print "==============\nmo call:\n=============="
if Telephony.isCallState(device):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
  device.touch(270, 710, MonkeyDevice.DOWN_AND_UP);
#hang up calling
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Failed'
