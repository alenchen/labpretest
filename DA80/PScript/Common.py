from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from time import sleep
import os

def scrollDown(device, duration=0.3, steps=10):
  """ void scrollDown ( MonkeyDevice device )
  """
  start = (220, 800)
  end = (220, 200)
  device.drag(start, end, duration, steps)


def scrollUp(device, duration=0.3, steps=10):
  """ void scrollUp ( MonkeyDevice device )
  """
  start = (220, 500)
  end = (220, 800)
  device.drag(start, end, duration, steps)

def scrollLeft(device, duration=0.3, steps=10):
  """ void scrollLeft ( MonkeyDevice device )
  """
  start = (50, 500)
  end = (500, 500)
  device.drag(start, end, duration, steps)

def scrollRight(device, duration=0.3, steps=10):
  """ void scrollRight ( MonkeyDevice device )
  """
  start = (500, 500)
  end = (50, 500)
  device.drag(start, end, duration, steps);


def dragSwitchOn(device, x, y, width=118):
  """ void dragSwitchOn (MonkeyDevice device, int x, int y, int width = 118)
  """
  start = (x, y)
  end   = (x+width, y)
  device.drag(start, end)
  sleep(2)

def dragSwitchOff(device, x, y, width=118):
  """ void dragSwitchOff (MonkeyDevice device, int x, int y, int width = 118)
  """
  start = (x+width, y)
  end   = (x, y)
  device.drag(start, end)
  sleep(2)

def takeSnapshot(device, filename=None, path='errors'):
  """ void takeSnapshot ( MonkeyDevice device)
  """
  picfile = ''
  if not os.path.exists('public/'+path):
    os.makedirs('public/'+path)

  if filename :
    picfile = os.path.join ('public/'+path, filename) + ".png"
  else :
    for x in range(1, 9999):
      filename = "screenshot%04d" % x
      picfile = os.path.join ('public/'+path, filename) + ".png"
      if not os.path.isfile(picfile) :
        break
  image = device.takeSnapshot()
  image.writeToFile(picfile,'png')

def getBrightness(device):
    """ NO use, backight is turned on when we use monkeyrunner.
    """
    return device.shell('cat /sys/class/leds/lcd-backlight/brightness')

def updateResult(result):
  """ void updateResult ( string result )
  """
  os.system("echo 'case_result_is=" + result + "' > case_result_is")
