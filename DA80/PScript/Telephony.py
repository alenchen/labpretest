from com.android.monkeyrunner import MonkeyDevice, MonkeyImage
import re
import sys
import filecmp
import os
import string
from time import sleep

def clearSms(device):
# pull db , after testing push back
  str1 = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT count(*) from sms\'');
  n = string.atoi(str1)
  if n > 0:
#use UI to delete all msg
#trigger submenu
    device.touch(496, 852, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
#Delete all thread
    device.touch(360, 779, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
#Delete
    device.touch(389, 555, MonkeyDevice.DOWN_AND_UP);
    sleep(1)

def isMOSMSOK(device, tele):
  """ boolean is is MO SMS OK ( MonkeyDevice device )
  """
  string = device.shell('sqlite3 /data/data/com.android.providers.telephony/databases/mmssms.db \'SELECT address from sms where (body=\"Qw\" AND type=2)\'');
#  print tele
  telen=tele[0:4]+' '+tele[4:7]+' '+tele[7:10]
#  print telen
  p = re.compile(telen)
  m = p.search(string)
  if m :
    return True
  else :
    return False

def touchP(device, c):
  c = string.atoi(c)
  if c == 0:
    device.touch(270, 690, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
  elif c >= 1 and c <= 9:
    c=c-1
    device.touch(100+(c%3)*170, 330+(c/3)*120, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
 
def isCallState(device):
  """ boolean isCallState ( MonkeyDevice device )
  """
  string = device.shell('service call phone 35');
  p = re.compile('00000002')
  m = p.search(string)
  if m :
    return True
  else :
    return False

def getX(x, Mx):
  """ boolean getX ( MonkeyDevice device, int x, int y)
  """
  x = string.atoi(x)
  if x == 0:
    x=10
  res = ((x*2)-1)*(Mx/20);
  return res



def isBuildNumOK(device, x, y):
  """ boolean isBuildNumOK ( MonkeyDevice device, int x, int y)
  """
  image = device.takeSnapshot()
  rect = (x, y, 234, 96) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA80/PScript/img/build.png');
  os.remove(".tmp")
  return res
