from com.android.monkeyrunner import MonkeyDevice, MonkeyImage
import filecmp
import os
import sys
from time import sleep

def isLock(device):
  """ boolean isLock ( MonkeyDevice device ) 
      capture picture, compare if the picture is the same with lock icon.

      Wanrring: the fucntion doesn't work proper when wallpaper is changed.
  """
  rect = (253, 728, 33, 43) # x, y ,w ,h
  for x in range(1, 6):
    image = device.takeSnapshot()
    subimage = image.getSubImage( rect )
    subimage.writeToFile('.tmp','png')
    res = filecmp.cmp('.tmp', 'DA80/PScript/img/lock_0' + str(x) + '.png');
    os.remove(".tmp")
    if res :
      return True
  return False

def unLock(device, duration=0.5, steps=10):
  """ void unLock (MonkeyDevice device ) 
      drag unlock icon to unlock lock screen, waste 0.5 sec to unlock.
  """
  start = (271, 753)
  end = (496, 753)
  device.drag(start, end)

def waitUnLock(device, maxWaitTime=5):
  i = 0
  while isLock(device):
    if i > maxWaitTime:
      sys.stderr.write('warning: time is over when try to unlock screen\n')
      return False
    i += 1
    sleep(1)
  return True

