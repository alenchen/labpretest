from com.android.monkeyrunner import MonkeyDevice, MonkeyImage
import filecmp
import os
import sys
from time import sleep

def isAutoChecked(device):
  """ boolean isAutoChecked ( MonkeyDevice device )
  """
  image = device.takeSnapshot()
  rect = (79, 452, 252, 26) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA80/PScript/img/brightness_checked.png');
  os.remove(".tmp")
  return res

def isAutoUnChecked(device):
  """ boolean isAutoChecked ( MonkeyDevice device )
  """
  image = device.takeSnapshot()
  rect = (79, 398, 252, 26) # x, y ,w ,h
  subimage = image.getSubImage( rect )
  subimage.writeToFile('.tmp','png')
  res = filecmp.cmp('.tmp', 'DA80/PScript/img/brightness_unchecked.png');
  os.remove(".tmp")
  return res
