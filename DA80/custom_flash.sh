#!/bin/bash
#
# Copyright 2010 Google Inc. All Rights Reserved.
# Author: bgay@google.com (Bruce Gay)
#
# used for flashing bootloader image on DA80

BOOTPART='aboot'
FlexOpt=Baidu
################################################
# sets the name of the boot partition and
# bootfile, then flashes device
#
# Globals:
#   product
#   ROOT
#   BOOTPART
#   bootloaderfile
#   device
# Arguments:
#   None
# Returns:
#   None
################################################
flash_bootloader_image()
{
  if [ $product != "DA80" ]; then
    log_print "Wrong device type, expected DA80!"
    exit
  fi
  if [ "$bootloaderfile" == '' ]; then
    log_print "getting bootloader file for $product"
    bootloaderfile=emmc_appsboot.mbn

    # download bootloader image from build machine
    

    if [ "$bootloaderfile" == '' ]; then
      log_print "bootloader file empty: $bootloaderfile"
      exit
    fi
    if [ ! -e "$ROOT/$product/$bootloaderfile" ]; then
      log_print "bootloader file not found: ./$product/$bootloaderfile"
      exit
    fi
    log_print "using $ROOT/$product/$bootloaderfile as bootloader image file"
  fi
  log_print "downloading bootloader image to $device"
  flash_partition $BOOTPART $ROOT/$product/$bootloaderfile
  flash_kernel_image
  #reboot_into_fastboot_from_fastboot
}
################################################
# sets the name of the kernel partition and
# flashes device
#
# Globals:
#   product
#   ROOT  
#   device
# Arguments:
#   None
# Returns:
#   None
################################################
flash_kernel_image()
{
  if [ "$kernelpart" == '' ]; then
    log_print "setting kernel partion to 'boot'"
    kernelpart='boot'
  fi
  if [ "$kernelfile" == "" ]; then
    log_print "getting kernel file for $product"
    kernelfile=boot.img

    # download kernel image from build machine
    if [ "$kernelfile" == "" ]; then
      log_print "kernel file empty: $kernelfile"
      exit
    fi
    if [ ! -e "$ROOT/$product/$kernelfile" ]; then
      log_print "kernel file not found: ./$product/$kernelfile"
      exit
    fi
    log_print "using $ROOT/$product/$kernelfile as the kernel image file"
  fi
  log_print "downloading kernel image to $device"
  flash_partition $kernelpart  $ROOT/$product/$kernelfile
#  reboot_into_fastboot_from_fastboot
}


################################################
# flashes recovery, flex and persist partition
#
# Globals:
#   product
#   ROOT
# Arguments:
#   None
# Returns:
#   None
################################################
flash_others_image()
{
  log_print "flashing recovery image..."
  
  if [ -e $ROOT/$product/recovery.img ];then
    flash_partition recovery $ROOT/$product/recovery.img
  else
    log_print "recovery.img file not found: $ROOT/$product/recovery.img"
    exit
  fi

  log_print "flashing flex image..."
  #suppose $FlexOpt saved, a(ATT),b(Baidu),d(DSR),e(eMoible),f(Factory),k(KT),s(SBM)
  if [ -e $ROOT/$product/flex_$FlexOpt.img.ext4 ];then
    flash_partition flex $ROOT/$product/flex_$FlexOpt.img.ext4
  else
    log_print "flex file not found: $ROOT/$product/flex_$FlexOpt.img.ext4"
    exit
  fi
  log_print "flashing persist image..."
  if [ -e $ROOT/$product/persist.img ];then
    flash_partition persist $ROOT/$product/persist.img
  else
    log_print "persist.img file not found: $ROOT/$product/persist.img"
    exit
  fi
}
################################################
#
################################################
download_images()
{
  if [ "$DL_APPS" == "1" ]; then
    # download_userdata_image
    rm -f $ROOT/$product/userdata.img.ext4
    curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/userdata.img.ext4 > $ROOT/$product/userdata.img.ext4
    rm -f $ROOT/$product/recovery.img
    curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/recovery.img > $ROOT/$product/recovery.img
    rm -f $ROOT/$product/flex_$FlexOpt.img.ext4
    curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/flex_$FlexOpt.img.ext4 > $ROOT/$product/flex_$FlexOpt.img.ext4
    rm -f $ROOT/$product/persist.img
    curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/persist.img > $ROOT/$product/persist.img
    # download_bootloader_image
    if [ "$bootloaderfile" == '' ]; then
      log_print "getting bootloader file for $product"
      bootloaderfile=emmc_appsboot.mbn
    fi
    rm -f $ROOT/$product/$bootloaderfile
    curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/$bootloaderfile > $ROOT/$product/$bootloaderfile
    if [ "$kernelfile" == "" ]; then
      log_print "getting kernel file for $product"
      kernelfile=boot.img
    fi
    rm -f $ROOT/$product/$kernelfile
    curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/$kernelfile > $ROOT/$product/$kernelfile
  fi

  if [ "$DL_MODEM" == "1" ]; then
    if [ "$radiofile" == "" ]; then
    # download NON_HLOS image from build machine
      radiofile=NON_HLOS.bin
      rm -f $ROOT/$product/$radiofile
      curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/$radiofile > $ROOT/$product/$radiofile
      rm -f $ROOT/$product/sbl1.mbn
      curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/sbl1.mbn > $ROOT/$product/sbl1.mbn
      rm -f $ROOT/$product/sbl2.mbn
      curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/sbl2.mbn > $ROOT/$product/sbl2.mbn
      rm -f $ROOT/$product/sbl3.mbn
      curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/sbl3.mbn > $ROOT/$product/sbl3.mbn
      rm -f $ROOT/$product/rpm.mbn
      curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/rpm.mbn > $ROOT/$product/rpm.mbn
      rm -f $ROOT/$product/tz.mbn
      curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/tz.mbn > $ROOT/$product/tz.mbn
      if [ ! -e "$ROOT/$product/sbl1.mbn" ]||[ ! -e "$ROOT/$product/sbl3.mbn" ]||[
           ! -e "$ROOT/$product/sbl2.mbn" ]||[ ! -e "$ROOT/$product/rpm.mbn"]||[
           ! -e "$ROOT/$product/tz.mbn"]; then
        log_print "... sbl1, sbl2, sbl3 rpm and tz files not found!"
        exit
      fi
    fi
  fi

  if [ "$DL_APPS" == "1" ]; then
    rm -f $ROOT/$product/system.img.ext4
    curl http://$BUILD_MACHINE:8080/job/$PROJECT_NAME/$BUILD_NUMBER/artifact/out/target/product/da80/system.img.ext4 > $ROOT/$product/system.img.ext4
  fi
  log_print "finish downloading"
}
################################################
# flashes the userdata partition
#
# Globals:
#   product
#   ROOT
# Arguments:
#   None
# Returns:
#   None
################################################
flash_userdata_image()
{
  sleep 5
  log_print "flashing userdata..."
  if [ -e $ROOT/$product/userdata.img.ext4 ];then
    flash_partition userdata $ROOT/$product/userdata.img.ext4
  else
    log_print "userdata.img.ext4 file not found: $ROOT/$product/userdata.img.ext4"
    exit
  fi
  flash_others_image
}

################################################
# sets the name of the radio partition and
# radiofile and flashes device
#
# Globals:
#   product
#   ROOT
#   radiofile
#   radiopart
#   device
# Arguments:
#   None
# Returns:
#   None
################################################
flash_radio_image()
{
  if [ "$radiopart" == '' ]; then
    log_print "setting radio partion to 'radio'"
    radiopart='modem'
  fi
  if [ "$radiofile" == "" ]; then
    log_print "getting radio file for $product"
   

    if [ "$radiofile" == "" ]; then
      log_print "radio file empty: $radiofile"
      exit
    fi
    if [ ! -e "$ROOT/$product/$radiofile" ]; then
      log_print "radio file not found: ./$product/$radiofile"
      exit
    fi
    log_print "using $ROOT/$product/$radiofile as the radio image file"
  fi

  log_print "Flash sbl1, sbl2, sbl3 rpm and tz for DA80"
  flash_partition sbl1 $ROOT/$product/sbl1.mbn
  flash_partition sbl2 $ROOT/$product/sbl2.mbn
  flash_partition sbl3 $ROOT/$product/sbl3.mbn
  flash_partition rpm  $ROOT/$product/rpm.mbn
  flash_partition tz   $ROOT/$product/tz.mbn
#  log_print "downloading radio image to $device"
#  flash_partition $radiopart  $ROOT/$product/$radiofile
#  reboot_into_fastboot_from_fastboot
}
################################################
# flashes the system partition
#
# Globals:
#   product
#   ROOT
# Arguments:
#   None
# Returns:
#   None
################################################
flash_system_image()
{
  # download tz image from build machine
  
  log_print "flashing system..."
  if [ -e $ROOT/$product/system.img.ext4 ];then
    flash_partition system $ROOT/$product/system.img.ext4
  else
    log_print "system.img.ext4 file not found: $ROOT/$product/system.img.ext4"
    exit
  fi
  fastboot_command reboot
}
