from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

print 'wake up device'
device.wake()
sleep(2)

if LockScreen.isLock(device):
  print 'unlock screen'
  LockScreen.unLock(device)
  LockScreen.waitUnLock(device)  

print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch system settings'
device.touch(280, 853, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'scroll down'
Common.scrollDown(device)
sleep(1)

#about phone
device.touch(287, 856, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#go to Build number
Common.scrollDown(device)
sleep(1)

print "==============\ncheck build number:\n=============="
if Telephony.isBuildNumOK(device, 0, 792):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Failed'
