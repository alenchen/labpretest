#!/bin/bash
#
# Test Plan : Auto Regular CM Process Demo
#
#

# Set delay time between two test case
DELAY=5
MAILDELAY=1
PROJECT="DA80"
REPORT_NAME="${1}_${2}.html"
REPORT_SW_VERSION="${3}"
REPORT_HW_VERSION="DVT1-2"
TEST_SERIAL="${2}"
TEL_NUMBER="${4}"
PLAN_DESCRIPTION="Demo-Automation_CM_Process"

. common/plan_basic.sh

# Part 1: Define mail list
add_maillist alen2_chen
add_maillist carrie_wang
add_maillist danny_tsai

# Part 2: Define test case
# ====== Case 1. Turn on Wi-Fi ======
insert_testcase "Turn on Wi-Fi" "IO" "turn_on_wifi.py" "" 1
# ====== Case 2. Turn on BT ======
insert_testcase "Turn on BT" "IO" "turn_on_bt.py" "" 1
# ====== Case 3. Setup auto brightness ======
insert_testcase "Setup auto brightness" "MM" "mm_brightness.py" "" 1
# ====== Case 4. Take a picture ======
insert_testcase "Take a picture" "MM" "mm_camera.py" "" 1
# ====== Case 5. Verify ADB functionality ======
insert_testcase "Verify ADB functionality" "SYS" "adb_fun.py" "" 1
# ====== Case 6. Check build version ======
insert_testcase "Check build version" "SYS" "check_bv.py" "" 1
# ====== Case 7. Send a short message ======
insert_testcase "Send a short message" "RIL" "mo_sms.py" "$TEL_NUMBER" 1
# ====== Case 8. Dial a voice call ======
insert_testcase "Dial a voice call" "RIL" "mo_call.py" "$TEL_NUMBER" 1

# Start to run test plan
run_test_plan "${PROJECT}" "${2}"