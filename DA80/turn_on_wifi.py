from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from time import sleep
import sys
from PScript import *

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

print 'wake up device'
device.wake()
sleep(2)

if LockScreen.isLock(device):
  print 'unlock screen'
  LockScreen.unLock(device)
  LockScreen.waitUnLock(device)  


print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch system settings'
device.touch(280, 853, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print 'scroll up'
Common.scrollUp(device)
sleep(1)

print "==============\nturn wifi on:\n=============="
if not Wifi.isWifiOn(device):
  # turn wifi on
  Common.dragSwitchOn(device, 352, 192)

if Wifi.waitWifiOn(device):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Failed' 
