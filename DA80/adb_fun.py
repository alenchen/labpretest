from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import os
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

print 'wake up device'
device.wake()
sleep(2)

if LockScreen.isLock(device):
  print 'unlock screen'
  LockScreen.unLock(device)
  LockScreen.waitUnLock(device)  

def adbFun(device):
  string = device.shell('getprop');
  f = open('Prop',"w+")
  f.write(string) 
  if f.readline() > 0:
    return True
  else :
    return False


print "==============\ncheck adb functionality:\n=============="
#print "1) check adb getprop:\n"
if adbFun(device):
#  print "2) check adb pull functionality:\n"
  os.system("platform-tools/adb pull /system/app/Settings.apk")
  if os.path.isfile("Settings.apk"):
    os.remove("Prop") 
    os.remove("Settings.apk")
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult("Pass");
    print 'Pass'
  else:
    Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult("Fail");
    print 'Fail'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult("Fail");
  print 'Fail' 
