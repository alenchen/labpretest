from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage, MonkeyView
from com.android.monkeyrunner.easy import EasyMonkeyDevice
from com.android.monkeyrunner.easy import By
from com.cci.monkeyrunnerplugin import Plugin, CCIEasyMonkeyDevice, MonkeyHelper
import sys,os,commands
from PScript import *
from time import sleep

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

devices = commands.getoutput('adb devices').strip().split('\n')[1:]
if len(devices) == 0:
  MonkeyRunner.alert("No devices found. Start an emulator or connect a device.", "No devices found", "Exit")
  sys.exit(1)
elif len(devices) == 1:
  choice = 0
else:
  choice = MonkeyRunner.choice("More than one device found. Please select target device.", devices, "Select target device")

device_id = devices[choice].split('\t')[0]

print 'wake up device'
device.wake()
sleep(2)

if LockScreen.isLock(device):
  print 'unlock screen'
  LockScreen.unLock(device)
  LockScreen.waitUnLock(device)  


print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'press menu key'
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch system settings'
device.touch(280, 853, MonkeyDevice.DOWN_AND_UP);
sleep(2)

print "Create CCI easymonkeydevice"
cci_device = CCIEasyMonkeyDevice(device_id, device)

print 'click Display'
cci_device.clickText('Display')
sleep(5)

print 'click Brightness'
cci_device.clickText('Brightness')
sleep(5)

print "==============\ndefault brightness is checked\n=============="
if Brightness.isAutoChecked(device) :
    Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Pass');
    print 'pass'
else :
    Common.takeSnapshot(device, filename=sys.argv[3]+'-1', path=sys.argv[1]+'/'+sys.argv[2])
    Common.updateResult('Fail');
    print 'false'

for x in range(1, 3):
  if Brightness.isAutoChecked(device) :
    print 'uncheck it'
    device.touch(191, 466, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
    print 'press ok'
    device.touch(390, 615, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
    print 'touch Brightness'
    cci_device.clickText('Brightness')
    sleep(5)
    print "==============\nbrightness uncheck test.\n=============="
    if Brightness.isAutoUnChecked(device):
      Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
      Common.updateResult('Pass');
      print 'Pass'
    else :
      Common.takeSnapshot(device, filename=sys.argv[3]+'-2', path=sys.argv[1]+'/'+sys.argv[2])
      Common.updateResult('Fail');
      print 'Failed'
  elif Brightness.isAutoUnChecked(device) :
    print 'check it'
    device.touch(191, 414, MonkeyDevice.DOWN_AND_UP)
    sleep(1)
    print 'press ok'
    device.touch(368, 563, MonkeyDevice.DOWN_AND_UP);
    sleep(1)
    print 'touch Brightness'
    cci_device.clickText('Brightness')
    sleep(5)
    print "==============\nbrightness check test.\n=============="
    if Brightness.isAutoChecked(device):
      Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
      Common.updateResult('Pass');
      print 'Pass'
    else :
      Common.takeSnapshot(device, filename=sys.argv[3]+'-3', path=sys.argv[1]+'/'+sys.argv[2])
      Common.updateResult('Fail');
      print 'Failed'
