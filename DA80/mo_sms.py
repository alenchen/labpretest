from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
from PScript import *
from time import sleep
import sys

print 'wait connect to device'
device = MonkeyRunner.waitForConnection()
print 'connected to device'

print 'wake up device'
device.wake()
sleep(2)

if LockScreen.isLock(device):
  print 'unlock screen'
  LockScreen.unLock(device)
  LockScreen.waitUnLock(device)  


print 'press home key'
device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
sleep(2)

print 'touch sms application'
device.touch(367,822, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#Telephony.clearSms(device)

#add sms button
device.touch(43,851, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#start to input Tele num,  
device.touch(43,851, MonkeyDevice.DOWN_AND_UP);
sleep(1)

for x in sys.argv[4]:
  device.touch(Telephony.getX(x, 540),592, MonkeyDevice.DOWN_AND_UP);
  sleep(1)


#focus on body 
device.touch(44,514, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#Q
device.touch(30,595, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#e
device.touch(80,595, MonkeyDevice.DOWN_AND_UP);
sleep(1)

#send buttom
device.touch(494,453, MonkeyDevice.DOWN_AND_UP);
sleep(5)

print "==============\nmo sms:\n=============="
#print 'check database '
if Telephony.isMOSMSOK(device, sys.argv[4]):
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Pass');
# twice back key, return to messaging screen, for more than twice test
  device.touch(126,930, MonkeyDevice.DOWN_AND_UP);
  sleep(1)
  device.touch(126,930, MonkeyDevice.DOWN_AND_UP);
  sleep(1)
  print 'Pass'
else :
  Common.takeSnapshot(device, filename=sys.argv[3], path=sys.argv[1]+'/'+sys.argv[2])
  Common.updateResult('Fail');
  print 'Failed' 
