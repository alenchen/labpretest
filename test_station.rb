require 'sinatra'
require 'pony'
require 'json'

list_of_device = Array.new
get '/update_device' do
    alreadyHas = "f"
    new_device = {:device_name => "",:device_id => "",:device_status => {
            :state => "",:data => ""}}
    for x in 0..(list_of_device.size-1)
      if (params["device_name"] == list_of_device[x][:device_name])and(params["device_id"] == list_of_device[x][:device_id])
          alreadyHas="t"
          list_of_device[x][:device_status][:state] = params["state"]
          list_of_device[x][:device_status][:data] = params["data"]
          break
      end
    end
    if alreadyHas.eql? "f"
      new_device[:device_name] = params["device_name"]
      new_device[:device_id] = params["device_id"]
      new_device[:device_status][:state] = params["state"]
      new_device[:device_status][:data] = params["data"]
      list_of_device << new_device
    end
    #p list_of_device
end

get '/call_test_station' do
    "Call test station, #{params.inspect}"
    cmd = "./labpretest.sh -d #{params["device"]} -b #{params["build_machine"]} -a #{params["dl_apps"]} -c #{params["dl_modem"]} -p #{params["testplan"]} -n #{params["project_name"]} -s #{params["build_number"]} -t #{params["telephone_number"]} -g #{params["tag_name"]}"
    "cmd = #{cmd}"
    value = `#{cmd}`
end

get '/send_notify_mail' do
    Pony.mail({
        :to => "#{params["to"]}@compalcomm.com",
        :via => :smtp,
        :via_options => {
            :address => 'webmail.compalcomm.com',
            :port => '25'
        },
        :from => "#{params["test_station"]}@compalcomm.com",
        :subject => "[#{params["project"]}] #{params["plan_description"]} Test Result - #{params["sw_version"]}",
        :html_body => erb(:notify_mail)
    })
end

get '/send_test_mail' do
    Pony.mail({
        :to => "#{params["to"]}@compalcomm.com",
        :via => :smtp,
        :via_options => {
            :address => 'webmail.compalcomm.com',
            :port => '25'
        },
        :from => "#{params["from"]}@compalcomm.com",
        :subject => "Hello cm test",
        :html_body => "It's a test mail"
    })
end

get '/test_station.json' do
    callback = params.delete('jsonp')
    json = list_of_device.to_json

    if callback
        content_type :js
        response = "#{callback}(#{json})"
    else
        content_type :json
        response = json
    end
    response
end

get '/hello' do
    "Hello World!"
end

get '/generator' do
    erb :generator
end

get '/' do
    erb :index
end
